package com.AskingProject.utils;

public enum userType {
	Invited,
	normalUser,
	questionOwner,
	answerOwner
}
