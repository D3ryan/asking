package com.AskingProject.dao;

import java.io.InputStream;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.AskingProject.models.Preguntas_Model;
import com.AskingProject.utils.DbConnection;

public class PreguntasDAO {
	
	public static int insertUpdateDeletePregunta(String pOpc, Preguntas_Model pregunta) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Preguntas(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
           
            statement.setString(1, pOpc);            
            statement.setLong(2, pregunta.getIdPregunta());            
            statement.setString(3, pregunta.getTituloPregunta());           
            statement.setString(4, pregunta.getDescripcionPregunta());            
            statement.setBlob(5, pregunta.getImagenPregunta());
            
            if (pregunta.getFechaCreacionPregunta() != null) {
            	java.util.Date utilDateFechaCrea = pregunta.getFechaCreacionPregunta();
        		java.sql.Date fechaConvertidaFechaCrea = new java.sql.Date(utilDateFechaCrea.getTime());    		
                statement.setDate(6, fechaConvertidaFechaCrea);
            }
            else {
            	statement.setDate(6, null);
            }
            statement.setByte(7, pregunta.getEstadoPregunta());            
            statement.setByte(8, pregunta.getEditadoPregunta());            
            statement.setLong(9, pregunta.getUsuario_Pregunta());            
            statement.setLong(10, pregunta.getRespuesta_Correcta());            
            statement.setInt(11, pregunta.getCategoria_Pregunta());
            statement.setInt(12, pregunta.getPreguntaInicioPagina());
            
            if (pregunta.getPreguntaFechaDesde() != null) {
            	java.util.Date utilDateFechaDesde = pregunta.getPreguntaFechaDesde();
        		java.sql.Date fechaConvertidaFechaDesde = new java.sql.Date(utilDateFechaDesde.getTime());    		
                statement.setDate(13, fechaConvertidaFechaDesde);
            }
            else {
            	statement.setDate(13, null);
            }
            
            if (pregunta.getPreguntaFechaHasta() != null) {
            	java.util.Date utilDateFechaHasta = pregunta.getPreguntaFechaHasta();
        		java.sql.Date fechaConvertidaFechaHasta = new java.sql.Date(utilDateFechaHasta.getTime());    		
                statement.setDate(14, fechaConvertidaFechaHasta);
            }
            else {
            	statement.setDate(14, null);
            }
            
            statement.setLong(15, pregunta.getCantidadVotosUtiles());       
            statement.setLong(16, pregunta.getCantidadVotosFavoritos());       
            
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }

    public static List<Preguntas_Model> getPregunta(String pOpc, Preguntas_Model pregunta) throws Exception  {
        List<Preguntas_Model> listaPreguntas = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Preguntas(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, pregunta.getIdPregunta());            
            statement.setString(3, pregunta.getTituloPregunta());           
            statement.setString(4, pregunta.getDescripcionPregunta());            
            statement.setBlob(5, pregunta.getImagenPregunta());
            
            if (pregunta.getFechaCreacionPregunta() != null) {
            	 java.util.Date utilDateFechaCrea = pregunta.getFechaCreacionPregunta();
         		 java.sql.Date fechaConvertidaFechaCrea = new java.sql.Date(utilDateFechaCrea.getTime());    		
                 statement.setDate(6, fechaConvertidaFechaCrea);
            }
            else {
            	statement.setDate(6, null);
            }
           
            
            statement.setByte(7, pregunta.getEstadoPregunta());            
            statement.setByte(8, pregunta.getEditadoPregunta());            
            statement.setLong(9, pregunta.getUsuario_Pregunta());            
            statement.setLong(10, pregunta.getRespuesta_Correcta());            
            statement.setInt(11, pregunta.getCategoria_Pregunta());
            statement.setInt(12, pregunta.getPreguntaInicioPagina());
            

            if (pregunta.getPreguntaFechaDesde() != null) {
            	java.util.Date utilDateFechaDesde = pregunta.getPreguntaFechaDesde();
        		java.sql.Date fechaConvertidaFechaDesde = new java.sql.Date(utilDateFechaDesde.getTime());    		
                statement.setDate(13, fechaConvertidaFechaDesde);
            }
            else {
            	statement.setDate(13, null);
            }
            
            if (pregunta.getPreguntaFechaHasta() != null) {
            	java.util.Date utilDateFechaHasta = pregunta.getPreguntaFechaHasta();
        		java.sql.Date fechaConvertidaFechaHasta = new java.sql.Date(utilDateFechaHasta.getTime());    		
                statement.setDate(14, fechaConvertidaFechaHasta);
            }
            else {
            	statement.setDate(14, null);
            }
            
            statement.setLong(15, pregunta.getCantidadVotosUtiles());       
            statement.setLong(16, pregunta.getCantidadVotosFavoritos());       
            
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                
            	long IdPregunta = resultSet.getLong("IdPregunta");
                String TituloPregunta = resultSet.getString("TituloPregunta");
                String DescripcionPregunta = resultSet.getString("DescripcionPregunta");
                InputStream ImagenPregunta = resultSet.getBinaryStream("ImagenPregunta");
                
                Timestamp timeStampAux = resultSet.getTimestamp("FechaCreacionPregunta");
				Date FechaCreacionPregunta = null;
					if (timeStampAux != null)
                FechaCreacionPregunta = new Date(timeStampAux.getTime());
                
                byte EstadoPregunta = resultSet.getByte("EstadoPregunta");
                byte EditadoPregunta = resultSet.getByte("EditadoPregunta");
                long Usuario_Pregunta = resultSet.getLong("Usuario_Pregunta");
                long Respuesta_Correcta = resultSet.getLong("Respuesta_Correcta");
                int Categoria_Pregunta = resultSet.getInt("Categoria_Pregunta");
                
                
                String usernameUsuarioPregunta = "";
                InputStream imagenPerfilUsuarioPregunta = null;
                String tituloCategoriaPregunta = "";
                long cantidadVotosUtiles = -1;
                long cantidadVotosNoUtiles = -1;
                long cantidadVotosFavoritos = -1;
                byte EstadoUsuarioPregunta = -1;
                
                if (DbConnection.hasColumn(resultSet, "UsernameUsuario"))
                	usernameUsuarioPregunta = resultSet.getString("UsernameUsuario");
                
                if (DbConnection.hasColumn(resultSet, "ImagenPerfilUsuario"))
                	imagenPerfilUsuarioPregunta = resultSet.getBinaryStream("ImagenPerfilUsuario");
                
                if (DbConnection.hasColumn(resultSet, "EstadoUsuario"))
                	EstadoUsuarioPregunta = resultSet.getByte("EstadoUsuario");
                
                if (DbConnection.hasColumn(resultSet, "TituloCategoria"))
                	tituloCategoriaPregunta = resultSet.getString("TituloCategoria");
                
                if (DbConnection.hasColumn(resultSet, "CantVotosUtiles"))
                	cantidadVotosUtiles = resultSet.getLong("CantVotosUtiles");
                
                if (DbConnection.hasColumn(resultSet, "CantVotosNoUtiles"))
                	cantidadVotosNoUtiles = resultSet.getLong("CantVotosNoUtiles");
                
                if (DbConnection.hasColumn(resultSet, "CantVotosFavoritas"))
                	cantidadVotosFavoritos = resultSet.getLong("CantVotosFavoritas");
                
                // Agregamos el usuario a la lista
                
                listaPreguntas.add(new Preguntas_Model(IdPregunta, TituloPregunta, DescripcionPregunta, ImagenPregunta,
                							FechaCreacionPregunta, EstadoPregunta, EditadoPregunta, Usuario_Pregunta, Respuesta_Correcta, Categoria_Pregunta,
                							usernameUsuarioPregunta, imagenPerfilUsuarioPregunta, EstadoUsuarioPregunta, tituloCategoriaPregunta, 
                							cantidadVotosUtiles, cantidadVotosNoUtiles, cantidadVotosFavoritos
                		));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaPreguntas;
    }
	
	 public static int insertPregunta(Preguntas_Model pregunta) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Preguntas('I', NULL, ?, ?, ?, NULL, NULL, NULL, ?, NULL, ?, NULL, NULL, NULL, NULL, NULL);");
	           
	            statement.setString(1, pregunta.getTituloPregunta());
	            
	            statement.setString(2, pregunta.getDescripcionPregunta());
	            
	            statement.setBlob(3, pregunta.getImagenPregunta());
	            
	            statement.setLong(4, pregunta.getUsuario_Pregunta());
	            
	            statement.setInt(5, pregunta.getCategoria_Pregunta());

	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
	    public static int updatePregunta(Preguntas_Model pregunta) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Preguntas('U', ?, ?, ?, ?, NULL, NULL, NULL, NULL, NULL, ?, NULL, NULL, NULL, NULL, NULL);");
	           
	            statement.setLong(1, pregunta.getIdPregunta());
	            
	            statement.setString(2, pregunta.getTituloPregunta());
	            
	            statement.setString(3, pregunta.getDescripcionPregunta());
	            
	            statement.setBlob(4, pregunta.getImagenPregunta());
	            
	            statement.setInt(5, pregunta.getCategoria_Pregunta());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
	    public static int setRespuestaCorrectaAPregunta(Preguntas_Model pregunta) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Preguntas('R', ?, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ?, NULL, NULL, NULL, NULL, NULL);");
	           
	            statement.setLong(1, pregunta.getIdPregunta());
	            
	            statement.setLong(2, pregunta.getRespuesta_Correcta());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
	    public static int deletePregunta(Preguntas_Model pregunta) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Preguntas('D', ?, 	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
	           
	            statement.setLong(1, pregunta.getIdPregunta());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	  
	    public static Preguntas_Model getPregunta(long pIdPregunta) throws Exception  {
	    	Preguntas_Model preguntaElegida = null;
	        Connection con = null;
	        CallableStatement statement = null;

	        try {
	            con = DbConnection.getConnection();
	            statement = con.prepareCall("CALL sp_Preguntas('S', ?, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
	            statement.setLong(1, pIdPregunta);
	            
	            ResultSet resultSet = statement.executeQuery();
	            
	            if (resultSet.next()) {
	                long IdPregunta = resultSet.getLong("IdPregunta");
	                String TituloPregunta = resultSet.getString("TituloPregunta");
	                String DescripcionPregunta = resultSet.getString("DescripcionPregunta");
	                InputStream ImagenPregunta = resultSet.getBinaryStream("ImagenPregunta");
	                Date FechaCreacionPregunta = resultSet.getDate("FechaCreacionPregunta");
	                byte EstadoPregunta = resultSet.getByte("EstadoPregunta");
	                byte EditadoPregunta = resultSet.getByte("EditadoPregunta");
	                long Usuario_Pregunta = resultSet.getLong("Usuario_Pregunta");
	                long Respuesta_Correcta = resultSet.getLong("Respuesta_Correcta");
	                int Categoria_Pregunta = resultSet.getInt("Categoria_Pregunta");
	                // Agregamos el usuario a la lista
	                preguntaElegida =new Preguntas_Model(IdPregunta, TituloPregunta, DescripcionPregunta, ImagenPregunta,
	            			FechaCreacionPregunta, EstadoPregunta, EditadoPregunta, Usuario_Pregunta, Respuesta_Correcta, Categoria_Pregunta
	                		);
	            }
	            else {
	    			throw new Exception ("No hemos encontrado el usuario con c�digo = " + pIdPregunta);
	    		}
	            
	        } 
	        catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        } 
	        finally {
	            statement.close();
	            con.close();
	        }

	        return preguntaElegida;
	    }
	    
	    public static List<Preguntas_Model> getListPreguntas() throws Exception  {
	        List<Preguntas_Model> listaPreguntas = new ArrayList<>();
	        Connection con = null;
	        CallableStatement statement = null;

	        try {
	            con = DbConnection.getConnection();
	            statement = con.prepareCall("CALL sp_Preguntas('X', NULL, 	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
	            ResultSet resultSet = statement.executeQuery();
	            // Si el resultSet tiene resultados lo recorremos
	            while (resultSet.next()) {
	                // Obtenemos el valor del result set en base al nombre de la
	                // columna
	                long IdPregunta = resultSet.getLong("IdPregunta");
	                String TituloPregunta = resultSet.getString("TituloPregunta");
	                String DescripcionPregunta = resultSet.getString("DescripcionPregunta");
	                InputStream ImagenPregunta = resultSet.getBinaryStream("ImagenPregunta");
	                Date FechaCreacionPregunta = resultSet.getDate("FechaCreacionPregunta");
	                byte EstadoPregunta = resultSet.getByte("EstadoPregunta");
	                byte EditadoPregunta = resultSet.getByte("EditadoPregunta");
	                long Usuario_Pregunta = resultSet.getLong("Usuario_Pregunta");
	                long Respuesta_Correcta = resultSet.getLong("Respuesta_Correcta");
	                int Categoria_Pregunta = resultSet.getInt("Categoria_Pregunta");
	                // Agregamos el usuario a la lista
	                
	                listaPreguntas.add(new Preguntas_Model(IdPregunta, TituloPregunta, DescripcionPregunta, ImagenPregunta,
	                							FechaCreacionPregunta, EstadoPregunta, EditadoPregunta, Usuario_Pregunta, Respuesta_Correcta, Categoria_Pregunta
	                		));
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        } finally {
	            statement.close();
	            con.close();
	        }

	        return listaPreguntas;
	    }

}
