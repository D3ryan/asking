/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.AskingProject.dao;

import com.AskingProject.models.Usuarios_Model;
import com.AskingProject.utils.DbConnection;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author magoc
 */
public class UsuariosDAO {

    /**
     * Inserta un usuario en la base de datos
     *
     * @param user
     * @return
     */

     //TODO: Es ideal incluir el "throws Exception" ?
	
	public static int insertUpdateDeleteUsuario(String pOpc, Usuarios_Model usuario) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Usuarios(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
           
            statement.setString(1, pOpc);
            
            statement.setLong(2, usuario.getIdUsuario());
            
            statement.setString(3, usuario.getNombreUsuario());
           
            statement.setString(4, usuario.getApellidoPaternoUsuario());
            
            statement.setString(5, usuario.getApellidoMaternoUsuario());
            
            if (usuario.getFechaNacimientoUsuario() != null) {
            java.util.Date utilDateFechaNac = usuario.getFechaNacimientoUsuario();
    		java.sql.Date fechaConvertidaFechaNac = new java.sql.Date(utilDateFechaNac.getTime());
    		
            statement.setDate(6, fechaConvertidaFechaNac);
            }
            else {
            	statement.setDate(6, null);
            }
            
            statement.setString(7, usuario.getCorreoUsuario());
            
            statement.setBlob(8, usuario.getImagenPerfilUsuario());
            
            statement.setString(9, usuario.getUsernameUsuario());
            
            statement.setString(10, usuario.getPasswordUsuario());
            
            if (usuario.getFechaCreacionUsuario() != null) {
            java.util.Date utilDateFechaCrea = usuario.getFechaCreacionUsuario();
    		java.sql.Date fechaConvertidaFechaCrea = new java.sql.Date(utilDateFechaCrea.getTime());
    		
            statement.setDate(11, fechaConvertidaFechaCrea);
            }
            else {
            	statement.setDate(11, null);
            }
            
            statement.setByte(12, usuario.getEstadoUsuario());
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }

    public static List<Usuarios_Model> getUsuarios(String pOpc, Usuarios_Model usuario) throws Exception  {
        List<Usuarios_Model> listaUsuarios = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Usuarios(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);
            statement.setLong(2, usuario.getIdUsuario());
            statement.setString(3, usuario.getNombreUsuario());           
            statement.setString(4, usuario.getApellidoPaternoUsuario());            
            statement.setString(5, usuario.getApellidoMaternoUsuario());
            
            if (usuario.getFechaNacimientoUsuario() != null) {
            java.util.Date utilDateFechaNac = usuario.getFechaNacimientoUsuario();
    		java.sql.Date fechaConvertidaFechaNac = new java.sql.Date(utilDateFechaNac.getTime());    		
            statement.setDate(6, fechaConvertidaFechaNac);
            }
            else {
            	statement.setDate(6, null);
            }
            
            statement.setString(7, usuario.getCorreoUsuario());            
            statement.setBlob(8, usuario.getImagenPerfilUsuario());            
            statement.setString(9, usuario.getUsernameUsuario());            
            statement.setString(10, usuario.getPasswordUsuario());
            
            if (usuario.getFechaCreacionUsuario() != null) {
            java.util.Date utilDateFechaCrea = usuario.getFechaCreacionUsuario();
    		java.sql.Date fechaConvertidaFechaCrea = new java.sql.Date(utilDateFechaCrea.getTime());            
            statement.setDate(11, fechaConvertidaFechaCrea);
            }
            else {
            	statement.setDate(11, null);
            }
            
            statement.setByte(12, usuario.getEstadoUsuario());
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                
                long IdUsuario = resultSet.getLong("IdUsuario");
                String NombreUsuario = resultSet.getString("NombreUsuario");
                String ApellidoPaternoUsuario = resultSet.getString("ApellidoPaternoUsuario");
                String ApellidoMaternoUsuario = resultSet.getString("ApellidoMaternoUsuario");
                Date FechaNacimientoUsuario = resultSet.getDate("FechaNacimientoUsuario");
                String CorreoUsuario = resultSet.getString("CorreoUsuario");
                InputStream ImagenPerfilUsuario = resultSet.getBinaryStream("ImagenPerfilUsuario");
                String UsernameUsuario = resultSet.getString("UsernameUsuario");
                String PasswordUsuario = resultSet.getString("PasswordUsuario");
                Date FechaCreacionUsuario = resultSet.getDate("FechaCreacionUsuario");
                byte EstadoUsuario = resultSet.getByte("EstadoUsuario");
                
                long cantPreguntasUsuario = 0;
                long cantRespuestasUsuario = 0;
                long cantPreguntasFavoritasUsuario = 0;
                long cantPreguntasUtilesUsuario = 0;
                long cantPreguntasNoUtilesUsuario = 0;
                
                if (DbConnection.hasColumn(resultSet, "CantPreguntasUsuario"))
                	cantPreguntasUsuario = resultSet.getLong("CantPreguntasUsuario");
                
                if (DbConnection.hasColumn(resultSet, "CantRespuestasUsuario"))
                	cantRespuestasUsuario = resultSet.getLong("CantRespuestasUsuario");
                
                if (DbConnection.hasColumn(resultSet, "CantPreguntasFavoritasUsuario"))
                	cantPreguntasFavoritasUsuario = resultSet.getLong("CantPreguntasFavoritasUsuario");
                
                if (DbConnection.hasColumn(resultSet, "CantPreguntasUtilesUsuario"))
                	cantPreguntasUtilesUsuario = resultSet.getLong("CantPreguntasUtilesUsuario");
                
                if (DbConnection.hasColumn(resultSet, "CantPreguntasNoUtilesUsuario"))
                	cantPreguntasNoUtilesUsuario = resultSet.getLong("CantPreguntasNoUtilesUsuario");
                
                
                listaUsuarios.add(new Usuarios_Model(
                		IdUsuario, NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario,
                		FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, 
                		UsernameUsuario, PasswordUsuario, FechaCreacionUsuario, EstadoUsuario,
                		cantPreguntasUsuario, cantRespuestasUsuario, 
                		cantPreguntasFavoritasUsuario, cantPreguntasUtilesUsuario, cantPreguntasNoUtilesUsuario
                		
                		));
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaUsuarios;
    }

    public static int insertUsuario(Usuarios_Model usuario) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Usuarios('I', NULL, ?, ?, ?, ?, ?, ?, ?, ?, NULL, NULL);");
           
            statement.setString(1, usuario.getNombreUsuario());
            
            statement.setString(2, usuario.getApellidoPaternoUsuario());
            
            statement.setString(3, usuario.getApellidoMaternoUsuario());
            
            java.util.Date utilDate = usuario.getFechaNacimientoUsuario();
    		java.sql.Date fechaConvertida = new java.sql.Date(utilDate.getTime());
    		
            statement.setDate(4, fechaConvertida);
            
            statement.setString(5, usuario.getCorreoUsuario());
            
            statement.setBlob(6, usuario.getImagenPerfilUsuario());
            
            statement.setString(7, usuario.getUsernameUsuario());
            
            statement.setString(8, usuario.getPasswordUsuario());

            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
    
    public static int updateUsuario(Usuarios_Model usuario) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Usuarios('U', ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL, NULL);");
           
            statement.setLong(1, usuario.getIdUsuario());
            
            statement.setString(2, usuario.getNombreUsuario());
            
            statement.setString(3, usuario.getApellidoPaternoUsuario());
            
            statement.setString(4, usuario.getApellidoMaternoUsuario());
            
            java.util.Date utilDate = usuario.getFechaNacimientoUsuario();
    		java.sql.Date fechaConvertida = new java.sql.Date(utilDate.getTime());
    		
            statement.setDate(5, fechaConvertida);
            
            statement.setString(6, usuario.getCorreoUsuario());
            
            statement.setBlob(7, usuario.getImagenPerfilUsuario());
            
            statement.setString(8, usuario.getUsernameUsuario());
            
            statement.setString(9, usuario.getPasswordUsuario());

            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
    
    public static int deleteUsuario(Usuarios_Model usuario) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Usuarios('D', ?, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
           
            statement.setLong(1, usuario.getIdUsuario());
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
  
    
    
    public static Usuarios_Model getUsuario(long pIdUsuario) throws Exception  {
        Usuarios_Model usuarioElegido = null;
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Usuarios('S', ?, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
            statement.setLong(1, pIdUsuario);
            
            ResultSet resultSet = statement.executeQuery();
            
            if (resultSet.next()) {
                
                long IdUsuario = resultSet.getLong("IdUsuario");
                String NombreUsuario = resultSet.getString("NombreUsuario");
                String ApellidoPaternoUsuario = resultSet.getString("ApellidoPaternoUsuario");
                String ApellidoMaternoUsuario = resultSet.getString("ApellidoMaternoUsuario");
                Date FechaNacimientoUsuario = resultSet.getDate("FechaNacimientoUsuario");
                String CorreoUsuario = resultSet.getString("CorreoUsuario");
                InputStream ImagenPerfilUsuario = resultSet.getBinaryStream("ImagenPerfilUsuario");
                String UsernameUsuario = resultSet.getString("UsernameUsuario");
                String PasswordUsuario = resultSet.getString("PasswordUsuario");
                Date FechaCreacionUsuario = resultSet.getDate("FechaCreacionUsuario");
                byte EstadoUsuario = resultSet.getByte("EstadoUsuario");
                // Agregamos el usuario a la lista
                usuarioElegido =new Usuarios_Model(
                		IdUsuario, NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario,
                		FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, 
                		UsernameUsuario, PasswordUsuario, FechaCreacionUsuario, EstadoUsuario
                		);
            }
            else {
    			throw new Exception ("No hemos encontrado el usuario con c�digo = " + pIdUsuario);
    		}
            
        } 
        catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } 
        finally {
            statement.close();
            con.close();
        }

        return usuarioElegido;
    }
    
    public static int getUsuario(String UsernameUsuario) throws Exception  {
    	 int usernameEncontrado = 0;
         Connection con = null;
         CallableStatement statement = null;

         try {
             con = DbConnection.getConnection();
             statement = con.prepareCall("CALL sp_Usuarios('V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, ?, NULL, NULL, NULL);");
             statement.setString(1, UsernameUsuario);
             
             ResultSet resultSet = statement.executeQuery();
             
             if (resultSet.next())
            	 usernameEncontrado = resultSet.getInt("UsernameEncontrado");
             
         } 
         catch (SQLException ex) {
             System.out.println(ex.getMessage());
         } 
         finally {
             statement.close();
             con.close();
         }

         return usernameEncontrado;
    }
    
    public static List<Usuarios_Model> getListUsuarios() throws Exception  {
        List<Usuarios_Model> listaUsuarios = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Usuarios('X', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
            ResultSet resultSet = statement.executeQuery();
            // Si el resultSet tiene resultados lo recorremos
            while (resultSet.next()) {
                // Obtenemos el valor del result set en base al nombre de la
                // columna
                long IdUsuario = resultSet.getLong("IdUsuario");
                String NombreUsuario = resultSet.getString("NombreUsuario");
                String ApellidoPaternoUsuario = resultSet.getString("ApellidoPaternoUsuario");
                String ApellidoMaternoUsuario = resultSet.getString("ApellidoMaternoUsuario");
                Date FechaNacimientoUsuario = resultSet.getDate("FechaNacimientoUsuario");
                String CorreoUsuario = resultSet.getString("CorreoUsuario");
                InputStream ImagenPerfilUsuario = resultSet.getBinaryStream("ImagenPerfilUsuario");
                String UsernameUsuario = resultSet.getString("UsernameUsuario");
                String PasswordUsuario = resultSet.getString("PasswordUsuario");
                Date FechaCreacionUsuario = resultSet.getDate("FechaCreacionUsuario");
                byte EstadoUsuario = resultSet.getByte("EstadoUsuario");
                // Agregamos el usuario a la lista
                listaUsuarios.add(new Usuarios_Model(
                		IdUsuario, NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario,
                		FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, 
                		UsernameUsuario, PasswordUsuario, FechaCreacionUsuario, EstadoUsuario
                		
                		));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaUsuarios;
    }

    
}
