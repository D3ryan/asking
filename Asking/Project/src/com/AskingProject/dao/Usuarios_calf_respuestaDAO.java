package com.AskingProject.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.AskingProject.models.Usuarios_calf_respuesta_Model;
import com.AskingProject.utils.DbConnection;

public class Usuarios_calf_respuestaDAO {

	public static int insertUpdateDeleteUCR(String pOpc, Usuarios_calf_respuesta_Model respuestaCalificada) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Usuarios_calf_respuesta( ?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, respuestaCalificada.getIdCalificacion());            
            statement.setLong(3, respuestaCalificada.getUsuarioCalifica());           
            statement.setLong(4, respuestaCalificada.getRespuestaCalificada());
            statement.setByte(5, respuestaCalificada.getUtilidadRespuestaCalificada()); 
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }

    public static List<Usuarios_calf_respuesta_Model> getUCR(String pOpc, Usuarios_calf_respuesta_Model respuestaCalificada) throws Exception  {
        List<Usuarios_calf_respuesta_Model> listaRespuestaCalificada = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Usuarios_calf_respuesta(?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, respuestaCalificada.getIdCalificacion());            
            statement.setLong(3, respuestaCalificada.getUsuarioCalifica());           
            statement.setLong(4, respuestaCalificada.getRespuestaCalificada());
            statement.setByte(5, respuestaCalificada.getUtilidadRespuestaCalificada());
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
            	long IdCalificacion = resultSet.getLong("IdCalificacion");
                long UsuarioCalifica = resultSet.getLong("UsuarioCalifica");
                long RespuestaCalificada = resultSet.getLong("RespuestaCalificada");
                byte UtilidadRespuestaCalificada = resultSet.getByte("UtilidadRespuestaCalificada");
                
                listaRespuestaCalificada.add(new Usuarios_calf_respuesta_Model(IdCalificacion, UsuarioCalifica, RespuestaCalificada, UtilidadRespuestaCalificada)
             		   );
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaRespuestaCalificada;
    }
	
	
	
	
	
	
	 public static int insertUCR(Usuarios_calf_respuesta_Model respuestaCalificada) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Usuarios_calf_respuesta('I', NULL, 	?, ?, ?);");
	            
	            statement.setLong(1, respuestaCalificada.getUsuarioCalifica());
	            
	            statement.setLong(2, respuestaCalificada.getRespuestaCalificada());
	            
	            statement.setLong(3, respuestaCalificada.getUtilidadRespuestaCalificada());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
 public static int updateUCR(Usuarios_calf_respuesta_Model respuestaCalificada) throws Exception {
 	Connection con = null;
 	CallableStatement statement = null;

     int rowsAffectted = 0;
     try {
         con = DbConnection.getConnection();
         
         statement = con.prepareCall("CALL sp_Usuarios_calf_respuesta('U', ?, 	?, ?, ?);");
        
         statement.setLong(1, respuestaCalificada.getIdCalificacion());
         
         statement.setLong(2, respuestaCalificada.getUsuarioCalifica());
         
         statement.setLong(3, respuestaCalificada.getRespuestaCalificada());
         
         statement.setLong(4, respuestaCalificada.getUtilidadRespuestaCalificada());
         
         
         rowsAffectted = statement.executeUpdate();
         
         
     } catch (SQLException ex) {
         System.out.println(ex.getMessage());
         rowsAffectted = 0;
     } finally {
     	statement.close();
         con.close();
     }
     return rowsAffectted;
 }
 
 public static int deleteUCR(Usuarios_calf_respuesta_Model respuestaCalificada) throws Exception {
 	Connection con = null;
 	CallableStatement statement = null;

     int rowsAffectted = 0;
     try {
         con = DbConnection.getConnection();
         
         statement = con.prepareCall("CALL sp_Usuarios_calf_respuesta('D', ?, NULL, NULL, NULL);");
        
         statement.setLong(1, respuestaCalificada.getIdCalificacion());
         
         rowsAffectted = statement.executeUpdate();
         
         
     } catch (SQLException ex) {
         System.out.println(ex.getMessage());
         rowsAffectted = 0;
     } finally {
     	statement.close();
         con.close();
     }
     return rowsAffectted;
 }

 public static Usuarios_calf_respuesta_Model getUCR(long pIdCalificacion) throws Exception  {
	 Usuarios_calf_respuesta_Model respuestaCalificadaElegida = null;
     Connection con = null;
     CallableStatement statement = null;

     try {
         con = DbConnection.getConnection();
         statement = con.prepareCall("CALL sp_Usuarios_calf_respuesta('S', ?, NULL, NULL, NULL);");
         statement.setLong(1, pIdCalificacion);
         
         ResultSet resultSet = statement.executeQuery();
         
         if (resultSet.next()) {
             long IdCalificacion = resultSet.getLong("IdCalificacion");
             long UsuarioCalifica = resultSet.getLong("UsuarioCalifica");
             long RespuestaCalificada = resultSet.getLong("RespuestaCalificada");
             byte UtilidadRespuestaCalificada = resultSet.getByte("UtilidadRespuestaCalificada");
             
             respuestaCalificadaElegida = new Usuarios_calf_respuesta_Model(IdCalificacion, UsuarioCalifica, RespuestaCalificada, UtilidadRespuestaCalificada);
         }
         else {
 			throw new Exception ("No hemos encontrado el usuario con c�digo = " + pIdCalificacion);
 		}
         
     } 
     catch (SQLException ex) {
         System.out.println(ex.getMessage());
     } 
     finally {
         statement.close();
         con.close();
     }

     return respuestaCalificadaElegida;
 }
 
 public static List<Usuarios_calf_respuesta_Model> getListUCR() throws Exception  {
     List<Usuarios_calf_respuesta_Model> listaRespuestaCalificada = new ArrayList<>();
     Connection con = null;
     CallableStatement statement = null;

     try {
         con = DbConnection.getConnection();
         statement = con.prepareCall("CALL sp_Usuarios_calf_pregunta('X', NULL, NULL, NULL, NULL);");
         ResultSet resultSet = statement.executeQuery();

         while (resultSet.next()) {
        	 long IdCalificacion = resultSet.getLong("IdCalificacion");
             long UsuarioCalifica = resultSet.getLong("UsuarioCalifica");
             long RespuestaCalificada = resultSet.getLong("RespuestaCalificada");
             byte UtilidadRespuestaCalificada = resultSet.getByte("UtilidadRespuestaCalificada");
             
             listaRespuestaCalificada.add(new Usuarios_calf_respuesta_Model(IdCalificacion, UsuarioCalifica, RespuestaCalificada, UtilidadRespuestaCalificada)
          		   );
         }
     } catch (SQLException ex) {
         System.out.println(ex.getMessage());
     } finally {
         statement.close();
         con.close();
     }

     return listaRespuestaCalificada;
 }

}
