package com.AskingProject.dao;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.AskingProject.models.Respuestas_Model;
import com.AskingProject.utils.DbConnection;

public class RespuestasDAO {

	public static int insertUpdateDeleteRespuesta(String pOpc, Respuestas_Model respuesta) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Respuestas(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, respuesta.getIdRespuesta());            
            statement.setString(3, respuesta.getTextoRespuesta());           
            statement.setBlob(4, respuesta.getImagenRespuesta());   
            
            if (respuesta.getFechaCreacionRespuesta() != null) {
            	java.util.Date utilDateFechaCrea = respuesta.getFechaCreacionRespuesta();
        		java.sql.Date fechaConvertidaFechaCrea = new java.sql.Date(utilDateFechaCrea.getTime());    		
                statement.setDate(5, fechaConvertidaFechaCrea);
			} else {
				statement.setDate(5, null);
			}

            statement.setByte(6, respuesta.getEstadoRespuesta());            
            statement.setByte(7, respuesta.getEditadoRespuesta());            
            statement.setLong(8, respuesta.getUsuario_Responde());            
            statement.setLong(9, respuesta.getPregunta_Respondida());
            statement.setInt(10, respuesta.getNumeroRespuestaPagina());  
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }

    public static List<Respuestas_Model> getRespuesta(String pOpc, Respuestas_Model respuesta) throws Exception  {
        List<Respuestas_Model> listaRespuestas = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Respuestas(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, respuesta.getIdRespuesta());            
            statement.setString(3, respuesta.getTextoRespuesta());           
            statement.setBlob(4, respuesta.getImagenRespuesta());   
            
			if (respuesta.getFechaCreacionRespuesta() != null) {
				java.util.Date utilDateFechaCrea = respuesta.getFechaCreacionRespuesta();
				java.sql.Date fechaConvertidaFechaCrea = new java.sql.Date(utilDateFechaCrea.getTime());
				statement.setDate(5, fechaConvertidaFechaCrea);
			} else {
				statement.setDate(5, null);
			}
			
            statement.setByte(6, respuesta.getEstadoRespuesta());            
            statement.setByte(7, respuesta.getEditadoRespuesta());            
            statement.setLong(8, respuesta.getUsuario_Responde());            
            statement.setLong(9, respuesta.getPregunta_Respondida());   
            statement.setInt(10, respuesta.getNumeroRespuestaPagina());
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                
            	long IdRespuesta = resultSet.getLong("IdRespuesta");
                String TextoRespuesta = resultSet.getString("TextoRespuesta");
                InputStream ImagenRespuesta = resultSet.getBinaryStream("ImagenRespuesta");
                
                Timestamp timeStampAux = resultSet.getTimestamp("FechaCreacionRespuesta");
				Date FechaCreacionRespuesta = null;
				if (timeStampAux != null)
                	FechaCreacionRespuesta = new Date(timeStampAux.getTime());
                
                byte EstadoRespuesta = resultSet.getByte("EstadoRespuesta");
                byte EditadoRespuesta = resultSet.getByte("EditadoRespuesta");
                long Usuario_Responde = resultSet.getLong("Usuario_Responde");
                long Pregunta_Respondida = resultSet.getLong("Pregunta_Respondida");
               
              
                String usernameUsuarioRespuesta = "";
                InputStream imagenPerfilUsuarioRespuesta = null;
                long cantidadVotosUtiles = -1;
                long cantidadVotosNoUtiles = -1;
                
                if (DbConnection.hasColumn(resultSet, "UsernameUsuario"))
                	usernameUsuarioRespuesta = resultSet.getString("UsernameUsuario");
                
                if (DbConnection.hasColumn(resultSet, "ImagenPerfilUsuario"))
                	imagenPerfilUsuarioRespuesta = resultSet.getBinaryStream("ImagenPerfilUsuario");
                
                if (DbConnection.hasColumn(resultSet, "CantVotosUtiles"))
                	cantidadVotosUtiles = resultSet.getLong("CantVotosUtiles");
                
                if (DbConnection.hasColumn(resultSet, "CantVotosNoUtiles"))
                	cantidadVotosNoUtiles = resultSet.getLong("CantVotosNoUtiles");
                
                // Agregamos el usuario a la lista
                
                listaRespuestas.add(new Respuestas_Model(IdRespuesta, TextoRespuesta, ImagenRespuesta, FechaCreacionRespuesta, 
    									EstadoRespuesta, EditadoRespuesta, Usuario_Responde, Pregunta_Respondida,
    									usernameUsuarioRespuesta, imagenPerfilUsuarioRespuesta,
    									cantidadVotosUtiles, cantidadVotosNoUtiles
                		));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaRespuestas;
    }
	
	
	 public static int insertRespuesta(Respuestas_Model respuesta) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Respuestas('I', NULL, 	?, ?, NULL, NULL, NULL, ?, ?, NULL);");
	           
	            statement.setString(1, respuesta.getTextoRespuesta());
	            
	            statement.setBlob(2, respuesta.getImagenRespuesta());
	            
	            statement.setLong(3, respuesta.getUsuario_Responde());
	            
	            statement.setLong(4, respuesta.getPregunta_Respondida());
	            

	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
	    public static int updateRespuesta(Respuestas_Model respuesta) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Respuestas('U', ?, ?, ?, NULL, NULL, NULL, NULL, NULL, NULL);");
	           
	            statement.setLong(1, respuesta.getIdRespuesta());
	            
	            statement.setString(2, respuesta.getTextoRespuesta());
	            
	            statement.setBlob(3, respuesta.getImagenRespuesta());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
	    public static int deleteRespuesta(Respuestas_Model respuesta) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Respuestas('D', ?, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
	           
	            statement.setLong(1, respuesta.getIdRespuesta());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	  
	    public static Respuestas_Model getRespuesta(long pIdRespuesta) throws Exception  {
	    	Respuestas_Model respuestaElegida = null;
	        Connection con = null;
	        CallableStatement statement = null;

	        try {
	            con = DbConnection.getConnection();
	            statement = con.prepareCall("CALL sp_Respuestas('X', NULL, 	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
	            statement.setLong(1, pIdRespuesta);
	            
	            ResultSet resultSet = statement.executeQuery();
	            
	            if (resultSet.next()) {
	                long IdRespuesta = resultSet.getLong("IdRespuesta");
	                String TextoRespuesta = resultSet.getString("TextoRespuesta");
	                InputStream ImagenRespuesta = resultSet.getBinaryStream("ImagenRespuesta");
	                Date FechaCreacionRespuesta = resultSet.getDate("FechaCreacionRespuesta");
	                byte EstadoRespuesta = resultSet.getByte("EstadoRespuesta");
	                byte EditadoRespuesta = resultSet.getByte("EditadoRespuesta");
	                long Usuario_Responde = resultSet.getLong("Usuario_Responde");
	                long Pregunta_Respondida = resultSet.getLong("Pregunta_Respondida");
	                // Agregamos el usuario a la lista
	                respuestaElegida = new Respuestas_Model(IdRespuesta, TextoRespuesta, ImagenRespuesta, FechaCreacionRespuesta, 
	                					EstadoRespuesta, EditadoRespuesta, Usuario_Responde, Pregunta_Respondida
	                		);
	            }
	            else {
	    			throw new Exception ("No hemos encontrado el usuario con c�digo = " + pIdRespuesta);
	    		}
	            
	        } 
	        catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        } 
	        finally {
	            statement.close();
	            con.close();
	        }

	        return respuestaElegida;
	    }
	    
	    public static List<Respuestas_Model> getListRespuesta() throws Exception  {
	        List<Respuestas_Model> listaRespuestas = new ArrayList<>();
	        Connection con = null;
	        CallableStatement statement = null;

	        try {
	            con = DbConnection.getConnection();
	            statement = con.prepareCall("CALL sp_Preguntas('X', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);");
	            ResultSet resultSet = statement.executeQuery();
	            // Si el resultSet tiene resultados lo recorremos
	            while (resultSet.next()) {
	                // Obtenemos el valor del result set en base al nombre de la
	                // columna
	            	long IdRespuesta = resultSet.getLong("IdRespuesta");
	                String TextoRespuesta = resultSet.getString("TextoRespuesta");
	                InputStream ImagenRespuesta = resultSet.getBinaryStream("ImagenRespuesta");
	                Date FechaCreacionRespuesta = resultSet.getDate("FechaCreacionRespuesta");
	                byte EstadoRespuesta = resultSet.getByte("EstadoRespuesta");
	                byte EditadoRespuesta = resultSet.getByte("EditadoRespuesta");
	                long Usuario_Responde = resultSet.getLong("Usuario_Responde");
	                long Pregunta_Respondida = resultSet.getLong("Pregunta_Respondida");
	                // Agregamos el usuario a la lista
	                
	                listaRespuestas.add(new Respuestas_Model(IdRespuesta, TextoRespuesta, ImagenRespuesta, FechaCreacionRespuesta, 
        									EstadoRespuesta, EditadoRespuesta, Usuario_Responde, Pregunta_Respondida
	                		));
	            }
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	        } finally {
	            statement.close();
	            con.close();
	        }

	        return listaRespuestas;
	    }

}
