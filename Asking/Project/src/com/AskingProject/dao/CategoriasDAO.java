package com.AskingProject.dao;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.AskingProject.models.Categorias_Model;
import com.AskingProject.utils.DbConnection;

public class CategoriasDAO {


	public static int insertUpdateDeleteCategoria(String pOpc, Categorias_Model categoria) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Categorias(?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setInt(2, categoria.getIdCategoria());            
            statement.setString(3, categoria.getTituloCategoria());   
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }

    public static List<Categorias_Model> getCategorias(String pOpc, Categorias_Model categoria) throws Exception  {
        List<Categorias_Model> listaCategorias = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Categorias(?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setInt(2, categoria.getIdCategoria());            
            statement.setString(3, categoria.getTituloCategoria());    
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                
            	int IdCategoria = resultSet.getInt("IdCategoria");
                String TituloCategoria = resultSet.getString("TituloCategoria");
                
                
                listaCategorias.add(new Categorias_Model(IdCategoria, TituloCategoria)
                		);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaCategorias;
    }
	
    
    
	public static int insertCategoria(Categorias_Model categoria) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Categorias('I', NULL, 	?);");
	           
	            statement.setString(1, categoria.getTituloCategoria());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
    public static int updateCategoria(Categorias_Model categoria) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Categorias('U', ?, ?);");
           
            statement.setInt(1, categoria.getIdCategoria());
            
            statement.setString(2, categoria.getTituloCategoria());
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
    
    public static int deleteCategoria(Categorias_Model categoria) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Categorias('D', ?, NULL);");
           
            statement.setInt(1, categoria.getIdCategoria());
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }
  
    public static Categorias_Model getCategoria(long pIdCategoria) throws Exception  {
    	Categorias_Model categoriaElegida = null;
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Categorias('S', ?, NULL);");
            statement.setLong(1, pIdCategoria);
            
            ResultSet resultSet = statement.executeQuery();
            
            if (resultSet.next()) {
                int IdCategoria = resultSet.getInt("IdCategoria");
                String TituloCategoria = resultSet.getString("TituloCategoria");
                
                categoriaElegida = new Categorias_Model(IdCategoria, TituloCategoria);
            }
            else {
    			throw new Exception ("No hemos encontrado el usuario con c�digo = " + pIdCategoria);
    		}
            
        } 
        catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } 
        finally {
            statement.close();
            con.close();
        }

        return categoriaElegida;
    }
    
    public static List<Categorias_Model> getListCategoria() throws Exception  {
        List<Categorias_Model> listaCategorias = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Categorias('X', NULL, NULL);");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
            	int IdCategoria = resultSet.getInt("IdCategoria");
                String TituloCategoria = resultSet.getString("TituloCategoria");
                
                
                listaCategorias.add(new Categorias_Model(IdCategoria, TituloCategoria)
                		);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaCategorias;
    }

}
