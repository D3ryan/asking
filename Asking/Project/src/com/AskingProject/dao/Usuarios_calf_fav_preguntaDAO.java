package com.AskingProject.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.AskingProject.models.Usuarios_calf_fav_pregunta_Model;
import com.AskingProject.utils.DbConnection;

public class Usuarios_calf_fav_preguntaDAO {


	public static int insertUpdateDeleteUCFP(String pOpc, Usuarios_calf_fav_pregunta_Model preguntaCalificada) throws Exception {
    	Connection con = null;
    	CallableStatement statement = null;

        int rowsAffectted = 0;
        try {
            con = DbConnection.getConnection();
            
            statement = con.prepareCall("CALL sp_Usuarios_calf_fav_pregunta(?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, preguntaCalificada.getIdCalificacion());            
            statement.setLong(3, preguntaCalificada.getUsuarioCalifica());           
            statement.setLong(4, preguntaCalificada.getPreguntaCalificada());   
            
            rowsAffectted = statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            rowsAffectted = 0;
        } finally {
        	statement.close();
            con.close();
        }
        return rowsAffectted;
    }

    public static List<Usuarios_calf_fav_pregunta_Model> getUCFP(String pOpc, Usuarios_calf_fav_pregunta_Model preguntaCalificada) throws Exception  {
        List<Usuarios_calf_fav_pregunta_Model> listaPreguntaCalificada = new ArrayList<>();
        Connection con = null;
        CallableStatement statement = null;

        try {
            con = DbConnection.getConnection();
            statement = con.prepareCall("CALL sp_Usuarios_calf_fav_pregunta(?, ?, ?, ?);");
            
            statement.setString(1, pOpc);            
            statement.setLong(2, preguntaCalificada.getIdCalificacion());            
            statement.setLong(3, preguntaCalificada.getUsuarioCalifica());           
            statement.setLong(4, preguntaCalificada.getPreguntaCalificada());     
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                
            	 long IdCalificacion = resultSet.getLong("IdCalificacion");
                 long UsuarioCalifica = resultSet.getLong("UsuarioCalifica");
                 long PreguntaCalificada = resultSet.getLong("PreguntaCalificada");
                 
                 listaPreguntaCalificada.add(new Usuarios_calf_fav_pregunta_Model(IdCalificacion, UsuarioCalifica, PreguntaCalificada)
              		   );
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            statement.close();
            con.close();
        }

        return listaPreguntaCalificada;
    }
	
	
	
    public static int insertUCFP(Usuarios_calf_fav_pregunta_Model preguntaCalificada) throws Exception {
	    	Connection con = null;
	    	CallableStatement statement = null;

	        int rowsAffectted = 0;
	        try {
	            con = DbConnection.getConnection();
	            
	            statement = con.prepareCall("CALL sp_Usuarios_calf_fav_pregunta('I', NULL, 	?, ?);");
	           
	            statement.setLong(1, preguntaCalificada.getUsuarioCalifica());
	            
	            statement.setLong(2, preguntaCalificada.getPreguntaCalificada());
	            
	            rowsAffectted = statement.executeUpdate();
	            
	            
	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            rowsAffectted = 0;
	        } finally {
	        	statement.close();
	            con.close();
	        }
	        return rowsAffectted;
	    }
	    
    public static int updateUCFP(Usuarios_calf_fav_pregunta_Model preguntaCalificada) throws Exception {
   	Connection con = null;
   	CallableStatement statement = null;

       int rowsAffectted = 0;
       try {
           con = DbConnection.getConnection();
           
           statement = con.prepareCall("CALL sp_Usuarios_calf_fav_pregunta('U', ?, ?, ?);");
          
           statement.setLong(1, preguntaCalificada.getIdCalificacion());
           
           statement.setLong(2, preguntaCalificada.getUsuarioCalifica());
           
           statement.setLong(3, preguntaCalificada.getPreguntaCalificada());
           
           
           rowsAffectted = statement.executeUpdate();
           
           
       } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           rowsAffectted = 0;
       } finally {
       	statement.close();
           con.close();
       }
       return rowsAffectted;
   }
   
    public static int deleteUCFP(Usuarios_calf_fav_pregunta_Model preguntaCalificada) throws Exception {
   	Connection con = null;
   	CallableStatement statement = null;

       int rowsAffectted = 0;
       try {
           con = DbConnection.getConnection();
           
           statement = con.prepareCall("CALL sp_Usuarios_calf_fav_pregunta('D', ?, NULL, NULL);");
          
           statement.setLong(1, preguntaCalificada.getIdCalificacion());
           
           rowsAffectted = statement.executeUpdate();
           
           
       } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           rowsAffectted = 0;
       } finally {
       	statement.close();
           con.close();
       }
       return rowsAffectted;
   }
 
    public static Usuarios_calf_fav_pregunta_Model getUCFP(long pIdCalificacion) throws Exception  {
	   Usuarios_calf_fav_pregunta_Model preguntaCalificadaElegida = null;
       Connection con = null;
       CallableStatement statement = null;

       try {
           con = DbConnection.getConnection();
           statement = con.prepareCall("CALL sp_Usuarios_calf_fav_pregunta('S', ?, NULL, NULL);");
           statement.setLong(1, pIdCalificacion);
           
           ResultSet resultSet = statement.executeQuery();
           
           if (resultSet.next()) {
               long IdCalificacion = resultSet.getLong("IdCalificacion");
               long UsuarioCalifica = resultSet.getLong("UsuarioCalifica");
               long PreguntaCalificada = resultSet.getLong("PreguntaCalificada");
               
               preguntaCalificadaElegida = new Usuarios_calf_fav_pregunta_Model(IdCalificacion, UsuarioCalifica, PreguntaCalificada);
           }
           else {
   			throw new Exception ("No hemos encontrado el usuario con c�digo = " + pIdCalificacion);
   		}
           
       } 
       catch (SQLException ex) {
           System.out.println(ex.getMessage());
       } 
       finally {
           statement.close();
           con.close();
       }

       return preguntaCalificadaElegida;
   }
   
    public static List<Usuarios_calf_fav_pregunta_Model> getListUCFP() throws Exception  {
       List<Usuarios_calf_fav_pregunta_Model> listaPreguntaCalificada = new ArrayList<>();
       Connection con = null;
       CallableStatement statement = null;

       try {
           con = DbConnection.getConnection();
           statement = con.prepareCall("CALL sp_Usuarios_calf_fav_pregunta('X', NULL, NULL, NULL);");
           ResultSet resultSet = statement.executeQuery();

           while (resultSet.next()) {
        	   long IdCalificacion = resultSet.getLong("IdCalificacion");
               long UsuarioCalifica = resultSet.getLong("UsuarioCalifica");
               long PreguntaCalificada = resultSet.getLong("PreguntaCalificada");
               
               listaPreguntaCalificada.add(new Usuarios_calf_fav_pregunta_Model(IdCalificacion, UsuarioCalifica, PreguntaCalificada)
            		   );
           }
       } catch (SQLException ex) {
           System.out.println(ex.getMessage());
       } finally {
           statement.close();
           con.close();
       }

       return listaPreguntaCalificada;
   }

}
