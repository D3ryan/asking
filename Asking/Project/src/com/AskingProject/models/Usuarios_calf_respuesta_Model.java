package com.AskingProject.models;

public class Usuarios_calf_respuesta_Model {
	private long IdCalificacion;
	 private long UsuarioCalifica; 
	 private long RespuestaCalificada;
	 private byte UtilidadRespuestaCalificada;
	 
	 
	 
	 
	public Usuarios_calf_respuesta_Model(long idCalificacion, long usuarioCalifica, long respuestaCalificada,
			byte utilidadRespuestaCalificada) {
		super();
		IdCalificacion = idCalificacion;
		UsuarioCalifica = usuarioCalifica;
		RespuestaCalificada = respuestaCalificada;
		UtilidadRespuestaCalificada = utilidadRespuestaCalificada;
	}
	
	
	
	
	public Usuarios_calf_respuesta_Model(long idCalificacion) {
		super();
		IdCalificacion = idCalificacion;
	}




	public Usuarios_calf_respuesta_Model(long usuarioCalifica, long respuestaCalificada,
			byte utilidadRespuestaCalificada) {
		super();
		UsuarioCalifica = usuarioCalifica;
		RespuestaCalificada = respuestaCalificada;
		UtilidadRespuestaCalificada = utilidadRespuestaCalificada;
	}
	
	public Usuarios_calf_respuesta_Model(long usuarioCalifica, long respuestaCalificada) {
		super();
		UsuarioCalifica = usuarioCalifica;
		RespuestaCalificada = respuestaCalificada;
	}



	public Usuarios_calf_respuesta_Model() {
		super();
	}




	public long getIdCalificacion() {
		return IdCalificacion;
	}
	public void setIdCalificacion(long idCalificacion) {
		IdCalificacion = idCalificacion;
	}
	public long getUsuarioCalifica() {
		return UsuarioCalifica;
	}
	public void setUsuarioCalifica(long usuarioCalifica) {
		UsuarioCalifica = usuarioCalifica;
	}
	public long getRespuestaCalificada() {
		return RespuestaCalificada;
	}
	public void setRespuestaCalificada(long respuestaCalificada) {
		RespuestaCalificada = respuestaCalificada;
	}
	public byte getUtilidadRespuestaCalificada() {
		return UtilidadRespuestaCalificada;
	}
	public void setUtilidadRespuestaCalificada(byte utilidadRespuestaCalificada) {
		UtilidadRespuestaCalificada = utilidadRespuestaCalificada;
	}
	 
	 
	 
}
