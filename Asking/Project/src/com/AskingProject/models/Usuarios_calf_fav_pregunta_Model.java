package com.AskingProject.models;

public class Usuarios_calf_fav_pregunta_Model {
	private long IdCalificacion;
	 private long UsuarioCalifica; 
	 private long PreguntaCalificada;
	 
	 
	 
	 
	public Usuarios_calf_fav_pregunta_Model(long idCalificacion, long usuarioCalifica, long preguntaCalificada) {
		super();
		IdCalificacion = idCalificacion;
		UsuarioCalifica = usuarioCalifica;
		PreguntaCalificada = preguntaCalificada;
	}
	
	
	
	
	public Usuarios_calf_fav_pregunta_Model(long idCalificacion) {
		super();
		IdCalificacion = idCalificacion;
	}




	public Usuarios_calf_fav_pregunta_Model(long usuarioCalifica, long preguntaCalificada) {
		super();
		UsuarioCalifica = usuarioCalifica;
		PreguntaCalificada = preguntaCalificada;
	}




	public Usuarios_calf_fav_pregunta_Model() {
		super();
	}




	public long getIdCalificacion() {
		return IdCalificacion;
	}
	public void setIdCalificacion(long idCalificacion) {
		IdCalificacion = idCalificacion;
	}
	public long getUsuarioCalifica() {
		return UsuarioCalifica;
	}
	public void setUsuarioCalifica(long usuarioCalifica) {
		UsuarioCalifica = usuarioCalifica;
	}
	public long getPreguntaCalificada() {
		return PreguntaCalificada;
	}
	public void setPreguntaCalificada(long preguntaCalificada) {
		PreguntaCalificada = preguntaCalificada;
	}
	 
	 
	 
}
