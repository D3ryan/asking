package com.AskingProject.models;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Preguntas_Model {
	 private long IdPregunta;
	 private String TituloPregunta; 
	 private String DescripcionPregunta;
	 private InputStream ImagenPregunta; 
	 private Date FechaCreacionPregunta;
	 private byte EstadoPregunta; 
	 private byte EditadoPregunta;
	    
	 private long Usuario_Pregunta; 
	 private long Respuesta_Correcta;
	 private int Categoria_Pregunta;
	 
	 private int NumeroPreguntaPagina;
	 
	 private Date PreguntaFechaDesde;   
	 private Date PreguntaFechaHasta;
	 
	 private long CantidadVotosUtiles;
	 private long CantidadVotosNoUtiles;
	 private long CantidadVotosFavoritos;
	 
	 private String UsernameUsuarioPregunta; 
	 private InputStream ImagenPerfilUsuarioPregunta;
	 private byte EstadoUsuarioPregunta;
	

	private String TituloCategoriaPregunta;




	public Preguntas_Model(long idPregunta, String tituloPregunta, String descripcionPregunta, InputStream imagenPregunta,
			Date fechaCreacionPregunta, byte estadoPregunta, byte editadoPregunta, long usuario_Pregunta,
			long respuesta_Correcta, int categoria_Pregunta) {
		super();
		IdPregunta = idPregunta;
		TituloPregunta = tituloPregunta;
		DescripcionPregunta = descripcionPregunta;
		ImagenPregunta = imagenPregunta;
		FechaCreacionPregunta = fechaCreacionPregunta;
		EstadoPregunta = estadoPregunta;
		EditadoPregunta = editadoPregunta;
		Usuario_Pregunta = usuario_Pregunta;
		Respuesta_Correcta = respuesta_Correcta;
		Categoria_Pregunta = categoria_Pregunta;
	}
	
	
	
	
	
	
	
	public Preguntas_Model(long idPregunta) {
		super();
		IdPregunta = idPregunta;
	}


	public Preguntas_Model(String tituloPregunta, String descripcionPregunta, InputStream imagenPregunta,
			Date fechaCreacionPregunta, byte estadoPregunta, byte editadoPregunta, long usuario_Pregunta,
			long respuesta_Correcta, int categoria_Pregunta) {
		super();
		TituloPregunta = tituloPregunta;
		DescripcionPregunta = descripcionPregunta;
		ImagenPregunta = imagenPregunta;
		FechaCreacionPregunta = fechaCreacionPregunta;
		EstadoPregunta = estadoPregunta;
		EditadoPregunta = editadoPregunta;
		Usuario_Pregunta = usuario_Pregunta;
		Respuesta_Correcta = respuesta_Correcta;
		Categoria_Pregunta = categoria_Pregunta;
	}


	public Preguntas_Model() {
		super();
	}

	

	public Preguntas_Model(int preguntaInicioPagina) {
		super();
		this.NumeroPreguntaPagina = preguntaInicioPagina;
	}



	



	public Preguntas_Model(long idPregunta, long respuesta_Correcta) {
		super();
		IdPregunta = idPregunta;
		Respuesta_Correcta = respuesta_Correcta;
	}







	public Preguntas_Model(String tituloPregunta, String descripcionPregunta, InputStream imagenPregunta,
			long usuario_Pregunta, int categoria_Pregunta) {
		super();
		TituloPregunta = tituloPregunta;
		DescripcionPregunta = descripcionPregunta;
		ImagenPregunta = imagenPregunta;
		Usuario_Pregunta = usuario_Pregunta;
		Categoria_Pregunta = categoria_Pregunta;
	}



	



	public Preguntas_Model(long idPregunta, String tituloPregunta, String descripcionPregunta,
			InputStream imagenPregunta, long usuario_Pregunta, int categoria_Pregunta) {
		super();
		IdPregunta = idPregunta;
		TituloPregunta = tituloPregunta;
		DescripcionPregunta = descripcionPregunta;
		ImagenPregunta = imagenPregunta;
		Usuario_Pregunta = usuario_Pregunta;
		Categoria_Pregunta = categoria_Pregunta;
	}







	public Preguntas_Model(int numeroPregunta, String tituloPregunta, int categoria_Pregunta, Date preguntaFechaDesde,
			Date preguntaFechaHasta, long cantidadVotosUtiles, long cantidadVotosFavoritos) {
		super();
		this.NumeroPreguntaPagina = numeroPregunta;
		TituloPregunta = tituloPregunta;
		Categoria_Pregunta = categoria_Pregunta;
		this.PreguntaFechaDesde = preguntaFechaDesde;
		this.PreguntaFechaHasta = preguntaFechaHasta;
		CantidadVotosUtiles = cantidadVotosUtiles;
		CantidadVotosFavoritos = cantidadVotosFavoritos;
	}







	public Preguntas_Model(long idPregunta, String tituloPregunta, String descripcionPregunta,
			InputStream imagenPregunta, Date fechaCreacionPregunta, byte estadoPregunta, byte editadoPregunta,
			long usuario_Pregunta, long respuesta_Correcta, int categoria_Pregunta, 
			String usernameUsuarioPregunta, InputStream imagenPerfilUsuarioPregunta, byte estadoUsuarioPregunta, String tituloCategoriaPregunta,
			long cantidadVotosUtiles, long cantidadVotosNoUtiles, long cantidadVotosFavoritos) {
		super();
		IdPregunta = idPregunta;
		TituloPregunta = tituloPregunta;
		DescripcionPregunta = descripcionPregunta;
		ImagenPregunta = imagenPregunta;
		FechaCreacionPregunta = fechaCreacionPregunta;
		EstadoPregunta = estadoPregunta;
		EditadoPregunta = editadoPregunta;
		Usuario_Pregunta = usuario_Pregunta;
		Respuesta_Correcta = respuesta_Correcta;
		Categoria_Pregunta = categoria_Pregunta;
		CantidadVotosUtiles = cantidadVotosUtiles;
		CantidadVotosNoUtiles = cantidadVotosNoUtiles;
		CantidadVotosFavoritos = cantidadVotosFavoritos;
		UsernameUsuarioPregunta = usernameUsuarioPregunta;
		ImagenPerfilUsuarioPregunta = imagenPerfilUsuarioPregunta;
		EstadoUsuarioPregunta = estadoUsuarioPregunta;
		TituloCategoriaPregunta = tituloCategoriaPregunta;
	}

	





	public long getIdPregunta() {
		return IdPregunta;
	}
	public void setIdPregunta(long idPregunta) {
		IdPregunta = idPregunta;
	}
	public String getTituloPregunta() {
		return TituloPregunta;
	}
	public void setTituloPregunta(String tituloPregunta) {
		TituloPregunta = tituloPregunta;
	}
	public String getDescripcionPregunta() {
		return DescripcionPregunta;
	}
	public void setDescripcionPregunta(String descripcionPregunta) {
		DescripcionPregunta = descripcionPregunta;
	}
	public InputStream getImagenPregunta() {
		return ImagenPregunta;
	}
	public InputStream isImagenPregunta() {
		if (ImagenPregunta != null) {
			PushbackInputStream pushbackInputStream = new PushbackInputStream(ImagenPregunta);
		    int bytes = 0;
		    try {
				bytes = pushbackInputStream.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    if (bytes == -1) {
		    	return null;
		    }
		    else {
		    	return ImagenPregunta;
		    }
			
		}
		else {
		    return null;
		}
	}
	public void setImagenPregunta(InputStream imagenPregunta) {
		ImagenPregunta = imagenPregunta;
	}
	public Date getFechaCreacionPregunta() {
		return FechaCreacionPregunta;
	}
	public String getFechaCreacionPreguntaToString() {
		return FechaCreacionPregunta.toGMTString();
	}
	public void setFechaCreacionPregunta(Date fechaCreacionPregunta) {
		FechaCreacionPregunta = fechaCreacionPregunta;
	}
	public byte getEstadoPregunta() {
		return EstadoPregunta;
	}
	public void setEstadoPregunta(byte estadoPregunta) {
		EstadoPregunta = estadoPregunta;
	}
	public byte getEditadoPregunta() {
		return EditadoPregunta;
	}
	public void setEditadoPregunta(byte editadoPregunta) {
		EditadoPregunta = editadoPregunta;
	}
	public long getUsuario_Pregunta() {
		return Usuario_Pregunta;
	}
	public void setUsuario_Pregunta(long usuario_Pregunta) {
		Usuario_Pregunta = usuario_Pregunta;
	}
	public long getRespuesta_Correcta() {
		return Respuesta_Correcta;
	}
	public void setRespuesta_Correcta(long respuesta_Correcta) {
		Respuesta_Correcta = respuesta_Correcta;
	}
	public int getCategoria_Pregunta() {
		return Categoria_Pregunta;
	}
	public void setCategoria_Pregunta(int categoria_Pregunta) {
		Categoria_Pregunta = categoria_Pregunta;
	}


	public int getPreguntaInicioPagina() {
		return NumeroPreguntaPagina;
	}


	public void setPreguntaInicioPagina(int preguntaInicioPagina) {
		this.NumeroPreguntaPagina = preguntaInicioPagina;
	} 
	 
	public long getCantidadVotosFavoritos() {
		return CantidadVotosFavoritos;
	}


	public String getCantidadVotosFavoritosString() {
		if (CantidadVotosFavoritos != -1) {
			return String.valueOf(CantidadVotosFavoritos);
		}
		else {
			return "";
		}
		
	}


	public void setCantidadVotosFavoritos(long cantidadVotosFavoritos) {
		CantidadVotosFavoritos = cantidadVotosFavoritos;
	}



	public int getNumeroPregunta() {
		return NumeroPreguntaPagina;
	}

	public void setNumeroPregunta(int numeroPregunta) {
		this.NumeroPreguntaPagina = numeroPregunta;
	}


	public long getCantidadVotosUtiles() {
		return CantidadVotosUtiles;
	}
	
	public String getCantidadVotosUtilesString() {
		if (CantidadVotosUtiles != -1) {
			return String.valueOf(CantidadVotosUtiles);
		}
		else {
			return "";
		}
	}
	

	public void setCantidadVotosUtiles(long cantidadVotosUtiles) {
		CantidadVotosUtiles = cantidadVotosUtiles;
	}
	
	public long getCantidadVotosNoUtiles() {
		return CantidadVotosNoUtiles;
	}
	

	public void setCantidadVotosNoUtiles(long cantidadVotosNoUtiles) {
		CantidadVotosNoUtiles = cantidadVotosNoUtiles;
	}


	public Date getPreguntaFechaDesde() {
		return PreguntaFechaDesde;
	}
	
	public String getPreguntaFechaDesdeString() {
		String format = "";
		if (PreguntaFechaDesde != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			format = formatter.format(PreguntaFechaDesde);
		}
		return format;	    
	} 

	public void setPreguntaFechaDesde(Date preguntaFechaDesde) {
		this.PreguntaFechaDesde = preguntaFechaDesde;
	}

	public Date getPreguntaFechaHasta() {
		return PreguntaFechaHasta;
	}

	public String getPreguntaFechaHastaString() {
		String format = "";
		if (PreguntaFechaHasta != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			format = formatter.format(PreguntaFechaHasta);
		}
		return format;	    
	} 

	public void setPreguntaFechaHasta(Date preguntaFechaHasta) {
		this.PreguntaFechaHasta = preguntaFechaHasta;
	}

	
	public String getUsernameUsuarioPregunta() {
		return UsernameUsuarioPregunta;
	}

	
	public void setUsernameUsuarioPregunta(String usernameUsuarioPregunta) {
		UsernameUsuarioPregunta = usernameUsuarioPregunta;
	}

	
	public InputStream getImagenPerfilUsuarioPregunta() throws IOException {
		PushbackInputStream pushbackInputStream = new PushbackInputStream(ImagenPerfilUsuarioPregunta);
	    int bytes;
	    bytes = pushbackInputStream.read();
	    if (bytes == -1) {
	      return null;
	    }
	    else {
	    	return ImagenPerfilUsuarioPregunta;
	    }
	}

	
	public void setImagenPerfilUsuarioPregunta(InputStream imagenPerfilUsuarioPregunta) {
		ImagenPerfilUsuarioPregunta = imagenPerfilUsuarioPregunta;
	}
	
	
	public byte getEstadoUsuarioPregunta() {
		return EstadoUsuarioPregunta;
	}

	
	 public void setEstadoUsuarioPregunta(byte estadoUsuarioPregunta) {
		EstadoUsuarioPregunta = estadoUsuarioPregunta;
	 }
	
	 
	public String getTituloCategoriaPregunta() {                               
		return TituloCategoriaPregunta;                                        
	}                                                                          
	
	
	public void setTituloCategoriaPregunta(String tituloCategoriaPregunta) {   
		TituloCategoriaPregunta = tituloCategoriaPregunta;                     
	}                                                                          
	                                                                           
	                                                                           
	                                                                           
	 
}
