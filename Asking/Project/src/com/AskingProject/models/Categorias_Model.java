package com.AskingProject.models;

public class Categorias_Model {
	private int IdCategoria;
	 private String TituloCategoria;
	 
	public Categorias_Model(int idCategoria, String tituloCategoria) {
		super();
		IdCategoria = idCategoria;
		TituloCategoria = tituloCategoria;
	}

	public Categorias_Model(int idCategoria) {
		super();
		IdCategoria = idCategoria;
	}

	public Categorias_Model(String tituloCategoria) {
		super();
		TituloCategoria = tituloCategoria;
	}

	public int getIdCategoria() {
		return IdCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		IdCategoria = idCategoria;
	}

	public String getTituloCategoria() {
		return TituloCategoria;
	}

	public void setTituloCategoria(String tituloCategoria) {
		TituloCategoria = tituloCategoria;
	} 
	 
	 
}
