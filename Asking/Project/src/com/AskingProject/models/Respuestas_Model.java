package com.AskingProject.models;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Respuestas_Model {
	private long IdRespuesta;
	 private String TextoRespuesta; 
	 private InputStream ImagenRespuesta;
	 private Date FechaCreacionRespuesta; 
	 private byte EstadoRespuesta; 
	 private byte EditadoRespuesta;
	    
	 private long Usuario_Responde; 
	 private long Pregunta_Respondida;

	 private int NumeroRespuestaPagina;
	 
	 private String UsernameUsuarioResponde; 
	 private InputStream ImagenPerfilUsuarioResponde;
	
	 private long CantidadVotosUtiles;
	 private long CantidadVotosNoUtiles;
	 
	 



	public Respuestas_Model(long idRespuesta, String textoRespuesta, InputStream imagenRespuesta, Date fechaCreacionRespuesta,
			byte estadoRespuesta, byte editadoRespuesta, long usuario_Responde, long pregunta_Respondida) {
		super();
		IdRespuesta = idRespuesta;
		TextoRespuesta = textoRespuesta;
		ImagenRespuesta = imagenRespuesta;
		FechaCreacionRespuesta = fechaCreacionRespuesta;
		EstadoRespuesta = estadoRespuesta;
		EditadoRespuesta = editadoRespuesta;
		Usuario_Responde = usuario_Responde;
		Pregunta_Respondida = pregunta_Respondida;
	}
	
	public Respuestas_Model(long idRespuesta, String textoRespuesta, InputStream imagenRespuesta, long usuario_Responde, long pregunta_Respondida) {
		super();
		IdRespuesta = idRespuesta;
		TextoRespuesta = textoRespuesta;
		ImagenRespuesta = imagenRespuesta;
		Usuario_Responde = usuario_Responde;
		Pregunta_Respondida = pregunta_Respondida;
	}
	
	
	
	public Respuestas_Model(long idRespuesta) {
		super();
		IdRespuesta = idRespuesta;
	}


	public Respuestas_Model(String textoRespuesta, InputStream imagenRespuesta, Date fechaCreacionRespuesta,
			byte estadoRespuesta, byte editadoRespuesta, long usuario_Responde, long pregunta_Respondida) {
		super();
		TextoRespuesta = textoRespuesta;
		ImagenRespuesta = imagenRespuesta;
		FechaCreacionRespuesta = fechaCreacionRespuesta;
		EstadoRespuesta = estadoRespuesta;
		EditadoRespuesta = editadoRespuesta;
		Usuario_Responde = usuario_Responde;
		Pregunta_Respondida = pregunta_Respondida;
	}




	public Respuestas_Model(String textoRespuesta, InputStream imagenRespuesta, long usuario_Responde,
			long pregunta_Respondida) {
		super();
		TextoRespuesta = textoRespuesta;
		ImagenRespuesta = imagenRespuesta;
		Usuario_Responde = usuario_Responde;
		Pregunta_Respondida = pregunta_Respondida;
	}

	


	public Respuestas_Model() {
		super();
	}




	public Respuestas_Model(int numeroRespuestaPagina) {
		super();
		NumeroRespuestaPagina = numeroRespuestaPagina;
	}

	public Respuestas_Model(long pregunta_Respondida, int numeroRespuestaPagina) {
		super();
		Pregunta_Respondida = pregunta_Respondida;
		NumeroRespuestaPagina = numeroRespuestaPagina;
	}

	
	
	

	public Respuestas_Model(long idRespuesta, String textoRespuesta, InputStream imagenRespuesta,
			Date fechaCreacionRespuesta, byte estadoRespuesta, byte editadoRespuesta, long usuario_Responde,
			long pregunta_Respondida, String usernameUsuarioResponde, InputStream imagenPerfilUsuarioResponde,
			long cantidadVotosUtiles, long cantidadVotosNoUtiles) {
		super();
		IdRespuesta = idRespuesta;
		TextoRespuesta = textoRespuesta;
		ImagenRespuesta = imagenRespuesta;
		FechaCreacionRespuesta = fechaCreacionRespuesta;
		EstadoRespuesta = estadoRespuesta;
		EditadoRespuesta = editadoRespuesta;
		Usuario_Responde = usuario_Responde;
		Pregunta_Respondida = pregunta_Respondida;
		UsernameUsuarioResponde = usernameUsuarioResponde;
		ImagenPerfilUsuarioResponde = imagenPerfilUsuarioResponde;
		CantidadVotosUtiles = cantidadVotosUtiles;
		CantidadVotosNoUtiles = cantidadVotosNoUtiles;
	}




	public long getIdRespuesta() {
		return IdRespuesta;
	}
	public void setIdRespuesta(long idRespuesta) {
		IdRespuesta = idRespuesta;
	}
	public String getTextoRespuesta() {
		return TextoRespuesta;
	}
	public void setTextoRespuesta(String textoRespuesta) {
		TextoRespuesta = textoRespuesta;
	}
	public InputStream getImagenRespuesta() {
		return ImagenRespuesta;
	}
	public InputStream isImagenRespuesta() {
		if (ImagenRespuesta != null) {
			PushbackInputStream pushbackInputStream = new PushbackInputStream(ImagenRespuesta);
		    int bytes = 0;
		    try {
				bytes = pushbackInputStream.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    if (bytes == -1) {
		    	return null;
		    }
		    else {
		    	return ImagenRespuesta;
		    }
			
		}
		else {
		    return null;
		}
	}
	public void setImagenRespuesta(InputStream imagenRespuesta) {
		ImagenRespuesta = imagenRespuesta;
	}
	public Date getFechaCreacionRespuesta() {
		return FechaCreacionRespuesta;
	}
	public String getFechaCreacionRespuestaToString() {
		return FechaCreacionRespuesta.toGMTString();
	}
	public void setFechaCreacionRespuesta(Date fechaCreacionRespuesta) {
		FechaCreacionRespuesta = fechaCreacionRespuesta;
	}
	public byte getEstadoRespuesta() {
		return EstadoRespuesta;
	}
	public void setEstadoRespuesta(byte estadoRespuesta) {
		EstadoRespuesta = estadoRespuesta;
	}
	public byte getEditadoRespuesta() {
		return EditadoRespuesta;
	}
	public void setEditadoRespuesta(byte editadoRespuesta) {
		EditadoRespuesta = editadoRespuesta;
	}
	public long getUsuario_Responde() {
		return Usuario_Responde;
	}
	public void setUsuario_Responde(long usuario_Responde) {
		Usuario_Responde = usuario_Responde;
	}
	public long getPregunta_Respondida() {
		return Pregunta_Respondida;
	}
	public void setPregunta_Respondida(long pregunta_Respondida) {
		Pregunta_Respondida = pregunta_Respondida;
	}




	public int getNumeroRespuestaPagina() {
		return NumeroRespuestaPagina;
	}




	public void setNumeroRespuestaPagina(int numeroRespuestaPagina) {
		NumeroRespuestaPagina = numeroRespuestaPagina;
	}
	
	
	public String getUsernameUsuarioResponde() {
		return UsernameUsuarioResponde;
	}


	public void setUsernameUsuarioResponde(String usernameUsuarioResponde) {
		UsernameUsuarioResponde = usernameUsuarioResponde;
	}


	public InputStream getImagenPerfilUsuarioResponde() throws IOException {
		PushbackInputStream pushbackInputStream = new PushbackInputStream(ImagenPerfilUsuarioResponde);
	    int bytes;
	    bytes = pushbackInputStream.read();
	    if (bytes == -1) {
	      return null;
	    }
	    else {
	    	return ImagenPerfilUsuarioResponde;
	    }
	}


	public void setImagenPerfilUsuarioResponde(InputStream imagenPerfilUsuarioResponde) {
		ImagenPerfilUsuarioResponde = imagenPerfilUsuarioResponde;
	}
	

	public long getCantidadVotosUtiles() {
		return CantidadVotosUtiles;
	}

	public void setCantidadVotosUtiles(long cantidadVotosUtiles) {
		CantidadVotosUtiles = cantidadVotosUtiles;
	}

	public long getCantidadVotosNoUtiles() {
		return CantidadVotosNoUtiles;
	}

	public void setCantidadVotosNoUtiles(long cantidadVotosNoUtiles) {
		CantidadVotosNoUtiles = cantidadVotosNoUtiles;
	}

	 
}
