/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.AskingProject.models;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Date;

/**
 *
 * @author magoc
 */
public class Usuarios_Model {

    private long IdUsuario;
    private String NombreUsuario; 
    private String ApellidoPaternoUsuario;
    private String ApellidoMaternoUsuario; 
    private Date FechaNacimientoUsuario;
    private String CorreoUsuario; 
    private InputStream ImagenPerfilUsuario;
    
    private String UsernameUsuario; 
    private String PasswordUsuario;
    private Date FechaCreacionUsuario; 
    private byte EstadoUsuario;
    
    private long cantPreguntasUsuario;
    private long cantRespuestasUsuario;
    private long cantPreguntasFavoritasUsuario;
    private long cantPreguntasUtilesUsuario;
    private long cantPreguntasNoUtilesUsuario;
    
    
	public Usuarios_Model(long idUsuario, String nombreUsuario, String apellidoPaternoUsuario,
			String apellidoMaternoUsuario, Date fechaNacimientoUsuario, String correoUsuario,
			InputStream imagenPerfilUsuario, String usernameUsuario, String passwordUsuario, Date fechaCreacionUsuario,
			byte estadoUsuario) {
		super();
		IdUsuario = idUsuario;
		NombreUsuario = nombreUsuario;
		ApellidoPaternoUsuario = apellidoPaternoUsuario;
		ApellidoMaternoUsuario = apellidoMaternoUsuario;
		FechaNacimientoUsuario = fechaNacimientoUsuario;
		CorreoUsuario = correoUsuario;
		ImagenPerfilUsuario = imagenPerfilUsuario;
		UsernameUsuario = usernameUsuario;
		PasswordUsuario = passwordUsuario;
		FechaCreacionUsuario = fechaCreacionUsuario;
		EstadoUsuario = estadoUsuario;
	}
	
	
	
	
	
	public Usuarios_Model(long idUsuario) {
		super();
		IdUsuario = idUsuario;
	}






	public Usuarios_Model(String nombreUsuario, String apellidoPaternoUsuario, String apellidoMaternoUsuario,
			Date fechaNacimientoUsuario, String correoUsuario, InputStream imagenPerfilUsuario, String usernameUsuario,
			String passwordUsuario, Date fechaCreacionUsuario, byte estadoUsuario) {
		super();
		NombreUsuario = nombreUsuario;
		ApellidoPaternoUsuario = apellidoPaternoUsuario;
		ApellidoMaternoUsuario = apellidoMaternoUsuario;
		FechaNacimientoUsuario = fechaNacimientoUsuario;
		CorreoUsuario = correoUsuario;
		ImagenPerfilUsuario = imagenPerfilUsuario;
		UsernameUsuario = usernameUsuario;
		PasswordUsuario = passwordUsuario;
		FechaCreacionUsuario = fechaCreacionUsuario;
		EstadoUsuario = estadoUsuario;
	}





	public Usuarios_Model() {
		super();
	}


	


	public Usuarios_Model(String nombreUsuario, String apellidoPaternoUsuario, String apellidoMaternoUsuario,
			Date fechaNacimientoUsuario, String correoUsuario, InputStream imagenPerfilUsuario, String usernameUsuario,
			String passwordUsuario) {
		super();
		NombreUsuario = nombreUsuario;
		ApellidoPaternoUsuario = apellidoPaternoUsuario;
		ApellidoMaternoUsuario = apellidoMaternoUsuario;
		FechaNacimientoUsuario = fechaNacimientoUsuario;
		CorreoUsuario = correoUsuario;
		ImagenPerfilUsuario = imagenPerfilUsuario;
		UsernameUsuario = usernameUsuario;
		PasswordUsuario = passwordUsuario;
	}


	


	public Usuarios_Model(long idUsuario, String nombreUsuario, String apellidoPaternoUsuario,
			String apellidoMaternoUsuario, Date fechaNacimientoUsuario, String correoUsuario,
			InputStream imagenPerfilUsuario, String usernameUsuario, String passwordUsuario) {
		super();
		IdUsuario = idUsuario;
		NombreUsuario = nombreUsuario;
		ApellidoPaternoUsuario = apellidoPaternoUsuario;
		ApellidoMaternoUsuario = apellidoMaternoUsuario;
		FechaNacimientoUsuario = fechaNacimientoUsuario;
		CorreoUsuario = correoUsuario;
		ImagenPerfilUsuario = imagenPerfilUsuario;
		UsernameUsuario = usernameUsuario;
		PasswordUsuario = passwordUsuario;
	}





	public Usuarios_Model(String correoUsuario, String passwordUsuario) {
		super();
		CorreoUsuario = correoUsuario;
		PasswordUsuario = passwordUsuario;
	}
	
	
	public Usuarios_Model(long idUsuario, String nombreUsuario, String apellidoPaternoUsuario,
			String apellidoMaternoUsuario, Date fechaNacimientoUsuario, String correoUsuario,
			InputStream imagenPerfilUsuario, String usernameUsuario, String passwordUsuario, Date fechaCreacionUsuario,
			byte estadoUsuario, long cantPreguntasUsuario, long cantRespuestasUsuario,
			long cantPreguntasFavoritasUsuario, long cantPreguntasUtilesUsuario, long cantPreguntasNoUtilesUsuario) {
		super();
		IdUsuario = idUsuario;
		NombreUsuario = nombreUsuario;
		ApellidoPaternoUsuario = apellidoPaternoUsuario;
		ApellidoMaternoUsuario = apellidoMaternoUsuario;
		FechaNacimientoUsuario = fechaNacimientoUsuario;
		CorreoUsuario = correoUsuario;
		ImagenPerfilUsuario = imagenPerfilUsuario;
		UsernameUsuario = usernameUsuario;
		PasswordUsuario = passwordUsuario;
		FechaCreacionUsuario = fechaCreacionUsuario;
		EstadoUsuario = estadoUsuario;
		this.cantPreguntasUsuario = cantPreguntasUsuario;
		this.cantRespuestasUsuario = cantRespuestasUsuario;
		this.cantPreguntasFavoritasUsuario = cantPreguntasFavoritasUsuario;
		this.cantPreguntasUtilesUsuario = cantPreguntasUtilesUsuario;
		this.cantPreguntasNoUtilesUsuario = cantPreguntasNoUtilesUsuario;
	}





	public long getIdUsuario() {
		return IdUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		IdUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return NombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		NombreUsuario = nombreUsuario;
	}
	public String getApellidoPaternoUsuario() {
		return ApellidoPaternoUsuario;
	}
	public void setApellidoPaternoUsuario(String apellidoPaternoUsuario) {
		ApellidoPaternoUsuario = apellidoPaternoUsuario;
	}
	public String getApellidoMaternoUsuario() {
		return ApellidoMaternoUsuario;
	}
	public void setApellidoMaternoUsuario(String apellidoMaternoUsuario) {
		ApellidoMaternoUsuario = apellidoMaternoUsuario;
	}
	public String getNombreCompletoUsuario() {
		String nombreCompleto = NombreUsuario + " " + ApellidoPaternoUsuario + " " + ApellidoMaternoUsuario;
		return nombreCompleto;
	}	
	public Date getFechaNacimientoUsuario() {
		return FechaNacimientoUsuario;
	}
	public String getEdad() {
		String Edad = "";
		
		int yearNac = FechaNacimientoUsuario.getYear();
		int monthNac = FechaNacimientoUsuario.getMonth();
		int dayNac = FechaNacimientoUsuario.getDay();

		
		
		int yearActual = new java.util.Date().getYear();
		int monthActual = new java.util.Date().getMonth();
		int dayActual = new java.util.Date().getDay();
		
		boolean futureBirthday = false;
		int yearDifference = yearActual - yearNac;
		
		if(		(monthNac >  monthActual)		|| 		(monthNac ==  monthActual	&&		dayNac > dayActual)		) {
			futureBirthday = true;
		}
		
		if(futureBirthday == false) {
			Edad = String.valueOf( yearDifference );
		}
		else {
			Edad = String.valueOf( yearDifference - 1 );
		}
		return Edad;
	}
	public void setFechaNacimientoUsuario(Date fechaNacimientoUsuario) {
		FechaNacimientoUsuario = fechaNacimientoUsuario;
	}
	public String getCorreoUsuario() {
		return CorreoUsuario;
	}
	public void setCorreoUsuario(String correoUsuario) {
		CorreoUsuario = correoUsuario;
	}
	public InputStream getImagenPerfilUsuario() {
		return ImagenPerfilUsuario;
	}
	public InputStream isImagenPerfilUsuario() {
		if (ImagenPerfilUsuario != null) {
			PushbackInputStream pushbackInputStream = new PushbackInputStream(ImagenPerfilUsuario);
		    int bytes = 0;
		    try {
				bytes = pushbackInputStream.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    if (bytes == -1) {
		    	return null;
		    }
		    else {
		    	return ImagenPerfilUsuario;
		    }
			
		}
		else {
		    return null;
		}
	}
	public void setImagenPerfilUsuario(InputStream imagenPerfilUsuario) {
		ImagenPerfilUsuario = imagenPerfilUsuario;
	}
	public String getUsernameUsuario() {
		return UsernameUsuario;
	}
	public void setUsernameUsuario(String usernameUsuario) {
		UsernameUsuario = usernameUsuario;
	}
	public String getPasswordUsuario() {
		return PasswordUsuario;
	}
	public void setPasswordUsuario(String passwordUsuario) {
		PasswordUsuario = passwordUsuario;
	}
	public Date getFechaCreacionUsuario() {
		return FechaCreacionUsuario;
	}
	public void setFechaCreacionUsuario(Date fechaCreacionUsuario) {
		FechaCreacionUsuario = fechaCreacionUsuario;
	}
	public byte getEstadoUsuario() {
		return EstadoUsuario;
	}
	public String getEstadoUsuarioString() {
		if (EstadoUsuario == 1) {
			return "Activo";
		}
		else {
			return "Inactivo";
		}
	}
	public void setEstadoUsuario(byte estadoUsuario) {
		EstadoUsuario = estadoUsuario;
	}

	
	
	
	public long getCantPreguntasUsuario() {
		return cantPreguntasUsuario;
	}

	public void setCantPreguntasUsuario(long cantPreguntasUsuario) {
		this.cantPreguntasUsuario = cantPreguntasUsuario;
	}

	public long getCantRespuestasUsuario() {
		return cantRespuestasUsuario;
	}

	public void setCantRespuestasUsuario(long cantRespuestasUsuario) {
		this.cantRespuestasUsuario = cantRespuestasUsuario;
	}

	public long getCantPreguntasFavoritasUsuario() {
		return cantPreguntasFavoritasUsuario;
	}

	public void setCantPreguntasFavoritasUsuario(long cantPreguntasFavoritasUsuario) {
		this.cantPreguntasFavoritasUsuario = cantPreguntasFavoritasUsuario;
	}

	public long getCantPreguntasUtilesUsuario() {
		return cantPreguntasUtilesUsuario;
	}

	public void setCantPreguntasUtilesUsuario(long cantPreguntasUtilesUsuario) {
		this.cantPreguntasUtilesUsuario = cantPreguntasUtilesUsuario;
	}

	public long getCantPreguntasNoUtilesUsuario() {
		return cantPreguntasNoUtilesUsuario;
	}

	public void setCantPreguntasNoUtilesUsuario(long cantPreguntasNoUtilesUsuario) {
		this.cantPreguntasNoUtilesUsuario = cantPreguntasNoUtilesUsuario;
	}  

}