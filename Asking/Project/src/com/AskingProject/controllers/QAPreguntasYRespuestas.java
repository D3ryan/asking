package com.AskingProject.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.AskingProject.dao.PreguntasDAO;
import com.AskingProject.dao.RespuestasDAO;
import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.dao.Usuarios_calf_fav_preguntaDAO;
import com.AskingProject.dao.Usuarios_calf_preguntaDAO;
import com.AskingProject.dao.Usuarios_calf_respuestaDAO;
import com.AskingProject.models.Preguntas_Model;
import com.AskingProject.models.Respuestas_Model;
import com.AskingProject.models.Usuarios_Model;
import com.AskingProject.models.Usuarios_calf_fav_pregunta_Model;
import com.AskingProject.models.Usuarios_calf_pregunta_Model;
import com.AskingProject.models.Usuarios_calf_respuesta_Model;
import com.AskingProject.utils.userType;

/**
 * Servlet implementation class QAPregYResp
 */
@WebServlet("/QAPreguntasYRespuestas")
public class QAPreguntasYRespuestas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QAPreguntasYRespuestas() {
        super();

        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = null;
		try {
			boolean existePregunta = getPregunta(request, response);
			
			if (existePregunta == true)
				dispatcher = request.getRequestDispatcher("q&a.jsp");
			else {
				request.setAttribute("error", "Parece que la pregunta que esta buscando ha sido eliminada");
				dispatcher = request.getRequestDispatcher("error.jsp");
			}
				
		} catch(NullPointerException  e) {
			request.setAttribute("error", "Parece que no se encontro la pregunta que estabas buscando");
			dispatcher = request.getRequestDispatcher("error.jsp");
			
		} catch(NumberFormatException  e) {
			request.setAttribute("error", "�Trataste de buscar una pregunta con un caracter?");
			dispatcher = request.getRequestDispatcher("error.jsp");
		} catch(Exception  e) { //Error inesperado
			request.setAttribute("error", "Error inesperado");
			dispatcher = request.getRequestDispatcher("error.jsp");
		}
		finally {
			dispatcher.forward(request, response);
		}
		
		
	}

	

	private boolean getPregunta(HttpServletRequest request, HttpServletResponse response) {
		boolean existePregunta = false;
		
		Preguntas_Model preguntaElegida = null;
		
		List<Preguntas_Model> listaPreguntaAux = null;
		Preguntas_Model preguntaAux = null;

		long IdPregunta = 0;

		if (request.getParameter("IdPregunta") != null) {
			IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));
		}

		preguntaAux = new Preguntas_Model(IdPregunta);

		try {
			listaPreguntaAux = PreguntasDAO.getPregunta("B", preguntaAux);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (listaPreguntaAux.size() > 0) {
			preguntaElegida = listaPreguntaAux.get(0);
			
			if (preguntaElegida.getEstadoPregunta() == 1)
				existePregunta = true;
			else
				return false;

		}
		
		request.setAttribute("preguntaElegida", preguntaElegida);
		
		getTipoUsuario(preguntaElegida, request, response);
		getRespuestaCorrecta(preguntaElegida.getRespuesta_Correcta(), request, response);
		getRespuestas(preguntaElegida.getIdPregunta(), request, response);
		
		return existePregunta;
	}
	
	private void getTipoUsuario(Preguntas_Model preguntaElegida, HttpServletRequest request, HttpServletResponse response) {
		userType usuarioActivo = null;
		
		long IdUsuarioPregunta = preguntaElegida.getUsuario_Pregunta();
		byte EstadoUsuario = -1;
		//System.out.println("tipo de usuario x1:" + usuarioActivo);
		long IdUsuarioActivo = -1;
		if (request.getSession().getAttribute("IdUsuarioActivo") != null) {
			IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
			EstadoUsuario = (byte)request.getSession().getAttribute("EstadoUsuario");
		}
		//System.out.println("id usuario  x2:" + IdUsuarioActivo + " user pregunta: " + IdUsuarioPregunta + " estado user: " + EstadoUsuarioPregunta);
		
		if (IdUsuarioActivo == -1 || EstadoUsuario == 0) {
			usuarioActivo = userType.Invited;
		}
		else {
			if (IdUsuarioActivo == IdUsuarioPregunta) {
				usuarioActivo = userType.questionOwner;
			}
			else {
				usuarioActivo = userType.normalUser;
			}
		}
		
		request.setAttribute("usuarioActivo", usuarioActivo);
		request.setAttribute("IdUsuarioActivo", IdUsuarioActivo);
		
	}

	private void getRespuestaCorrecta(long respuesta_Correcta, HttpServletRequest request, HttpServletResponse response) {
		List<Respuestas_Model> respuestaCorrecta = null;
		
		Respuestas_Model respuestaAux = null;
		
        // Agregamos el usuario a la lista
		respuestaAux = new Respuestas_Model(respuesta_Correcta);
        

        try {
        	respuestaCorrecta = RespuestasDAO.getRespuesta("B", respuestaAux);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        //TODO: TODOS LOS QUE DIGAN "LIST.size() > 0 PARA REVISAR SI SE TRAJERON MAS DE UNA FILA, HAY QUE CAMBIARLOS POR != NULL Y DESPUES ASEGURAR QUE EL ID ES DIF DE 0"
		//LO DEL 0 ES DEBIDO A QUE EL QUERY B TRAE PROBLEMAS Y RETORNA 1 FILA (VALORES EN NULL EXCEPTO EL CONTADOR)
        if (respuestaCorrecta != null) {
        	if (respuestaCorrecta.get(0).getIdRespuesta() != 0)
        		request.setAttribute("respuestaCorrecta", respuestaCorrecta.get(0));
        }
	}

	private void getRespuestas(long IdPregunta, HttpServletRequest request, HttpServletResponse response) {
		List<Respuestas_Model> lista10Respuestas = null;
		
		Respuestas_Model respuestaAux = null;
		int numeroRespuesta = 0;
		if (request.getParameter("numeroPagina") != null) {
			int numeroPagina = Integer.valueOf(request.getParameter("numeroPagina"));
			if (numeroPagina <= 0)
				numeroPagina = 1;
				
			request.setAttribute("numeroPagina", numeroPagina);
			numeroRespuesta = 10 * (numeroPagina - 1);
		}
		
        // Agregamos el usuario a la lista
		respuestaAux = new Respuestas_Model(IdPregunta, numeroRespuesta);
        

		
        try {
        	lista10Respuestas = RespuestasDAO.getRespuesta("V", respuestaAux);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
        request.setAttribute("lista10Respuestas", lista10Respuestas);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String tipo = "";
		if (request.getSession().getAttribute("IdUsuarioActivo") != null) {			
			tipo = request.getParameter("tipo");
			
			switch(tipo) {
			case "Pregunta":
			{
				
				String Voto = request.getParameter("Vote");

				switch (Voto) {
				case "Util":
					ingresarVotoUtilidadPregunta(request, response, (byte) 1);
					break;

				case "NoUtil":
					ingresarVotoUtilidadPregunta(request, response, (byte) 0);
					break;

				case "Fav":
					ingresarVotoFavoritoPregunta(request, response);

					break;

				default:
//				404 Not Found
				}
				break;
			}	
			case "Respuesta":
			{	
				String Voto = request.getParameter("Vote");

				switch (Voto) {
				case "Util":
					ingresarVotoUtilidadRespuesta(request, response, (byte) 1);
					break;

				case "NoUtil":
					ingresarVotoUtilidadRespuesta(request, response, (byte) 0);
					break;
					
				default:
//				404 Not Found

				}
				
				break;
			}
			case "RespuestaCorrecta": 
				ingresarRespuestaCorrecta(request, response);
				break;
			case "BorrarPregunta": 
				borrarPregunta(request, response);
				break;
			case "BorrarRespuesta": 
				borrarRespuesta(request, response);
				break;
			default:
			}
			
		}		
		
		
		if (tipo.equals("BorrarPregunta") == false) {
			long IdPregunta = 0;
			if (request.getParameter("IdPregunta") != null) {
				IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));
			}
			response.sendRedirect("QAPreguntasYRespuestas?IdPregunta=" + IdPregunta + "&numeroPagina=1");
		}
		else {
			response.sendRedirect("IndexPreguntas?numeroPagina=1");			
		}
	}


	private void ingresarRespuestaCorrecta(HttpServletRequest request, HttpServletResponse response) {
		List<Preguntas_Model> PreguntaElegida = null;
		Preguntas_Model PreguntaParametros = null;
		
		long IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));
		long IdRespuesta = Long.parseLong(request.getParameter("IdRespuesta"));
		
		PreguntaParametros = new Preguntas_Model(IdPregunta, IdRespuesta);
		
		try {
			PreguntaElegida = PreguntasDAO.getPregunta("S", PreguntaParametros);

			if (PreguntaElegida.size() == 1) {
				//TODO: COMO EN LA BASE DE DATOS ES NULL, LO QUE ASIGNA EN RESPUESTA CORRECTA ES 0, Y COMO LAS RESPUESTAS EMPIEZAN EN EL INDICE 1, ENTONCES NO HAY PROBLEMA
				//PERO DEBER�A MANEJARLO DE OTRA MANERA POR SI EMPEZARA EN EL INDICE 0, CUAL SER�A LA MEJOR?
				if (PreguntaElegida.get(0).getRespuesta_Correcta() != IdRespuesta) {
					PreguntasDAO.insertUpdateDeletePregunta("IRC", PreguntaParametros);
				}
				
				else
					PreguntasDAO.insertUpdateDeletePregunta("DRC", PreguntaParametros);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	private void ingresarVotoUtilidadPregunta(HttpServletRequest request, HttpServletResponse response, byte UtilidadPreguntaCalificada) {
		List<Usuarios_calf_pregunta_Model> UCPUVoto = null;
		Usuarios_calf_pregunta_Model UCPUParametros = null;
		Usuarios_calf_pregunta_Model UCPUNuevoVoto = null;

		long IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
		long IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));
		
		if (true) {
			UCPUParametros = new Usuarios_calf_pregunta_Model(IdUsuarioActivo, IdPregunta);
			
			try {
				UCPUVoto = Usuarios_calf_preguntaDAO.getUCP("V", UCPUParametros);

				UCPUNuevoVoto = new Usuarios_calf_pregunta_Model(IdUsuarioActivo, IdPregunta, UtilidadPreguntaCalificada);

				if (UCPUVoto.size() == 0) {
					//INGRESAR VOTO
					Usuarios_calf_preguntaDAO.insertUpdateDeleteUCP("I", UCPUNuevoVoto);
				} 
				else {
					if (UCPUVoto.get(0).getUtilidadPreguntaCalificada() == 1) {
						if (UtilidadPreguntaCalificada == 1) {
							// BORAR VOTO
							Usuarios_calf_preguntaDAO.insertUpdateDeleteUCP("D", UCPUNuevoVoto);
						} 
						else if (UtilidadPreguntaCalificada == 0) {
							// UPDATEAR VOTO
							Usuarios_calf_preguntaDAO.insertUpdateDeleteUCP("U", UCPUNuevoVoto);
						}
					} 
					else if (UCPUVoto.get(0).getUtilidadPreguntaCalificada() == 0) {
						if (UtilidadPreguntaCalificada == 0) {
							// BORAR VOTO
							Usuarios_calf_preguntaDAO.insertUpdateDeleteUCP("D", UCPUNuevoVoto);
						} 
						else if (UtilidadPreguntaCalificada == 1) {
							// UPDATEAR VOTO
							Usuarios_calf_preguntaDAO.insertUpdateDeleteUCP("U", UCPUNuevoVoto);
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} 
	}
	
	private void ingresarVotoFavoritoPregunta(HttpServletRequest request, HttpServletResponse response) {
		List<Usuarios_calf_fav_pregunta_Model> UCFPVoto = null;
		Usuarios_calf_fav_pregunta_Model UCFPParametros = null;
		Usuarios_calf_fav_pregunta_Model UCFPNuevoVoto = null;

		long IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
		long IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));
		
		if (true) {
			UCFPParametros = new Usuarios_calf_fav_pregunta_Model(IdUsuarioActivo, IdPregunta);
			
			try {
				UCFPVoto = Usuarios_calf_fav_preguntaDAO.getUCFP("V", UCFPParametros);

				UCFPNuevoVoto = new Usuarios_calf_fav_pregunta_Model(IdUsuarioActivo, IdPregunta);

				if (UCFPVoto.size() == 0) {
					//INGRESAR VOTO
					Usuarios_calf_fav_preguntaDAO.insertUpdateDeleteUCFP("I", UCFPNuevoVoto);
				} 
				else {
					// BORAR VOTO
					Usuarios_calf_fav_preguntaDAO.insertUpdateDeleteUCFP("D", UCFPNuevoVoto);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} 
	}

	private void ingresarVotoUtilidadRespuesta(HttpServletRequest request, HttpServletResponse response, byte UtilidadRespuestaCalificada) {
		List<Usuarios_calf_respuesta_Model> UCRUVoto = null;
		Usuarios_calf_respuesta_Model UCRUParametros = null;
		Usuarios_calf_respuesta_Model UCRUNuevoVoto = null;

		long IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
		long IdRespuesta = Long.parseLong(request.getParameter("IdRespuesta"));
		
		if (true) {
			UCRUParametros = new Usuarios_calf_respuesta_Model(IdUsuarioActivo, IdRespuesta);
			
			try {
				UCRUVoto = Usuarios_calf_respuestaDAO.getUCR("V", UCRUParametros);

				UCRUNuevoVoto = new Usuarios_calf_respuesta_Model(IdUsuarioActivo, IdRespuesta, UtilidadRespuestaCalificada);

				if (UCRUVoto.size() == 0) {
					//INGRESAR VOTO
					Usuarios_calf_respuestaDAO.insertUpdateDeleteUCR("I", UCRUNuevoVoto);
				} 
				else {
					if (UCRUVoto.get(0).getUtilidadRespuestaCalificada() == 1) {
						if (UtilidadRespuestaCalificada == 1) {
							// BORAR VOTO
							Usuarios_calf_respuestaDAO.insertUpdateDeleteUCR("D", UCRUNuevoVoto);
						} 
						else if (UtilidadRespuestaCalificada == 0) {
							// UPDATEAR VOTO
							Usuarios_calf_respuestaDAO.insertUpdateDeleteUCR("U", UCRUNuevoVoto);
						}
					} 
					else if (UCRUVoto.get(0).getUtilidadRespuestaCalificada() == 0) {
						if (UtilidadRespuestaCalificada == 0) {
							// BORAR VOTO
							Usuarios_calf_respuestaDAO.insertUpdateDeleteUCR("D", UCRUNuevoVoto);
						} 
						else if (UtilidadRespuestaCalificada == 1) {
							// UPDATEAR VOTO
							Usuarios_calf_respuestaDAO.insertUpdateDeleteUCR("U", UCRUNuevoVoto);
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} 
	}
	
	private long borrarPregunta(HttpServletRequest request, HttpServletResponse response) {
		Preguntas_Model PreguntaParametros = null;
		
		long IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));
		
		PreguntaParametros = new Preguntas_Model(IdPregunta);
		
		try {
			PreguntasDAO.insertUpdateDeletePregunta("E", PreguntaParametros);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return IdPregunta;
	}

	private void borrarRespuesta(HttpServletRequest request, HttpServletResponse response) {
		Respuestas_Model RespuestaParametros = null;
		long IdRespuesta = Long.parseLong(request.getParameter("IdRespuesta"));
		
		RespuestaParametros = new Respuestas_Model(IdRespuesta);
		
		try {
			RespuestasDAO.insertUpdateDeleteRespuesta("E", RespuestaParametros);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
