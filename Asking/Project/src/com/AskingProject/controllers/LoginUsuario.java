package com.AskingProject.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.models.Usuarios_Model;

/**
 * Servlet implementation class LoginUsuario
 */
@WebServlet("/LoginUsuario")
public class LoginUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.getSession().invalidate();
		request.getSession().setAttribute("IdUsuarioActivo", null);
    	request.getSession().setAttribute("EstadoUsuario", null);
		
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Usuarios_Model> listaUsuarios = buscarUsuario(request, response);
		
		
        if (listaUsuarios.isEmpty() == false) {
        	
        	request.getSession().setAttribute("IdUsuarioActivo", listaUsuarios.get(0).getIdUsuario());

        	byte EstadoUsuario = getEstadoUsuarioPregunta(listaUsuarios.get(0).getIdUsuario());
        	request.getSession().setAttribute("EstadoUsuario", EstadoUsuario);
        	
        	response.sendRedirect("IndexPreguntas?numeroPagina=0");

        }
        else {
        	request.setAttribute("usuarioIncorrecto", "true");
        	RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
        	dispatcher.forward(request, response);
        }

        
	}

	private List<Usuarios_Model> buscarUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.setCharacterEncoding("UTF-8");
		List<Usuarios_Model> listaUsuarios = null;
		
		Usuarios_Model usuarioElegido = null;
        
        String Usuario = request.getParameter("user");
        String PasswordUsuario = request.getParameter("password");
       
        // Agregamos el usuario a la lista
        //usuarioElegido = new Usuarios_Model(Usuario, PasswordUsuario);
        
        usuarioElegido = new Usuarios_Model();
        
        usuarioElegido.setUsernameUsuario(Usuario);
        usuarioElegido.setPasswordUsuario(PasswordUsuario);
        
        
        try {
        	 listaUsuarios = UsuariosDAO.getUsuarios("L",usuarioElegido);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
        return listaUsuarios;
	}
	
	private byte getEstadoUsuarioPregunta(long IdUsuarioActivo) {
		Usuarios_Model usuarioAux = new Usuarios_Model(IdUsuarioActivo);
		List<Usuarios_Model> listaUsuarioAux = null;

		try {
			listaUsuarioAux = UsuariosDAO.getUsuarios("S", usuarioAux);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaUsuarioAux.get(0).getEstadoUsuario();
	}
	
}
