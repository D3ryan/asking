package com.AskingProject.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.AskingProject.dao.CategoriasDAO;
import com.AskingProject.dao.PreguntasDAO;
import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.models.Categorias_Model;
import com.AskingProject.models.Preguntas_Model;
import com.AskingProject.models.Usuarios_Model;

/**
 * Servlet implementation class SubirPreguntas
 */
@WebServlet("/SubirPreguntas")
@MultipartConfig(maxFileSize = 1000 * 1000 * 5, maxRequestSize = 1000 * 1000 * 25, fileSizeThreshold = 1000 * 1000)
public class SubirPreguntas extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SubirPreguntas() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		getUsuarioActivo(request, response);
		getPregunta(request, response);
		getCategorias(request, response);

		
		RequestDispatcher dispatcher = request.getRequestDispatcher("question.jsp");
		dispatcher.forward(request, response);
	}

	private void getPregunta(HttpServletRequest request, HttpServletResponse response) {
		long IdPregunta = 0;

		if (request.getParameter("IdPregunta") != null) {
			
			IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));

			Preguntas_Model preguntaElegida = null;

			List<Preguntas_Model> listaPreguntaAux = null;
			Preguntas_Model preguntaAux = null;

			preguntaAux = new Preguntas_Model(IdPregunta);

			try {
				listaPreguntaAux = PreguntasDAO.getPregunta("B", preguntaAux);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (listaPreguntaAux.size() > 0) {
				preguntaElegida = listaPreguntaAux.get(0);
			}

			request.setAttribute("preguntaElegida", preguntaElegida);
		}
	}

	private void getCategorias(HttpServletRequest request, HttpServletResponse response) {
		List<Categorias_Model> listaCategorias = null;

		try {
			listaCategorias = CategoriasDAO.getListCategoria();
		} catch (Exception e) {
			e.printStackTrace();
		}

		request.setAttribute("listaCategorias", listaCategorias);

	}

	private void getUsuarioActivo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Usuarios_Model> usuarioElegido = null;
		Usuarios_Model usuarioAux = null;
		
		if (request.getSession().getAttribute("IdUsuarioActivo") != null) {

			long IdUsuarioActivo = (Long) request.getSession().getAttribute("IdUsuarioActivo");
			usuarioAux = new Usuarios_Model(IdUsuarioActivo);

			try {
				usuarioElegido = UsuariosDAO.getUsuarios("S", usuarioAux);
			} catch (Exception e) {
				e.printStackTrace();
			}

			request.setAttribute("usuarioElegido", usuarioElegido.get(0));
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		if (request.getParameter("abrirPregunta") != null) {
			
			if (Boolean.valueOf(request.getParameter("abrirPregunta")) == true)
				doGet(request, response);
		}
		else {
			long IdPregunta = insertarPregunta(request, response);
			
			response.sendRedirect("QAPreguntasYRespuestas?IdPregunta=" + IdPregunta + "&numeroPagina=1");
		}
		
	}

	private long insertarPregunta(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		long IdPregunta = 0;
		
		Preguntas_Model preguntaNueva = null;

		String TituloPregunta = request.getParameter("question");
		String DescripcionPregunta = request.getParameter("description");

		InputStream ImagenPregunta = null; // input stream of the upload file

		List<Preguntas_Model> preguntaIngresada = null;
		
		// obtains the upload file part in this multipart request

		Part filePart = request.getPart("upload");

		if (filePart.getSize() != 0) {
			
			if (filePart.getSize() != 0)
				ImagenPregunta = filePart.getInputStream();
		}
		
		

		long IdUsuarioPregunta = (Long) request.getSession().getAttribute("IdUsuarioActivo");

		int IdCategoriaPregunta = Integer.valueOf(request.getParameter("category"));

		if (Boolean.valueOf(request.getParameter("preguntaNueva")) == true) {

			preguntaNueva = new Preguntas_Model(TituloPregunta, DescripcionPregunta, ImagenPregunta, IdUsuarioPregunta,
					IdCategoriaPregunta);
			
			try {
				PreguntasDAO.insertUpdateDeletePregunta("I", preguntaNueva);
				preguntaIngresada = PreguntasDAO.getPregunta("N", preguntaNueva);
			} catch (Exception e) {
				e.printStackTrace();
			}

			IdPregunta =  preguntaIngresada.get(0).getIdPregunta();
		} 
		else {
			IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));
			
			int eliminarImagen = Integer.valueOf(request.getParameter("eliminarImagen"));
			
			preguntaNueva = new Preguntas_Model(IdPregunta, TituloPregunta, DescripcionPregunta, ImagenPregunta, IdUsuarioPregunta,
					IdCategoriaPregunta);
			
			try {
				if (eliminarImagen == 0)
					PreguntasDAO.insertUpdateDeletePregunta("U", preguntaNueva);
				else if (eliminarImagen == 1)
					PreguntasDAO.insertUpdateDeletePregunta("UE", preguntaNueva);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		return IdPregunta;
	}

}
