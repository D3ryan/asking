package com.AskingProject.controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.models.Usuarios_Model;
import com.AskingProject.utils.FileUtils;

/**
 * Servlet implementation class SingUpUsuarios
 */
@WebServlet("/SingUpUsuarios")
@MultipartConfig(maxFileSize = 1000 * 1000 * 5, maxRequestSize = 1000 * 1000 * 25, fileSizeThreshold = 1000 * 1000)
public class SingUpUsuarios extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SingUpUsuarios() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		int usernameEncontrado = verificarUsername(request,response) ;
		if( usernameEncontrado == 1 ) {
			request.setAttribute("usernameEncontrado", "true");
			RequestDispatcher dispatcher = request.getRequestDispatcher("signup.jsp");
        	dispatcher.forward(request, response);
		}
		else if( usernameEncontrado == 0 ){
			insertarUsuario(request, response);
	        response.sendRedirect("login.jsp"); //Aqui redireccionar�a al servlet?
	          
		}
			
		
	}
	
	private int verificarUsername(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int usernameEncontrado = 0;
		String UsernameUsuario = request.getParameter("username");
	      try {
				usernameEncontrado = UsuariosDAO.getUsuario(UsernameUsuario);
			} catch (Exception e) {
				e.printStackTrace();
			}
	      
	      return usernameEncontrado;
	      
	}
	
	private void insertarUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
      
		Usuarios_Model usuarioNuevo = null;
      
      
      String NombreUsuario = request.getParameter("name");
      String ApellidoPaternoUsuario = request.getParameter("lastnameP");
      String ApellidoMaternoUsuario = request.getParameter("lastnameM");
      
      
      SimpleDateFormat formatoFechaNac = new SimpleDateFormat("yyyy-MM-dd");
      Date FechaNacimientoUsuario = null;
		try {
			FechaNacimientoUsuario = formatoFechaNac.parse(request.getParameter("birthday"));
		} catch (ParseException e2) {
			e2.printStackTrace();
		}
      
      String CorreoUsuario = request.getParameter("email");
      
      InputStream ImagenPerfilUsuario = null; // input stream of the upload file
      
      // obtains the upload file part in this multipart request
      Part filePart = request.getPart("imagen");
      if (filePart != null) {
          // prints out some information for debugging
          System.out.println(filePart.getName());
          System.out.println(filePart.getSize());
          System.out.println(filePart.getContentType());
           
          // obtains input stream of the upload file
          ImagenPerfilUsuario = filePart.getInputStream();
      }
      


      String UsernameUsuario = request.getParameter("username");
      String PasswordUsuario = request.getParameter("password");
     

      
      // Agregamos el usuario a la lista
      usuarioNuevo =new Usuarios_Model(
      		NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario,
      		FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, 
      		UsernameUsuario, PasswordUsuario
      		);
      
      
      try {
			UsuariosDAO.insertUpdateDeleteUsuario("I",usuarioNuevo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}