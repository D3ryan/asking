package com.AskingProject.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.AskingProject.dao.CategoriasDAO;
import com.AskingProject.dao.PreguntasDAO;
import com.AskingProject.models.Categorias_Model;
import com.AskingProject.models.Preguntas_Model;

/**
 * Servlet implementation class SearchPreguntas
 */
@WebServlet("/SearchPreguntas")
public class SearchPreguntas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchPreguntas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = null;
		try {
			
			if (request.getParameter("Busqueda") != null) {
				request.setAttribute("Busqueda", "Busqueda");
				buscarListaPreguntasBuscadas(request, response);	
			}
			
			getCategorias(request, response);
			
			long IdUsuarioActivo = -1;
			if (request.getSession().getAttribute("IdUsuarioActivo") != null)
				IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
			
			byte EstadoUsuario = 0;
			if (request.getSession().getAttribute("EstadoUsuario") != null)
				EstadoUsuario = (byte)request.getSession().getAttribute("EstadoUsuario");

			request.setAttribute("IdUsuarioActivo", IdUsuarioActivo);
			request.setAttribute("EstadoUsuario", EstadoUsuario);
			
			dispatcher = request.getRequestDispatcher("search.jsp");
			
			
		} catch(NumberFormatException  e) {
			request.setAttribute("error", "¿Trataste de buscar una pregunta con un caracter?");
			dispatcher = request.getRequestDispatcher("error.jsp");
		} catch(Exception e) {
			request.setAttribute("error", e.toString());
			dispatcher = request.getRequestDispatcher("error.jsp");
		}
		finally {
			dispatcher.forward(request, response);
		}
		
	}

	private void getCategorias(HttpServletRequest request, HttpServletResponse response) {
		List<Categorias_Model> listaCategorias = null;

		try {
			listaCategorias = CategoriasDAO.getListCategoria();
		} catch (Exception e) {
			e.printStackTrace();
		}

		request.setAttribute("listaCategorias", listaCategorias);

	}
	

	
	private void buscarListaPreguntasBuscadas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Preguntas_Model> listaPreguntasBuscadas = null;
		
		Preguntas_Model parametrosPregunta = null;
		
		int numeroPregunta = 0;
		if (request.getParameter("numeroPagina") != null) {
			int numeroPagina = Integer.valueOf(request.getParameter("numeroPagina"));
			if (numeroPagina <= 0)
				numeroPagina = 1;
				
			request.setAttribute("numeroPagina", numeroPagina);
			
			numeroPregunta = 10 * (numeroPagina - 1);
		}
       
		
		String TituloPregunta = null;
		if (request.getParameter("question") != null && request.getParameter("question") != ""){
			TituloPregunta = request.getParameter("question");
		}
		
		
		int IdCategoriaPregunta = -1;
		if (request.getParameter("categories") != null){
			IdCategoriaPregunta = Integer.valueOf(request.getParameter("categories"));
		}
		
	    Date preguntaFechaDesde = null;
	    try {
	    	if (request.getParameter("dateFrom") != null && request.getParameter("dateFrom") != "") {
	    		SimpleDateFormat formatoFechaDesde = new SimpleDateFormat("yyyy-MM-dd");
	    		preguntaFechaDesde = formatoFechaDesde.parse(request.getParameter("dateFrom"));
	    	}
	    	else {
	    		int i = 0; //TODO: ESTABLECEMOS LA FECHA COMO 01/01/0001? O LO MANDAMOS COMO NULL
	    	}
		} catch (ParseException e2) {
			e2.printStackTrace();
		}
			
		
		Date preguntaFechaHasta = null;
		try {
			if (request.getParameter("dateTo") != null && request.getParameter("dateTo") != "") {
				SimpleDateFormat formatoFechaHasta = new SimpleDateFormat("yyyy-MM-dd");
				preguntaFechaHasta = formatoFechaHasta.parse(request.getParameter("dateTo"));
			}
			else{
	    		int i = 0; //TODO: ESTABLECEMOS LA FECHA COMO 31/12/9999? O LO MANDAMOS COMO NULL
	    	}
		} catch (ParseException e2) {
			e2.printStackTrace();
		}
		
		long CantidadVotosUtiles = -1;
		if (request.getParameter("useful") != null && request.getParameter("useful") != ""){
			CantidadVotosUtiles = Long.parseLong(request.getParameter("useful"));
		}
		
		
		long CantidadVotosFavoritos = -1;
		if (request.getParameter("fav") != null && request.getParameter("fav") != ""){
			CantidadVotosFavoritos = Long.parseLong(request.getParameter("fav"));
		}
		
		parametrosPregunta =new Preguntas_Model(
				numeroPregunta, TituloPregunta, IdCategoriaPregunta, preguntaFechaDesde, 
				preguntaFechaHasta, CantidadVotosUtiles, CantidadVotosFavoritos
      		);
		
		request.setAttribute("parametrosPregunta", parametrosPregunta);
		
        try {
//        	System.out.println("Aqui llego x2, " + numeroPregunta + "," + TituloPregunta + "," + IdCategoriaPregunta + "," + preguntaFechaDesde + "," + preguntaFechaHasta + "," + CantidadVotosUtiles + "," + CantidadVotosFavoritos);
        	listaPreguntasBuscadas = PreguntasDAO.getPregunta("BA", parametrosPregunta);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
        request.setAttribute("lista10Preguntas", listaPreguntasBuscadas);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
	}

}
