package com.AskingProject.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.models.Usuarios_Model;

/**
 * Servlet implementation class EditarPerfilUsuario
 */
@WebServlet("/EditarPerfilUsuario")
@MultipartConfig(maxFileSize = 1000 * 1000 * 5, maxRequestSize = 1000 * 1000 * 25, fileSizeThreshold = 1000 * 1000)
public class EditarPerfilUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditarPerfilUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		getUsuario(request, response);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("edit.jsp");
		dispatcher.forward(request, response);
	}

	private void getUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Usuarios_Model> usuarioElegido = null;
		Usuarios_Model usuarioAux = null;

		long IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
		usuarioAux = new Usuarios_Model(IdUsuarioActivo);
		
		try {
			usuarioElegido = UsuariosDAO.getUsuarios("S", usuarioAux);
		} catch (Exception e) {
			
		}
		
		request.setAttribute("usuarioElegido", usuarioElegido.get(0)); 
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		actualizarUsuario(request, response);
		
		long IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
		response.sendRedirect("ProfileUsuario?IdUsuario=" + IdUsuarioActivo);
	}
	
	private void actualizarUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Usuarios_Model usuarioNuevo = null;

		long IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
		String NombreUsuario = request.getParameter("name");
		String ApellidoPaternoUsuario = request.getParameter("lastnameP");
		String ApellidoMaternoUsuario = request.getParameter("lastnameM");

		SimpleDateFormat formatoFechaNac = new SimpleDateFormat("yyyy-MM-dd");
		Date FechaNacimientoUsuario = null;
		try {
			FechaNacimientoUsuario = formatoFechaNac.parse(request.getParameter("birthday"));
		} catch (ParseException e2) {
			e2.printStackTrace();
		}

		String CorreoUsuario = request.getParameter("email");

		InputStream ImagenPerfilUsuario = null; // input stream of the upload file

		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("imagen");
		if (filePart != null) {
			
			if (filePart.getSize() != 0)
				ImagenPerfilUsuario = filePart.getInputStream();
		}

		String UsernameUsuario = request.getParameter("username");
		String PasswordUsuario = request.getParameter("password");

		usuarioNuevo = new Usuarios_Model(IdUsuarioActivo, NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario,
				FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, UsernameUsuario, PasswordUsuario);

		try {
			UsuariosDAO.insertUpdateDeleteUsuario("U", usuarioNuevo);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
