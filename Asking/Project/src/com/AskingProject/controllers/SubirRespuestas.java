package com.AskingProject.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.AskingProject.dao.CategoriasDAO;
import com.AskingProject.dao.PreguntasDAO;
import com.AskingProject.dao.RespuestasDAO;
import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.models.Categorias_Model;
import com.AskingProject.models.Preguntas_Model;
import com.AskingProject.models.Respuestas_Model;
import com.AskingProject.models.Usuarios_Model;

/**
 * Servlet implementation class SubirRespuestas
 */
@WebServlet("/SubirRespuestas")
@MultipartConfig(maxFileSize = 1000 * 1000 * 5, maxRequestSize = 1000 * 1000 * 25, fileSizeThreshold = 1000 * 1000)
public class SubirRespuestas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubirRespuestas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getUsuarioActivo(request, response);
		getPregunta(request, response);
		getRespuesta(request, response);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("answer.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		if (request.getParameter("abrirRespuesta") != null) {
			if (Boolean.valueOf(request.getParameter("abrirRespuesta")) == true)
				doGet(request, response);
		}
		else {
			long IdPreguntaRespondida = insertarRespuesta(request, response);
			System.out.println("La Pregunta respondida fue: " + IdPreguntaRespondida);
			response.sendRedirect("QAPreguntasYRespuestas?IdPregunta=" + IdPreguntaRespondida + "&numeroPagina=1");
		}

	}
	
	private long insertarRespuesta(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		long IdRespuesta = 0;
		
		Respuestas_Model respuestaNueva = null;
		
		String TextoRespuesta = request.getParameter("answer");
//		System.out.println("Texto de la respuesta" + request.getParameter("answer"));
		InputStream ImagenRespuesta = null; // input stream of the upload file

//		List<Respuestas_Model> RespuestaIngresada = null;
		
		// obtains the upload file part in this multipart request

		Part filePart = request.getPart("upload");

		if (filePart.getSize() != 0) {
			// obtains input stream of the upload file
			if (filePart.getSize() != 0)
				ImagenRespuesta = filePart.getInputStream();
		}

		long IdUsuarioRespuesta = (Long) request.getSession().getAttribute("IdUsuarioActivo");
//		System.out.println("Id Del Usuario que responde: " + IdUsuarioRespuesta);
		
		long IdPreguntaRespondida = Long.parseLong(request.getParameter("IdPregunta"));
		
//		System.out.println("Id de la pregunta: " + IdPreguntaRespondida);
 
		if (Boolean.valueOf(request.getParameter("respuestaNueva")) == true) {

			respuestaNueva = new Respuestas_Model(TextoRespuesta, ImagenRespuesta, IdUsuarioRespuesta, IdPreguntaRespondida);
			try {
				RespuestasDAO.insertUpdateDeleteRespuesta("I", respuestaNueva);
			} catch (Exception e) {
				e.printStackTrace();
			}

//			IdPregunta =  preguntaIngresada.get(0).getIdPregunta();
		} 
		else {

			IdRespuesta = Long.parseLong(request.getParameter("IdRespuesta"));
			int eliminarImagen = Integer.valueOf(request.getParameter("eliminarImagen"));
			
			System.out.print("Eliminar imagen: " + eliminarImagen);
			
			respuestaNueva = new Respuestas_Model(IdRespuesta, TextoRespuesta, ImagenRespuesta, IdUsuarioRespuesta, IdPreguntaRespondida); //Porque mandamos los ultimos Ids en estos updates
			
			try {
				if (eliminarImagen == 0)
					RespuestasDAO.insertUpdateDeleteRespuesta("U", respuestaNueva);
				else if (eliminarImagen == 1)
					RespuestasDAO.insertUpdateDeleteRespuesta("UE", respuestaNueva);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		return IdPreguntaRespondida;
	}
	
	private void getUsuarioActivo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Usuarios_Model> usuarioElegido = null;
		Usuarios_Model usuarioAux = null;
		
		if (request.getSession().getAttribute("IdUsuarioActivo") != null) {

			long IdUsuarioActivo = (Long) request.getSession().getAttribute("IdUsuarioActivo");
			usuarioAux = new Usuarios_Model(IdUsuarioActivo);

			try {
				usuarioElegido = UsuariosDAO.getUsuarios("S", usuarioAux);
			} catch (Exception e) {
				e.printStackTrace();
			}

			request.setAttribute("usuarioElegido", usuarioElegido.get(0));
		}
	}
	
	private void getPregunta(HttpServletRequest request, HttpServletResponse response) {
		long IdPregunta = 0;

		if (request.getParameter("IdPregunta") != null) {
			
			IdPregunta = Long.parseLong(request.getParameter("IdPregunta"));

			Preguntas_Model preguntaElegida = null;

			List<Preguntas_Model> listaPreguntaAux = null;
			Preguntas_Model preguntaAux = null;

			preguntaAux = new Preguntas_Model(IdPregunta);

			try {
				listaPreguntaAux = PreguntasDAO.getPregunta("B", preguntaAux);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (listaPreguntaAux.size() > 0) {
				preguntaElegida = listaPreguntaAux.get(0);
			}

			request.setAttribute("preguntaElegida", preguntaElegida);
		}
	}
	
	private void getRespuesta(HttpServletRequest request, HttpServletResponse response) {
		long IdRespuesta = 0;

		if (request.getParameter("IdRespuesta") != null) {
			
			IdRespuesta = Long.parseLong(request.getParameter("IdRespuesta"));
			
			Respuestas_Model respuestaElegida = null;

			List<Respuestas_Model> listaRespuestaAux = null;
			Respuestas_Model respuestaAux = null;

			respuestaAux = new Respuestas_Model(IdRespuesta);

			try {
				listaRespuestaAux = RespuestasDAO.getRespuesta("S", respuestaAux); //Por ahora lo haremos con el S falta un procedure con los votos.
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			

			if (listaRespuestaAux.size() > 0) {
				respuestaElegida = listaRespuestaAux.get(0);
			}

			request.setAttribute("respuestaElegida", respuestaElegida);
		}
	}

}
