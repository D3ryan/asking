package com.AskingProject.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.AskingProject.dao.CategoriasDAO;
import com.AskingProject.dao.PreguntasDAO;
import com.AskingProject.dao.RespuestasDAO;
import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.models.Categorias_Model;
import com.AskingProject.models.Preguntas_Model;
import com.AskingProject.models.Respuestas_Model;
import com.AskingProject.models.Usuarios_Model;

/**
 * Servlet implementation class generalServlet
 */
@WebServlet("/GeneralServlet")
public class GeneralServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GeneralServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String Imagen = request.getParameter("Imagen");
		
		switch(Imagen) {
		case "Usuario":
			getUsuarioImagen(request, response);
			break;
			
		case "Pregunta":
			getPreguntaImagen(request, response);
			break;
			
		case "Respuesta":
			getRespuestaImagen(request, response);
			
			break;
			
		default:			
//			404 Not Found
		
		}
		
	}

	private void getUsuarioImagen(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String imageName = "usuario.png"; // Returns "foo.png".
		List<Usuarios_Model> usuarioElegido = null;

		long IdUsuario = Long.parseLong(request.getParameter("Id"));
		Usuarios_Model usuarioAux = new Usuarios_Model(IdUsuario);
		
		try {
			usuarioElegido = UsuariosDAO.getUsuarios("S", usuarioAux);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (usuarioElegido.size() != 0) {
			if (usuarioElegido.get(0).getImagenPerfilUsuario() != null) {
				byte[] content = usuarioElegido.get(0).getImagenPerfilUsuario().readAllBytes();

				response.setContentType(getServletContext().getMimeType(imageName));
				response.setContentLength(content.length);
				response.getOutputStream().write(content);
			} 

		}
	}
	
	
	private void getPreguntaImagen(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String imageName = "pregunta.png"; // Returns "foo.png".
		List<Preguntas_Model> preguntaElegida = null;
		

		long IdPregunta = Long.parseLong(request.getParameter("Id"));
		Preguntas_Model preguntaAux = new Preguntas_Model(IdPregunta);
		
		try {
			preguntaElegida = PreguntasDAO.getPregunta("S", preguntaAux);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (preguntaElegida.size() != 0) {
			if (preguntaElegida.get(0).getImagenPregunta() != null) {

				byte[] content = preguntaElegida.get(0).getImagenPregunta().readAllBytes();

				response.setContentType(getServletContext().getMimeType(imageName)); // response.setContentType("image/jpeg");
				response.setContentLength(content.length);
				response.getOutputStream().write(content);
			} 

		}
	}
	
	
	private void getRespuestaImagen(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String imageName = "respuesta.png"; // Returns "foo.png".

		List<Respuestas_Model> respuestaElegida = null;

		long IdRespuesta = Long.parseLong(request.getParameter("Id"));
		Respuestas_Model respuestaAux = new Respuestas_Model(IdRespuesta);
		
		
		try {
			respuestaElegida = RespuestasDAO.getRespuesta("S", respuestaAux);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (respuestaElegida.size() != 0) {
			if (respuestaElegida.get(0).getImagenRespuesta() != null) {

				byte[] content = respuestaElegida.get(0).getImagenRespuesta().readAllBytes();

				response.setContentType(getServletContext().getMimeType(imageName));
				response.setContentLength(content.length);
				response.getOutputStream().write(content);
			} 

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);

		System.out.println("Aqui llegooooo x2");
	}

	public static Usuarios_Model getUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Usuarios_Model> usuarioElegido = null;
		Usuarios_Model usuarioAux = null;

		if (request.getSession().getAttribute("IdUsuarioActivo") != null) {
			long IdUsuarioActivo = (Long) request.getSession().getAttribute("IdUsuarioActivo");
			
			usuarioAux = new Usuarios_Model(IdUsuarioActivo);		
			
			try {
				usuarioElegido = UsuariosDAO.getUsuarios("S", usuarioAux);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (usuarioElegido != null)
			return usuarioElegido.get(0);
		else
			return null;
		
	}
	
	public static List<Categorias_Model>getCategorias() {
		List<Categorias_Model> listaCategorias = null;

		try {
			listaCategorias = CategoriasDAO.getListCategoria();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return  listaCategorias;
	}

}
