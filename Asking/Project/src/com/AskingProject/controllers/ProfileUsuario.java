package com.AskingProject.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.AskingProject.dao.PreguntasDAO;
import com.AskingProject.dao.RespuestasDAO;
import com.AskingProject.dao.UsuariosDAO;
import com.AskingProject.dao.Usuarios_calf_fav_preguntaDAO;
import com.AskingProject.dao.Usuarios_calf_preguntaDAO;
import com.AskingProject.models.Preguntas_Model;
import com.AskingProject.models.Respuestas_Model;
import com.AskingProject.models.Usuarios_Model;
import com.AskingProject.models.Usuarios_calf_fav_pregunta_Model;
import com.AskingProject.models.Usuarios_calf_pregunta_Model;
import com.AskingProject.utils.FileUtils;
/**
 * Servlet implementation class ProfileUsuario
 */
@WebServlet("/ProfileUsuario")
public class ProfileUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfileUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	RequestDispatcher dispatcher = null;
		try {
			
			getUsuario(request, response);
			
			String pagina = request.getParameter("pagina");
			
			if (pagina != null) {
				request.setAttribute("pagina", pagina);

				switch (pagina) {
				case "preguntas":
					getPreguntasUsuario(request, response);
					break;

				case "respuestas":
					getRespuestasUsuario(request, response);
					break;

				case "preguntasFavoritas":
					getPreguntasFavoritasUsuario(request, response);

					break;

				case "preguntasUtiles":
					getPreguntasUtilesParaUsuario(request, response);

					break;

				case "preguntasNoUtiles":
					getPreguntasNoUtilesParaUsuario(request, response);

					break;

				default:
//				404 Not Found

				}
			}
			
			dispatcher = request.getRequestDispatcher("profile.jsp");		
			
		} catch(IndexOutOfBoundsException e) {
			request.setAttribute("error", "�Parece que el perfil de este usuario no existe!");
			dispatcher = request.getRequestDispatcher("error.jsp");
		} catch(NumberFormatException e) {
			request.setAttribute("error", "Los perfiles no se buscan con caracteres, ten cuidado");
			dispatcher = request.getRequestDispatcher("error.jsp");
		} catch(Exception e) {
			request.setAttribute("error", "Error inesperado");
			dispatcher = request.getRequestDispatcher("error.jsp");
		}
		finally {
			dispatcher.forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	
	
	private void getUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Usuarios_Model> usuarioElegido = null;
		Usuarios_Model usuarioAux = null;

		long IdUsuario = Long.parseLong(request.getParameter("IdUsuario"));
		usuarioAux = new Usuarios_Model(IdUsuario);
		
		long IdUsuarioActivo = -1;
		if (request.getSession().getAttribute("IdUsuarioActivo") != null)
			IdUsuarioActivo = (Long)request.getSession().getAttribute("IdUsuarioActivo");
		
		try {
			usuarioElegido = UsuariosDAO.getUsuarios("UsP", usuarioAux);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.setAttribute("usuarioElegido", usuarioElegido.get(0));
		request.setAttribute("IdUsuarioActivo", IdUsuarioActivo);
	}

	
	private void getPreguntasUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long IdUsuario = Long.parseLong(request.getParameter("IdUsuario"));
		
		int numeroPregunta = 0;
		if (request.getParameter("numeroPagina") != null) {
			int numeroPagina = Integer.valueOf(request.getParameter("numeroPagina"));
			if (numeroPagina <= 0)
				numeroPagina = 1;
				
			request.setAttribute("numeroPagina", numeroPagina);
			
			numeroPregunta = 10 * (numeroPagina - 1);
		}
		
		Preguntas_Model parametroPreguntas = new Preguntas_Model();
		parametroPreguntas.setUsuario_Pregunta(IdUsuario);
		parametroPreguntas.setNumeroPregunta(numeroPregunta);
		
		List<Preguntas_Model> preguntasUsuarioElegido = null;
		
		try {
			preguntasUsuarioElegido = PreguntasDAO.getPregunta("UP", parametroPreguntas);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		request.setAttribute("preguntasUsuarioElegido", preguntasUsuarioElegido); 
	}
	
	private void getRespuestasUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long IdUsuario = Long.parseLong(request.getParameter("IdUsuario"));
		
		int numeroRespuesta = 0;
		if (request.getParameter("numeroPagina") != null) {
			int numeroPagina = Integer.valueOf(request.getParameter("numeroPagina"));
			if (numeroPagina <= 0)
				numeroPagina = 1;
				
			request.setAttribute("numeroPagina", numeroPagina);
			
			numeroRespuesta = 10 * (numeroPagina - 1);
		}
		
		Respuestas_Model parametroRespuestas = new Respuestas_Model();
		parametroRespuestas.setUsuario_Responde(IdUsuario);
		parametroRespuestas.setNumeroRespuestaPagina(numeroRespuesta);
		
		List<Respuestas_Model> respuestasUsuarioElegido = null;
		
		try {
			respuestasUsuarioElegido = RespuestasDAO.getRespuesta("UR", parametroRespuestas);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		request.setAttribute("respuestasUsuarioElegido", respuestasUsuarioElegido); 
	}
	
	private void getPreguntasFavoritasUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long IdUsuario = Long.parseLong(request.getParameter("IdUsuario"));
		
		int numeroPregunta = 0;
		if (request.getParameter("numeroPagina") != null) {
			int numeroPagina = Integer.valueOf(request.getParameter("numeroPagina"));
			if (numeroPagina <= 0)
				numeroPagina = 1;
				
			request.setAttribute("numeroPagina", numeroPagina);
			
			numeroPregunta = 10 * (numeroPagina - 1);
		}
		
		Preguntas_Model parametroPreguntasFavoritas = new Preguntas_Model();
		parametroPreguntasFavoritas.setUsuario_Pregunta(IdUsuario);
		parametroPreguntasFavoritas.setNumeroPregunta(numeroPregunta);
		
		List<Preguntas_Model> preguntasFavoritasUsuarioElegido = null;
		
		try {
			preguntasFavoritasUsuarioElegido = PreguntasDAO.getPregunta("UPF", parametroPreguntasFavoritas);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		request.setAttribute("preguntasUsuarioElegidoFavoritas", preguntasFavoritasUsuarioElegido); 
	}
	
	private void getPreguntasUtilesParaUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long IdUsuario = Long.parseLong(request.getParameter("IdUsuario"));
		
		int numeroPregunta = 0;
		if (request.getParameter("numeroPagina") != null) {
			int numeroPagina = Integer.valueOf(request.getParameter("numeroPagina"));
			if (numeroPagina <= 0)
				numeroPagina = 1;
				
			request.setAttribute("numeroPagina", numeroPagina);
			
			numeroPregunta = 10 * (numeroPagina - 1);
		}
		
		Preguntas_Model parametroPreguntasUtiles = new Preguntas_Model();
		parametroPreguntasUtiles.setUsuario_Pregunta(IdUsuario);
		parametroPreguntasUtiles.setNumeroPregunta(numeroPregunta);
		
		List<Preguntas_Model> preguntasUtilesUsuarioElegido = null;
		
		try {
			preguntasUtilesUsuarioElegido = PreguntasDAO.getPregunta("UPU", parametroPreguntasUtiles);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		request.setAttribute("preguntasUtilesUsuarioElegido", preguntasUtilesUsuarioElegido); 
	}
	
	private void getPreguntasNoUtilesParaUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long IdUsuario = Long.parseLong(request.getParameter("IdUsuario"));

		int numeroPregunta = 0;
		if (request.getParameter("numeroPagina") != null) {
			int numeroPagina = Integer.valueOf(request.getParameter("numeroPagina"));
			if (numeroPagina <= 0)
				numeroPagina = 1;
				
			request.setAttribute("numeroPagina", numeroPagina);
			
			numeroPregunta = 10 * (numeroPagina - 1);
		}
		
		
		Preguntas_Model parametroPreguntasNoUtiles = new Preguntas_Model();
		parametroPreguntasNoUtiles.setUsuario_Pregunta(IdUsuario);
		parametroPreguntasNoUtiles.setNumeroPregunta(numeroPregunta);
		
		List<Preguntas_Model> preguntasNoUtilesUsuarioElegido = null;
		
		try {
			preguntasNoUtilesUsuarioElegido = PreguntasDAO.getPregunta("UPN", parametroPreguntasNoUtiles);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		request.setAttribute("preguntasNoUtilesUsuarioElegido", preguntasNoUtilesUsuarioElegido); 
	}
}
