<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>
<%@page import="com.AskingProject.controllers.GeneralServlet"%>


<%
Preguntas_Model iPregunta = (Preguntas_Model) request.getAttribute("iPregunta");
pageContext.setAttribute("iPregunta", iPregunta);
%>

<div class="qa_qa-container">
	<a href="ProfileUsuario?IdUsuario=${iPregunta.getUsuario_Pregunta()}">
		<div class="user-container_qa">
			
			<div class="img-container_user">
					<img id="img_container" src="GeneralServlet?Imagen=Usuario&Id=${iPregunta.getUsuario_Pregunta()}" alt="imagen-usuario">
			</div>
			
			<div class="username_user">
				${iPregunta.getUsernameUsuarioPregunta()}
			</div>

		</div>
	 </a>
	<a class="link_qa" href="QAPreguntasYRespuestas?IdPregunta=${iPregunta.getIdPregunta()}&numeroPagina=1">
		<div class="text_qa">
			<p>${iPregunta.getTituloPregunta()}</p>
		</div>
	</a>
</div>