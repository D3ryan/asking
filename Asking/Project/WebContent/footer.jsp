<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>
<%@page import="com.AskingProject.controllers.GeneralServlet"%>

<%

List<Categorias_Model> listaCategorias = GeneralServlet.getCategorias();
pageContext.setAttribute("listaCategorias", listaCategorias);

int categoryCounter = 4;
%>

<p class="category-title">Lista de Categorías</p>

<div class="category-container">

	<div class="grid-container">
		<c:forEach var="iCategoria" items="${listaCategorias}">
			
				<a class="link" href="SearchPreguntas?Busqueda=Preguntas&numeroPagina=1&question=&categories=${iCategoria.getIdCategoria()}"> 
				${iCategoria.getTituloCategoria()} </a>
	

		</c:forEach>
	</div>
	
</div>

