<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>

<%
List<Preguntas_Model> lista10Preguntas = (List<Preguntas_Model>) request.getAttribute("lista10Preguntas");

pageContext.setAttribute("lista10Preguntas", lista10Preguntas);

long IdUsuarioActivo = -1;
if (request.getAttribute("IdUsuarioActivo") != null){
	IdUsuarioActivo = (long)request.getAttribute("IdUsuarioActivo");
	pageContext.setAttribute("IdUsuarioActivo", IdUsuarioActivo);
}

byte EstadoUsuario = 0;
if (request.getAttribute("EstadoUsuario") != null){
	EstadoUsuario = (byte)request.getAttribute("EstadoUsuario");
	pageContext.setAttribute("EstadoUsuario", EstadoUsuario);
}

int numeroPagina = 1;
if (request.getAttribute("numeroPagina") != null){
	numeroPagina = (int)request.getAttribute("numeroPagina");
	pageContext.setAttribute("numeroPagina", numeroPagina);
}

%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/navbar.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/ea1f7a49e6.js"
	crossorigin="anonymous"></script>
<script src="js/jquery.js"></script>
<script src="js/mainjquery.js"></script>
<title>Asking</title>
</head>
<body>
	<header>
		<div id="navbarPage"></div>
	</header>
	
	<main>
		<div class="qa-container">
			<c:if test="${IdUsuarioActivo != -1 && EstadoUsuario != 0}">
				<div class="askbar_qa-container">
					<label class="label_askbar">¿Deseas preguntar algo?</label>
					<a href="SubirPreguntas"><button class="button_askbar">¡Pregunta YA!</button></a>
				</div>
			</c:if>

			<c:forEach var="iPregunta" items="${lista10Preguntas}">
				
				<c:set var="iPregunta" value="${iPregunta}" scope="request"/>
				<jsp:include page="questionView.jsp"></jsp:include>
				
			</c:forEach>


			<div class="button-container_qa-container">
				<c:if test="${numeroPagina - 1 == 0}">
					<a href="IndexPreguntas?numeroPagina=1">
						<button class="btn_button-container">
							<i class="fas fa-angle-left"></i>
						</button>

					</a>
				</c:if>
				
				<c:if test="${numeroPagina - 1 != 0}">
					<a href="IndexPreguntas?numeroPagina=${numeroPagina - 1}">
						<button class="btn_button-container">
							<i class="fas fa-angle-left"></i>
						</button>

					</a>
				</c:if>

				<div class="counter_button-container">
					${numeroPagina}
				</div>

				<a href="IndexPreguntas?numeroPagina=${numeroPagina + 1}">
					<button class="btn_button-container">
						<i class="fas fa-angle-right"></i>
					</button>
				</a>
			</div>
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>