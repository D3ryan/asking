<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>
    
<%
Usuarios_Model usuarioElegido = (Usuarios_Model) request.getAttribute("usuarioElegido");
pageContext.setAttribute("usuarioElegido", usuarioElegido);
%>
    
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/edit.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/validation.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery.js"></script> 
	<script src ="js/jquery.validate.min.js"></script>
	<script src ="js/mainjquery.js"></script>
    <title>Asking</title>
</head>
<body>
	<header>
		<div id="navbarPage"></div>
	</header>
	<main>
        <p class="config-text">Configuración de la cuenta</p>
        <div class="flex-container">
            <div class="main-container">
		
				<form class="form_main-container" id="form_main-container" method = "post" action = "EditarPerfilUsuario" enctype="multipart/form-data">
			
					<div class="user_main-container">
						<c:if test="${ usuarioElegido.isImagenPerfilUsuario() != null }">
							<img id="img_user" src="GeneralServlet?Imagen=Usuario&Id=${usuarioElegido.getIdUsuario()}" alt="imagen-usuario">
						</c:if>
						<c:if test="${ usuarioElegido.isImagenPerfilUsuario() == null }"> <!-- Segun yo esto nunca va a suceder -->
							<img id="img_user" src="img/user-img.png" alt="imagen-usuario">
						</c:if>
						<div class="overlay_user">
							<label for="imagen-edit" id="text_overlay">Subir imagen</label>
            				<input type="file" name="imagen" id="imagen-edit" class="imagen ignore"  />
						</div>
					</div>
					<div class="input-container_form">
						<div class="left-column_form">
							<div class="name-container">
								<label for="name">Nombres:</label> 
								<input type="text" id="name-" name="name" value = "${usuarioElegido.getNombreUsuario()}" required> 
							</div>
							<div class="lastnameP-container">
								<label for="lastname">Apellido Paterno:</label> 
								<input type="text" name="lastnameP" value = "${usuarioElegido.getApellidoPaternoUsuario()}" required>
							</div>
							<div class="lastnameM-container">
								<label for="lastname">Apellido Materno:</label> 
								<input type="text" name="lastnameM" value = "${usuarioElegido.getApellidoMaternoUsuario()}" required>  
							</div>
							<div class="birthday-container">
								<label for="birthday">Fecha de nacimiento:</label> 
								<input type="date" name="birthday" value = "${usuarioElegido.getFechaNacimientoUsuario()}" required>
							</div>
						</div>
						<div class="right-column_form">
							<div class="email-container">
								<label for="email">Email</label> 
								<input type="email" name="email" value = "${usuarioElegido.getCorreoUsuario()}" required> 
							</div>
							<div class="user-container">
								<label for="username">Nombre de usuario</label>
								<input type="text" name="username" value = "${usuarioElegido.getUsernameUsuario()}" required> 
							</div>
							<div class="pass-container">
								<label for="password">Contraseña:</label> 
								<input type="password" name="password" value = "${usuarioElegido.getPasswordUsuario()}" required>
							</div>
						</div>
					</div>
					
				</form>
				<button id="button-edit_main-container" form="form_main-container">Actualizar</button>
			</div>
        </div>
			
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>