<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>
<%@page import="com.AskingProject.controllers.GeneralServlet"%>

<%
Usuarios_Model usuarioElegido = GeneralServlet.getUsuario(request, response);
pageContext.setAttribute("usuarioElegido", usuarioElegido);

List<Categorias_Model> listaCategorias = GeneralServlet.getCategorias();
pageContext.setAttribute("listaCategorias", listaCategorias);
%>

<nav>
	<div class="top-nav">
		<a href="IndexPreguntas?numeroPagina=1" id="asking_top-nav">Asking?</a>
		<form class="search_top-nav" action = "SearchPreguntas">
			<input name="Busqueda" type="hidden" value="Preguntas">
			<input type="text" name = "question">
			<button>
				<i class="fas fa-search"></i>
			</button>
		</form>
		<div class="user_top-nav">

			<c:if test="${empty usuarioElegido}">
				<a href="login.jsp" class="guest-text_top-nav">
					Invitado
					<img class="guest-icon_top-nav" src="img/user-img.png" alt="imagen-usuario">
				</a>
			</c:if>

			<c:if test="${not empty usuarioElegido}">
				<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}" class="guest-text_top-nav"> 
					<c:out value="${usuarioElegido.getUsernameUsuario()}"></c:out>
					<img class="guest-icon_top-nav" src="GeneralServlet?Imagen=Usuario&Id=${usuarioElegido.getIdUsuario()}" alt="imagen-usuario">
				</a>
			</c:if>
		</div>

		<c:if test="${not empty usuarioElegido}">
			<div class="sign-out_top-nav">
				<a href="LoginUsuario" class="signout-icon_top-nav"> 
					<i class="fas fa-door-open"></i>
				</a>
			</div>
		</c:if>
	</div>
	<div class="bottom-nav">
		<a href="SearchPreguntas"><i class="fas fa-filter"></i></a>
		<c:forEach var="iCategoria" items="${listaCategorias}" begin = "0" end = "5" >
			<a href="SearchPreguntas?Busqueda=Preguntas&numeroPagina=1&question=&categories=${iCategoria.getIdCategoria()}">
				${iCategoria.getTituloCategoria()}
			</a>
		</c:forEach>
	</div>
</nav>
