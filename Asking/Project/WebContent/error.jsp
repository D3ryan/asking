<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import = "com.AskingProject.models.*" %>
<%@page import = "com.AskingProject.utils.userType" %>
<%@page import = "java.util.*" %>


<%
userType usuarioActivo = (userType)request.getAttribute("usuarioActivo");
pageContext.setAttribute("usuarioActivo", usuarioActivo);

String error = request.getAttribute("error").toString();
%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/navbar.css">
<link rel="stylesheet" type="text/css" href="css/error.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="stylesheet" type="text/css" href="css/validation.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
<script src="https://kit.fontawesome.com/ea1f7a49e6.js"
	crossorigin="anonymous"></script>
<script src="js/jquery.js"></script> 
<script src ="js/jquery.validate.min.js"></script>
<script src ="js/mainjquery.js"></script>
<title>Asking</title>
</head>
<body>
	<header>
       	<div id="navbarPage"></div>
    </header>
	<main>
		<div class="errorMainContainer">
            <div class="errorContainer">
                <span class="bug-icon"><i class="fas fa-bug"></i></span>
                <h1 class="errorHeader">¡Parece que hubo un error!</h1>
                <p  class="errorSugParagraph">Será mejor regresar...</p>
                <p  class="errorParagraph">Tipo de error: ${error}</p>
            </div>
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</body>
</html>