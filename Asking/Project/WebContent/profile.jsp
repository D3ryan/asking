<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
	   
	    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import = "com.AskingProject.models.*" %>
<%@page import = "java.util.*" %>


<%
Usuarios_Model usuarioElegido = (Usuarios_Model)request.getAttribute("usuarioElegido");
pageContext.setAttribute("usuarioElegido", usuarioElegido);

long IdUsuarioActivo = (long)request.getAttribute("IdUsuarioActivo");
pageContext.setAttribute("IdUsuarioActivo", IdUsuarioActivo);

List<Preguntas_Model> listaPreguntasUsuario = (List<Preguntas_Model>) request.getAttribute("preguntasUsuarioElegido");
pageContext.setAttribute("listaPreguntasUsuario", listaPreguntasUsuario);


List<Respuestas_Model> listaRespuestasUsuario = (List<Respuestas_Model>) request.getAttribute("respuestasUsuarioElegido");
pageContext.setAttribute("listaRespuestasUsuario", listaRespuestasUsuario);

List<Preguntas_Model> listaPreguntasFavoritasUsuario = (List<Preguntas_Model>) request.getAttribute("preguntasUsuarioElegidoFavoritas");
pageContext.setAttribute("listaPreguntasFavoritasUsuario", listaPreguntasFavoritasUsuario);


List<Preguntas_Model> listaPreguntasUtilesUsuario = (List<Preguntas_Model>) request.getAttribute("preguntasUtilesUsuarioElegido");
pageContext.setAttribute("listaPreguntasUtilesUsuario", listaPreguntasUtilesUsuario);


List<Preguntas_Model> listaPreguntasNoUtilesUsuario = (List<Preguntas_Model>) request.getAttribute("preguntasNoUtilesUsuarioElegido");
pageContext.setAttribute("listaPreguntasNoUtilesUsuario", listaPreguntasNoUtilesUsuario);


int numeroPagina = 1;
if (request.getAttribute("numeroPagina") != null){
	numeroPagina = (int)request.getAttribute("numeroPagina");
	pageContext.setAttribute("numeroPagina", numeroPagina);
}

String pagina = (String)request.getAttribute("pagina");
pageContext.setAttribute("pagina", pagina);
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/navbar.css">
<link rel="stylesheet" type="text/css" href="css/profile.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/ea1f7a49e6.js"
	crossorigin="anonymous"></script>
	
	
<script src="js/jquery.js"></script> 
<script src ="js/mainjquery.js"></script>
<title>Asking</title>
</head>
<body>
	<header>
		<div id="navbarPage"></div>
	</header>
	<main>
		<div class="left-side_main">
			<div class="qa-container_left-side">
				<div class="bar_qa-container">

					<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=preguntas&numeroPagina=1">
						<button class="button_bar">
							Preguntas<span class="counter_button">${usuarioElegido.getCantPreguntasUsuario()}</span class="counter_button">
						</button>
					</a> 
					<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=respuestas&numeroPagina=1">
						<button class="button_bar">
							Respuestas<span class="counter_button">${usuarioElegido.getCantRespuestasUsuario()}</span class="counter_button">
						</button>
					</a> 
					<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=preguntasFavoritas&numeroPagina=1">
						<button class="button_bar">
							Favoritos<span class="counter_button">${usuarioElegido.getCantPreguntasFavoritasUsuario()}</span class="counter_button">
						</button>
					</a> 
					<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=preguntasUtiles&numeroPagina=1">
						<button class="button_bar">
							Útil<span class="counter_button">${usuarioElegido.getCantPreguntasUtilesUsuario()}</span class="counter_button">
						</button>
					</a>
					<c:if test="${IdUsuarioActivo == usuarioElegido.getIdUsuario()}">
						<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=preguntasNoUtiles&numeroPagina=1">
							<button class="button_bar">
								No Útil<span class="counter_button">${usuarioElegido.getCantPreguntasNoUtilesUsuario()}</span class="counter_button">
							</button>
						</a>
					</c:if>
				</div>


				<c:choose>
					<c:when test="${pagina == 'preguntas'}">
						<c:forEach var="iPregunta" items="${listaPreguntasUsuario}">
						
							<c:set var="iPregunta" value="${iPregunta}" scope="request"/>
							<jsp:include page="questionView.jsp"></jsp:include>

						</c:forEach>
					</c:when>

					<c:when test="${pagina == 'respuestas'}">
						<c:forEach var="iRespuesta" items="${listaRespuestasUsuario}">
					
						<div class="qa_qa-container">
							<a href="ProfileUsuario?IdUsuario=${iRespuesta.getUsuario_Responde()}">
								<div class="user-container_qa">
									<div class="img-container_user">
										<img id="img_container"
											src="GeneralServlet?Imagen=Usuario&Id=${iRespuesta.getUsuario_Responde()}"
											alt="imagen-usuario">
									</div>
									<div class="username_user">
										${iRespuesta.getUsernameUsuarioResponde()}
									</div>
								</div>
							</a>
							<a href="QAPreguntasYRespuestas?IdPregunta=${iRespuesta.getPregunta_Respondida()}&numeroPagina=1">
								<div class="link_qa">
									<p>${iRespuesta.getTextoRespuesta()}</p>
								</div>
							</a>
						</div>
						</c:forEach>
					</c:when>
					
					<c:when test="${pagina == 'preguntasFavoritas'}"> 
						<c:forEach var="iPregunta" items="${listaPreguntasFavoritasUsuario}">
						
							<c:set var="iPregunta" value="${iPregunta}" scope="request"/>
							<jsp:include page="questionView.jsp"></jsp:include>

						</c:forEach>
					</c:when>
					
					<c:when test="${pagina == 'preguntasUtiles'}"> 
						<c:forEach var="iPregunta" items="${listaPreguntasUtilesUsuario}">
						
							<c:set var="iPregunta" value="${iPregunta}" scope="request"/>
							<jsp:include page="questionView.jsp"></jsp:include>

						</c:forEach>
					</c:when>
					
					<c:when test="${pagina == 'preguntasNoUtiles'}">
						<c:if test="${IdUsuarioActivo == usuarioElegido.getIdUsuario()}"> 
							<c:forEach var="iPregunta" items="${listaPreguntasNoUtilesUsuario}">
						
								<c:set var="iPregunta" value="${iPregunta}" scope="request"/>
								<jsp:include page="questionView.jsp"></jsp:include>

							</c:forEach>
						</c:if>
					</c:when>

					<c:otherwise> 
						
					</c:otherwise>
				</c:choose>

				<c:if test="${not empty pagina}">

					<div class="button-container_qa-container">
						<c:if test="${numeroPagina - 1 == 0}">
							<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=${pagina}&numeroPagina=1">
								<button class="btn_button-container">
									<i class="fas fa-angle-left"></i>
								</button>
							</a>
						</c:if>

						<c:if test="${numeroPagina - 1 != 0}">
							<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=${pagina}&numeroPagina=${numeroPagina - 1}">
								<button class="btn_button-container">
									<i class="fas fa-angle-left"></i>
								</button>
							</a>
						</c:if>

						<div class="counter_button-container">${numeroPagina}</div>

						<a href="ProfileUsuario?IdUsuario=${usuarioElegido.getIdUsuario()}&pagina=${pagina}&numeroPagina=${numeroPagina + 1}">
							<button class="btn_button-container">
								<i class="fas fa-angle-right"></i>
							</button>
						</a>
					</div>

				</c:if>
			</div>
		</div>
		
		<div class="right-side_main">
			<div class="user-container_right-side">
				<div class="img-container_user-profile">
					<img id="img_user" src="GeneralServlet?Imagen=Usuario&Id=${usuarioElegido.getIdUsuario()}" alt="imagen-usuario">
				</div>
				<div class="info_user">
					<p class="">${usuarioElegido.getUsernameUsuario()}</p>
					<c:if test="${IdUsuarioActivo == usuarioElegido.getIdUsuario()}">
						<p>${usuarioElegido.getNombreCompletoUsuario()}</p>
						<p>${usuarioElegido.getCorreoUsuario()}</p>
						<p>${usuarioElegido.getEdad()}</p>
						<p>${usuarioElegido.getEstadoUsuarioString()}</p>
					</c:if>
				</div>


				<c:if test="${IdUsuarioActivo == usuarioElegido.getIdUsuario()}">
					<a href="EditarPerfilUsuario">
						<button class="button_user">Editar</button>
					</a>
				</c:if>
			</div>
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>