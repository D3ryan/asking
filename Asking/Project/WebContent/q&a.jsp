<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
	
	
	    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import = "com.AskingProject.models.*" %>
<%@page import = "com.AskingProject.utils.userType" %>
<%@page import = "java.util.*" %>


<%
userType usuarioActivo = (userType)request.getAttribute("usuarioActivo");
pageContext.setAttribute("usuarioActivo", usuarioActivo);

long IdUsuarioActivo = (long)request.getAttribute("IdUsuarioActivo");
pageContext.setAttribute("IdUsuarioActivo", IdUsuarioActivo);

Preguntas_Model preguntaElegida = (Preguntas_Model) request.getAttribute("preguntaElegida");
pageContext.setAttribute("preguntaElegida", preguntaElegida);


Respuestas_Model respuestaCorrecta = (Respuestas_Model) request.getAttribute("respuestaCorrecta");
pageContext.setAttribute("respuestaCorrecta", respuestaCorrecta);

List<Respuestas_Model> lista10Respuestas = (List<Respuestas_Model>) request.getAttribute("lista10Respuestas");
pageContext.setAttribute("lista10Respuestas", lista10Respuestas);

int numeroPagina = 1;

if (request.getAttribute("numeroPagina") != null){
	numeroPagina = (int)request.getAttribute("numeroPagina");
	pageContext.setAttribute("numeroPagina", numeroPagina);
}
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/navbar.css">
<link rel="stylesheet" type="text/css" href="css/q&a.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="stylesheet" type="text/css" href="css/jquery.sweet-modal.min.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
<script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
<script src="js/jquery.js"></script> 
<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.sweet-modal.min.js"></script>
<script src="js/mainjquery.js"></script>
<title>Asking</title>
</head>
<body>
	<header>
		<div id="navbarPage"></div>
	</header>
	<main>	
		<div class="qa-container">
			<div class="question-container_qa">
				<div class="user-container_question">
					<a href="ProfileUsuario?IdUsuario=${preguntaElegida.getUsuario_Pregunta()}" class="guest-text_top-nav">

						<div class="img-container_user">
							<img id="img_container" src="GeneralServlet?Imagen=Usuario&Id=${preguntaElegida.getUsuario_Pregunta()}" alt="imagen-usuario">
						</div>

						<div class="username_user">
							${preguntaElegida.getUsernameUsuarioPregunta()}
						</div>
						
					</a>
				</div>
				<div class="text-rating_question">
					<div class="category-erase_text">
						<div>
							<span class="category">${preguntaElegida.getTituloCategoriaPregunta()}</span>
							<c:if test="${preguntaElegida.getEditadoPregunta() == 1}">
								<p class="editado-pregunta">Editado</p>
							</c:if>
						</div>
						<c:if test="${usuarioActivo == userType.questionOwner}">
							<form class="form-erase" id="form-erase" action="QAPreguntasYRespuestas" method="post">
								<input name="tipo" type="hidden" value="BorrarPregunta">
								<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
								<button class="button-erase" type="button" id="btn-erase"><i class="fas fa-times-circle"></i></button>
							</form>
						</c:if>
					</div>

					<p class="text">${preguntaElegida.getTituloPregunta()}</p>

					<!-- 					TODO: A LA DESCRIPCION LE AGREGAMOS UN DISEï¿½O? O ASï¿½ COMO ESTA -->
					<c:if test="${preguntaElegida.getDescripcionPregunta() != ''}">
						<p class="text">${preguntaElegida.getDescripcionPregunta()}</p>
					</c:if>
					
					<c:if test="${preguntaElegida.isImagenPregunta() != null}">
						<div class="img-container">
							<img src="GeneralServlet?Imagen=Pregunta&Id=${preguntaElegida.getIdPregunta()}" alt="imagen" class="img" style="width: 450px; height: 250px;">
						</div>
					</c:if>
					
					<div class="rating-container_question">
						<div class="fav_rating">
						
							<c:if test="${usuarioActivo == userType.normalUser}">
								<form id="form_main-container-fav-p${preguntaElegida.getIdPregunta()}" method = "post" action = "QAPreguntasYRespuestas">
									<input name="tipo" type="hidden" value="Pregunta">
									<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
									<input name="Vote" type="hidden" value="Fav">
								</form>
							</c:if>
							
							<button class="button_fav" form="form_main-container-fav-p${preguntaElegida.getIdPregunta()}">
								<i class="fas fa-star"></i>
							</button>
							<span class="counter_fav">${preguntaElegida.getCantidadVotosFavoritos()}</span>
						</div>
						<div class="useful_rating">
						
							<c:if test="${usuarioActivo == userType.normalUser}">
								<form id="form_main-container-useful-p${preguntaElegida.getIdPregunta()}" method = "post" action = "QAPreguntasYRespuestas">
									<input name="tipo" type="hidden" value="Pregunta">
									<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
									<input name="Vote" type="hidden" value="Util">
								</form>
							</c:if>
							
							<button class="button_useful" form="form_main-container-useful-p${preguntaElegida.getIdPregunta()}">
								<i class="fas fa-thumbs-up"></i>
							</button>
							<span class="counter_unuseful">${preguntaElegida.getCantidadVotosUtiles()}</span>
						</div>
						<div class="unuseful_rating">
						
							<c:if test="${usuarioActivo == userType.normalUser}">
								<form id="form_main-container-unuseful-p${preguntaElegida.getIdPregunta()}" method = "post" action = "QAPreguntasYRespuestas">
									<input name="tipo" type="hidden" value="Pregunta">
									<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
									<input name="Vote" type="hidden" value="NoUtil">
								</form>
							</c:if>
							
							<button class="button_unuseful" form="form_main-container-unuseful-p${preguntaElegida.getIdPregunta()}">
								<i class="fas fa-thumbs-down"></i>
							</button>
							<c:if test="${usuarioActivo == userType.questionOwner}">
								<span class="counter_unuseful">${preguntaElegida.getCantidadVotosNoUtiles()}</span>
							</c:if>
						</div>
						
						<div class="date-container_text">
							<span class="datetime">${preguntaElegida.getFechaCreacionPreguntaToString()} </span>
						</div>
						<c:if test="${usuarioActivo == userType.questionOwner}">
							<div class="edit_text">							
								<form class="form_main-container" id="form_main-container-p${preguntaElegida.getIdPregunta()}" method = "post" action = "SubirPreguntas" enctype="multipart/form-data">
									<input name="abrirPregunta" type="hidden" value="true">
									<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
								</form>
									<button form="form_main-container-p${preguntaElegida.getIdPregunta()}" class="button_edit">
										<i class="fas fa-edit"></i>
									</button>
							</div>
						</c:if>
						<c:if test="${usuarioActivo == userType.normalUser}">
							<div class="answer_text">
								<form class="form_main-container" id="form_main-container-p${preguntaElegida.getIdPregunta()}" method="post" action="SubirRespuestas" enctype="multipart/form-data">
									<button class="button_aswer">Responder</button>
									<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
									<input name= "abrirRespuesta" type="hidden" value="true">
<%-- 									<input name="IdRespuesta" type="hidden" value="${preguntaRespuestaElegida.getId()}"> --%>
								</form>
							</div>
						</c:if>
					</div>
				</div>
			</div>


			<c:if test="${not empty respuestaCorrecta}">
			
				<c:if test="${IdUsuarioActivo == respuestaCorrecta.getUsuario_Responde()  && usuarioActivo != userType.Invited}">
					<c:set var = "usuarioActivo" value = "${userType.answerOwner}"></c:set>
				</c:if>

				<div class="correct-answer_qa">
					<div class="user-container_question">
						<a href="ProfileUsuario?IdUsuario=${respuestaCorrecta.getUsuario_Responde()}" class="guest-text_top-nav"> 
					
							<div class="correct-img-container_user">
								<img id="img_container" src="GeneralServlet?Imagen=Usuario&Id=${respuestaCorrecta.getUsuario_Responde()}" alt="imagen-usuario">
							</div>
							<div class="username_user">
								${respuestaCorrecta.getUsernameUsuarioResponde()}
							</div>
						</a>
						
						<button class="correct-button_user">
							<i class="fas fa-check"></i>
						</button>
					</div>
					<div class="text-rating_question">
						<c:if test="${respuestaCorrecta.getEditadoRespuesta() == 1}">
							<p class="editado-respuesta">Editado</p>
						</c:if>
						<c:if test="${usuarioActivo == userType.answerOwner}">
							<form class="form-erase-answer" id="form-erase" action="QAPreguntasYRespuestas" method="post">
								<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
								<input name="tipo" type="hidden" value="BorrarRespuesta">
								<input name="IdRespuesta" type="hidden" value="${respuestaCorrecta.getIdRespuesta()}">
								<button class="button-erase" type="button" id="btn-erase"><i class="fas fa-times-circle"></i></button>
							</form>
						</c:if>
						
						<p class="text">${respuestaCorrecta.getTextoRespuesta()}</p>
						<!-- 						TODO: HACE FALTA EL LUGAR DONDE SE PONDRA LA IMAGEN DE LA RESPUESTA, POR MIENTRAS PONDRE ESTO -->
						
						<c:if test="${respuestaCorrecta.isImagenRespuesta() != null}">
							<div class="img-container">
								<img src="GeneralServlet?Imagen=Respuesta&Id=${respuestaCorrecta.getIdRespuesta()}" alt="lap" class="img" style="width: 450px; height: 250px;">
							</div>
						</c:if>
						
						<c:if test="${usuarioActivo == userType.questionOwner}">
							<form id="form_main-container-delete-correct-answer-rc${respuestaCorrecta.getIdRespuesta()}" method = "post" action = "QAPreguntasYRespuestas">
								<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
								<input name="tipo" type="hidden" value="RespuestaCorrecta">
								<input name="IdRespuesta" type="hidden" value="${respuestaCorrecta.getIdRespuesta()}">
<!-- 								<input type="submit" id="correct" name="correct"> -->
							</form>
						</c:if>
						

						<div class="rating-container_question">
							<div class="useful_rating">
								<c:if test="${usuarioActivo == userType.normalUser || usuarioActivo == userType.questionOwner}">
<!-- 								FORM PARA INGRESAR VOTO -->
									<form id="form_main-container-correct-useful-answer-r${respuestaCorrecta.getIdRespuesta()}" method = "post" action = "QAPreguntasYRespuestas">
										<input name="tipo" type="hidden" value="Respuesta">
										<input name="IdRespuesta" type="hidden" value="${respuestaCorrecta.getIdRespuesta()}">
										<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
										<input name="Vote" type="hidden" value="Util">
									</form>
								</c:if>
							
								<button class="button_useful" form = "form_main-container-correct-useful-answer-r${respuestaCorrecta.getIdRespuesta()}">
									<i class="fas fa-thumbs-up"></i>
								</button>
<%-- 							<span class="correct-counter_useful">${respuestaCorrecta.getCantidadVotosUtiles()}</span> --%>
								<span class="counter_useful">${respuestaCorrecta.getCantidadVotosUtiles()}</span>
							</div>
							<div class="unuseful_rating">
								<c:if test="${usuarioActivo == userType.normalUser || usuarioActivo == userType.questionOwner}">
<!-- 								FORM PARA INGRESAR VOTO -->
									<form id="form_main-container-correct-unuseful-answer-r${respuestaCorrecta.getIdRespuesta()}" method = "post" action = "QAPreguntasYRespuestas">
										<input name="tipo" type="hidden" value="Respuesta">
										<input name="IdRespuesta" type="hidden" value="${respuestaCorrecta.getIdRespuesta()}">
										<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
										<input name="Vote" type="hidden" value="NoUtil">
									</form>
								</c:if>
								
								<button class="button_unuseful" form = "form_main-container-correct-unuseful-answer-r${respuestaCorrecta.getIdRespuesta()}">
									<i class="fas fa-thumbs-down"></i>
								</button>
								<c:if test="${usuarioActivo == userType.answerOwner}">
									<span class="counter_unuseful">${respuestaCorrecta.getCantidadVotosNoUtiles()}</span>
								</c:if>
							</div>
							
<!-- 							Aqui es lo de la respuesa correcta -->
							<c:if test="${usuarioActivo == userType.questionOwner}">
								<div class="correct-answer-container">
									<form class="form-correct" id = "form-correct-answer-rc${respuestaCorrecta.getIdRespuesta()}" method = "post" action ="QAPreguntasYRespuestas">
										<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
										<input name="tipo" type="hidden" value="RespuestaCorrecta">
										<input name="IdRespuesta" type="hidden" value="${respuestaCorrecta.getIdRespuesta()}">
										<label class="correct-label" for="correct-r${respuestaCorrecta.getIdRespuesta()}"><i class="fas fa-check"></i></label>
										<input type="submit" class="correct-input" id="correct-r${respuestaCorrecta.getIdRespuesta()}" name="correct">
									</form>
								</div>
							</c:if>
							
							<div class="date-container_text">
<%-- 							<span class="correct-datetime">${respuestaCorrecta.getFechaCreacionRespuestaToString()}</span> --%>
								<span class="datetime">${respuestaCorrecta.getFechaCreacionRespuestaToString()}</span>
							</div>
							<c:if test="${usuarioActivo == userType.answerOwner}">
								<div class="edit_text">
									<form class="form_edit" id="form_edit-rc${respuestaCorrecta.getIdRespuesta()}" method = "post" action = "SubirRespuestas" enctype="multipart/form-data">
											<input name="abrirRespuesta" type="hidden" value="true">
											<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
											<input name="IdRespuesta" type="hidden" value="${respuestaCorrecta.getIdRespuesta()}">
											<button class="correct-button_edit">
												<i class="fas fa-edit"></i>
											</button>
										</form>
								</div>
							</c:if>
						</div>
					</div>
				</div>
				
				<c:if test="${usuarioActivo == userType.answerOwner}">
					<c:set var = "usuarioActivo" value = "${userType.normalUser}"></c:set>
				</c:if>
			</c:if>


			<c:forEach var="iRespuesta" items="${lista10Respuestas}">
				<c:if test="${iRespuesta.getIdRespuesta() != preguntaElegida.getRespuesta_Correcta()}">
					
					<c:if test="${iRespuesta.getEstadoRespuesta() == 1}">
					
						<c:if test="${IdUsuarioActivo == iRespuesta.getUsuario_Responde() && usuarioActivo != userType.Invited}">
							<c:set var = "usuarioActivo" value = "${userType.answerOwner}"></c:set>
						</c:if>
					
					
						<div class="answer-container_qa">
							<div class="user-container_question">
								<a href="ProfileUsuario?IdUsuario=${iRespuesta.getUsuario_Responde()}" class="guest-text_top-nav">
									<div class="img-container_user">
										<img id="img_container" src="GeneralServlet?Imagen=Usuario&Id=${iRespuesta.getUsuario_Responde()}" alt="imagen-usuario">
									</div>
									<div class="username_user">
										${iRespuesta.getUsernameUsuarioResponde()}
									</div>
								</a>
							</div>
							<div class="text-rating_question">
								<c:if test="${iRespuesta.getEditadoRespuesta() == 1}">
									<p class="editado-respuesta">Editado</p>
								</c:if>
								
<!-- 								Este es el contenedor del boton para borrar -->
								<c:if test="${usuarioActivo == userType.answerOwner}">
									<form class="form-erase-answer" id="form-erase" action="QAPreguntasYRespuestas" method="post">
										<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
										<input name="tipo" type="hidden" value="BorrarRespuesta">
										<input name="IdRespuesta" type="hidden" value="${iRespuesta.getIdRespuesta()}">
										<button class="button-erase" type="button" id="btn-erase"><i class="fas fa-times-circle"></i></button>
									</form>
								</c:if>

								<p class="text">${iRespuesta.getTextoRespuesta()}</p>
								<!-- 						TODO: HACE FALTA EL LUGAR DONDE SE PONDRA LA IMAGEN DE LA RESPUESTA, POR MIENTRAS PONDRE ESTO -->
								<c:if test="${iRespuesta.isImagenRespuesta() != null}">
									<div class="img-container">
										<img src="GeneralServlet?Imagen=Respuesta&Id=${iRespuesta.getIdRespuesta()}" alt="lap" class="img" style="width: 450px; height: 250px;">
									</div>
								</c:if>
							
								<div class="rating-container_question">
									<div class="useful_rating">
									
										<c:if test="${usuarioActivo == userType.normalUser || usuarioActivo == userType.questionOwner}">
<!-- 										FORM PARA INGRESAR VOTO -->
											<form id="form_main-container-useful-answer-r${iRespuesta.getIdRespuesta()}" method = "post" action = "QAPreguntasYRespuestas">
												<input name="tipo" type="hidden" value="Respuesta">
												<input name="IdRespuesta" type="hidden" value="${iRespuesta.getIdRespuesta()}">
												<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
												<input name="Vote" type="hidden" value="Util">
											</form>
										</c:if>
									
										<button class="button_useful" form="form_main-container-useful-answer-r${iRespuesta.getIdRespuesta()}">
											<i class="fas fa-thumbs-up"></i>
										</button>
										<span class="counter_useful">${iRespuesta.getCantidadVotosUtiles()}</span>
									</div>
									<div class="unuseful_rating">
									
										<c:if test="${usuarioActivo == userType.normalUser || usuarioActivo == userType.questionOwner}">
<!-- 										FORM PARA INGRESAR VOTO -->
											<form id="form_main-container-unuseful-answer-r${iRespuesta.getIdRespuesta()}" method = "post" action = "QAPreguntasYRespuestas">
												<input name="tipo" type="hidden" value="Respuesta">
												<input name="IdRespuesta" type="hidden" value="${iRespuesta.getIdRespuesta()}">
												<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
												<input name="Vote" type="hidden" value="NoUtil">
											</form>
										</c:if>
										
										<button class="button_unuseful" form="form_main-container-unuseful-answer-r${iRespuesta.getIdRespuesta()}">
											<i class="fas fa-thumbs-down"></i>
										</button>
										<c:if test="${usuarioActivo == userType.answerOwner}">
											<span class="counter_unuseful">${iRespuesta.getCantidadVotosNoUtiles()}</span>
										</c:if>
									</div>
<!-- 									Este es el contenedor de la respuesta correcta!! -->
									<c:if test="${usuarioActivo == userType.questionOwner}">
										<div class="correct-answer-container">
											<form class="form-correct" id = "form-correct-answer-r${iRespuesta.getIdRespuesta()}" method = "post" action ="QAPreguntasYRespuestas">
												<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
												<input name="tipo" type="hidden" value="RespuestaCorrecta">
												<input name="IdRespuesta" type="hidden" value="${iRespuesta.getIdRespuesta()}">
												<label class="correct-label" for="correct-r${iRespuesta.getIdRespuesta()}"><i class="fas fa-check"></i></label>
												<input type="submit" class="correct-input" id="correct-r${iRespuesta.getIdRespuesta()}" name="correct">
											</form>
										</div>
									</c:if>


									<div class="date-container_text">
										<span class="datetime">${iRespuesta.getFechaCreacionRespuestaToString()}</span>
									</div>
									<c:if test="${usuarioActivo == userType.answerOwner}">
										<div class="edit_text">
											<form class="form_edit" id="form_edit-r${iRespuesta.getIdRespuesta()}" method = "post" action = "SubirRespuestas" enctype="multipart/form-data">
													<input name="abrirRespuesta" type="hidden" value="true">
													<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
													<input name="IdRespuesta" type="hidden" value="${iRespuesta.getIdRespuesta()}">
<!-- 													TODO: ESTE BUTTON FUNCIONA SIN SU FORM ASOCIADO? -->
													<button class="button_edit">
														<i class="fas fa-edit"></i>
													</button>
											</form>
										</div>
									</c:if>
								</div>
							</div>
						</div>
					
						<c:if test="${usuarioActivo == userType.answerOwner}">
							<c:set var = "usuarioActivo" value = "${userType.normalUser}"></c:set>
						</c:if>
					
					</c:if>
					<c:if test="${iRespuesta.getEstadoRespuesta() == 0}">
						<div class="answer-container_qa">
							<div class="text-rating_question">
								<p class="text">La respuesta ha sido eliminada</p>
							</div>
						</div>
					</c:if>

				</c:if>
				
			</c:forEach>


			<div class="button-container_qa-container">

				<c:if test="${numeroPagina - 1 == 0}">
					<a href="QAPreguntasYRespuestas?IdPregunta=${preguntaElegida.getIdPregunta()}&numeroPagina=1">
						<button class="btn_button-container">
							<i class="fas fa-angle-left"></i>
						</button>

					</a>
				</c:if>
				
				<c:if test="${numeroPagina - 1 != 0}">
					<a href="QAPreguntasYRespuestas?IdPregunta=${preguntaElegida.getIdPregunta()}&numeroPagina=${numeroPagina - 1}">
						<button class="btn_button-container">
							<i class="fas fa-angle-left"></i>
						</button>

					</a>
				</c:if>

				<div class="counter_button-container">
						${numeroPagina}
				</div>
				
				<a href="QAPreguntasYRespuestas?IdPregunta=${preguntaElegida.getIdPregunta()}&numeroPagina=${numeroPagina + 1}">
					<button class="btn_button-container">
						<i class="fas fa-angle-right"></i>
					</button>
				</a>
			
			</div>
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
		
	</footer>
</body>
</html>