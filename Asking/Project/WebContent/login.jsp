<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%

String usuarioIncorrecto = "false";
if(request.getAttribute("usuarioIncorrecto") != null)
	usuarioIncorrecto = request.getAttribute("usuarioIncorrecto").toString();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/navbar.css">
<link rel="stylesheet" type="text/css" href="css/login.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="stylesheet" type="text/css" href="css/validation.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
<script src="js/jquery.js"></script> 
<script src ="js/jquery.validate.min.js"></script>
<script src ="js/mainjquery.js"></script>
<title>Asking</title>
</head>
<body>
	<header>
		<div id="navbarPage"></div>
	</header>
	<main>
		<div class="form-container">
			<div class="leftside-form">
				<div id="asking_form">A?</div>
			</div>
			<form class="rightside-form" id="rightside-form" method = "post" action = "LoginUsuario">
				<div class="email-container">
					<input id="email_form" type="text" name="user" autocomplete="nope" placeholder="Usuario" required>
				</div>
				<div class="pass-container">
					<input id="pass_form" type="password" name="password" placeholder="Contraseña" required>
				</div>
				
				<input id="usuarioIncorrecto" type="hidden" name="usuarioIncorrecto" value="<%=usuarioIncorrecto%>">
				<button id="button_main-container" form="rightside-form">Ingresar</button>
							
				<div class="registration_form">
					¿Aún no tienes una cuenta? <a href="signup.jsp">¡Registrate!</a>
				</div>
			</form>
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</body>
</html>