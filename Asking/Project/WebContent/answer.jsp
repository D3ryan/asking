<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>

<%
Preguntas_Model preguntaElegida = (Preguntas_Model) request.getAttribute("preguntaElegida");
pageContext.setAttribute("preguntaElegida", preguntaElegida);

Usuarios_Model usuarioElegido = (Usuarios_Model)request.getAttribute("IdUsuarioActivo");
pageContext.setAttribute("usuarioElegido", usuarioElegido);

Respuestas_Model respuestaElegida = (Respuestas_Model) request.getAttribute("respuestaElegida");
pageContext.setAttribute("respuestaElegida", respuestaElegida);
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/answer.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/validation.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery.js"></script> 
	<script src ="js/jquery.validate.min.js"></script>
	<script src ="js/mainjquery.js"></script>
    <title>Asking</title>
</head>
<body>
    <header>
        <div id="navbarPage"></div>
    </header>
    <main>
        <div class="qa-container">
            <div class="question-container_qa">
            
                <div class="user-container_question">
                    <div class="img-container_user">
                    
                        <img id="img_container" src="GeneralServlet?Imagen=Usuario&Id=${preguntaElegida.getUsuario_Pregunta()}" alt="imagen-usuario">
                        
                    </div>
                    
                    <div class="username_user">${preguntaElegida.getUsernameUsuarioPregunta()}</div>
                    
                </div>
                <div class="text-rating_question"> 
                
                    <div class="category-container_text"><span class="category">${preguntaElegida.getTituloCategoriaPregunta()}</span></div>
                    
                    <!-- 					TODO: AL APARTADO DE EDITADO LE AGREGAMOS UN DISEï¿½O? O ASï¿½ COMO ESTA -->
                    <c:if test="${preguntaElegida.getEditadoPregunta() == 1}">
						<p class="text" style="font-style: italic;">Editado</p>
					</c:if>
                    
                    <p class="text">${preguntaElegida.getTituloPregunta()}</p>
                    
                    <c:if test="${preguntaElegida.getDescripcionPregunta() != ''}">
						<p class="text">${preguntaElegida.getDescripcionPregunta()}</p>
					</c:if>
                    
                    <c:if test="${preguntaElegida.isImagenPregunta() != null}">
						<div class="img-container">
							<img src="GeneralServlet?Imagen=Pregunta&Id=${preguntaElegida.getIdPregunta()}" alt="lap" class="img" style="width: 450px; height: 250px;">
						</div>
					</c:if>
                    
                    <div class="rating-container_question">
<!--                     	TODO: AQUI DEBERÍA ILUMINARSE LO QUE EL USUARIO VOTO (SI FUE UTIL, NO UTIL, FAV, ETC)  -->
                        <div class="fav_rating">
                            <button class="button_fav"><i class="fas fa-star"></i></button>
                            <span class="counter_fav">${preguntaElegida.getCantidadVotosFavoritos()}</span>
                        </div>
                        <div class="useful_rating">
                            <button class="button_useful"><i class="fas fa-thumbs-up"></i></button>
                            <span class="counter_useful">${preguntaElegida.getCantidadVotosUtiles()}</span>
                        </div>
                        <div class="unuseful_rating">
                            <button class="button_unuseful"><i class="fas fa-thumbs-down"></i></button>
<%--                             <span class="counter_unuseful">${preguntaElegida.getCantidadVotosNoUtiles()}</span> --%>
                        </div>
                        <div class="date-container_text"><span class="datetime">${preguntaElegida.getFechaCreacionPreguntaToString()}</span></div>
                    
                    </div> 
                </div>
            </div>
            <div class="answer-container_qa">
                <div class="user-container_answer">
                    <div class="img-container_user">
                    
                        <img id="img_container" src="GeneralServlet?Imagen=Usuario&Id=${usuarioElegido.getIdUsuario()}" alt="imagen-usuario">
                        
                    </div>
                    <div class="username_user">${usuarioElegido.getUsernameUsuario()}</div>
                </div>
                <form class="form-container_answer" id="form-container_answer" method = "post" action = "SubirRespuestas" enctype="multipart/form-data"> 
                
                    <c:if test="${empty respuestaElegida}">
                    
                    
	                    <div class="text-container-form">
	                        <textarea name="answer" id="textarea_form" class="textarea_form" placeholder="Responde aqui!" required></textarea>
	                    </div>
	                    <div class="tools-container_form">
	                        <div class="upload-container">
	                            <label for="upload" class="upload-label"><i class="fas fa-file-image"></i> Subir imagen</label>
	                            <input type="file" name="upload" id="upload" class="upload ignore"/>
	                        </div>
	                        <div class="answer_tools">
	                            <button type="submit" id="button_answer" class="button_answer">Responder</button>
	                        </div>
	                    </div>
	                    
	                    <input name="respuestaNueva" type="hidden" value="true">
						<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
						
						
					</c:if>
					
					 <c:if test="${not empty respuestaElegida}">
					 
					 
					 	<div class="text-container-form">
	                        <textarea name="answer" id="textarea_form" class="textarea_form" placeholder="Responde aqui!" required>${respuestaElegida.getTextoRespuesta()}</textarea>	
	                    </div>
	                    <c:if test="${respuestaElegida.isImagenRespuesta() != null}">
							<div class="img-container" id="img-container">
								<img src="GeneralServlet?Imagen=Respuesta&Id=${respuestaElegida.getIdRespuesta()}" alt="imagen" id="img" class="img" style="width: 450px; height: 250px; margin-top: 10px;">
							</div>
						</c:if>
						<c:if test="${respuestaElegida.isImagenRespuesta() == null}">
							<div class="img-container" id="img-container">
								<img src="" alt="imagen" id="img" class="img" style="width: 450px; height: 250px; margin-top: 10px; display: none;">
							</div>
						</c:if>
	                    <div class="tools-container_form">
	                        <div class="upload-container">
	                            <label for="upload" id="upload-image" class="upload-label"><i class="fas fa-file-image"></i> Subir imagen</label>
<!-- 	                            TODO: AQUI COMO PODEMOS HACER PARA ACTUALIZAR LA IMAGEN? O QUE SIMPLEMENTE DESAPAREZCA? EN ESTE MOMENTO DESAPARECE       -->
	                            <input type="file" name="upload" id="upload" class="upload ignore" value = "${respuestaElegida.getImagenRespuesta()}" />
	                            <button type="button" class="btn_delete-image" id="btn_delete-image"><i class="fas fa-trash-alt"></i> Borrar imagen</button>
	                        </div>
	                        <div class="answer_tools">
	                            <button type="submit" id="button_answer" class="button_answer">Responder</button>
	                        </div>
	                    </div>
	                    
	                    <input name="respuestaNueva" type="hidden" value="false">
						<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
					 	<input name="IdRespuesta" type="hidden" value="${respuestaElegida.getIdRespuesta()}">
					 	<input name="eliminarImagen" type="hidden" id="delete-image" value="0">
					 
					 </c:if>
					
                </form>
            </div>
        </div>
    </main>
    <footer>
<!--         <div class="footer-container">Hecho por Derek Figon y Luis Daniel</div>   -->
			<jsp:include page="footer.jsp"></jsp:include>
    </footer>
</body>
</html>