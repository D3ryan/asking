<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>

<%
Usuarios_Model usuarioElegido = (Usuarios_Model)request.getAttribute("IdUsuarioActivo");
pageContext.setAttribute("usuarioElegido", usuarioElegido);


Preguntas_Model preguntaElegida = (Preguntas_Model) request.getAttribute("preguntaElegida");
pageContext.setAttribute("preguntaElegida", preguntaElegida);


List<Categorias_Model> listaCategorias = (List<Categorias_Model>) request.getAttribute("listaCategorias");
pageContext.setAttribute("listaCategorias", listaCategorias);
%>
    
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/question.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/validation.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <script src="js/jquery.js"></script> 
	<script src ="js/jquery.validate.min.js"></script>
	<script src ="js/mainjquery.js"></script>
    <title>Asking</title>
</head>
<body>
    <header>
        <div id="navbarPage"></div>
    </header>
    <main>
        <div class="main-container">
            <div class="question-container_main">
            
                <div class="user-container_question">
                    <div class="img-container_user">
                        <img id="img_container" src="GeneralServlet?Imagen=Usuario&Id=${usuarioElegido.getIdUsuario()}" alt="imagen-usuario">
                    </div>
                    <div class="username_user">${usuarioElegido.getUsernameUsuario()}</div>
                </div>
                
                <form class="form-container_question" id="form-container_question" method = "post" action = "SubirPreguntas" enctype="multipart/form-data"> 
                    
                    <c:if test="${empty preguntaElegida}">

						<div class="category-container_form">
							<select class="category" name="category" id="category">
								<c:forEach var="iCategoria" items="${listaCategorias}">
								
									<option value="${iCategoria.getIdCategoria()}">${iCategoria.getTituloCategoria()}</option>
									
								</c:forEach>

							</select>

						</div>



						<div class="text-container-form">
							<input type="text" name="question" class="title_form"
								id="title_form" placeholder="Que deseas preguntar?" required>
							<textarea id="textarea_form" name="description"
								class="textarea_form" placeholder="Describe tu pregunta!"></textarea>
						</div>
						<div class="tools-container_form">
							<div class="upload-container">
								<label for="upload" class="upload-label"><i
									class="fas fa-file-image"></i> Subir imagen</label> <input type="file"
									name="upload" id="upload" class="upload ignore" />
							</div>
							<div class="ask_tools">
								<button type="submit" id="button_ask" class="button_ask"
									form="form-container_question">Preguntar</button>
							</div>
						</div>

						<input name="preguntaNueva" type="hidden" value="true">

					</c:if>

					<c:if test="${not empty preguntaElegida}">

						<div class="category-container_form">
                    
                        	<select class="category" name="category" id="category">

								<c:forEach var="iCategoria" items="${listaCategorias}">

									<option value="${iCategoria.getIdCategoria()}" 
										
										<c:if test="${iCategoria.getIdCategoria() eq preguntaElegida.getCategoria_Pregunta()}">
											selected="selected"
										</c:if>
									
									> 
																			
									${iCategoria.getTituloCategoria()}</option>
								
								</c:forEach>

                        	</select>
                        
                    	</div>


						<div class="text-container-form">
							<input type="text" name="question" class="title_form"
								id="title_form" placeholder="Que deseas preguntar?" value = "${preguntaElegida.getTituloPregunta()}" required>
							<textarea id="textarea_form" name="description"
								class="textarea_form" placeholder="Describe tu pregunta!">${preguntaElegida.getDescripcionPregunta()}</textarea>
						 	<c:if test="${preguntaElegida.isImagenPregunta() != null}">
								<div class="img-container" id="img-container">
									<img src="GeneralServlet?Imagen=Pregunta&Id=${preguntaElegida.getIdPregunta()}" alt="imagen" id="img" class="img" style="width: 450px; height: 250px; margin-top: 10px;">
								</div>
								</c:if>
							<c:if test="${preguntaElegida.isImagenPregunta() == null}">
								<div class="img-container" id="img-container">
									<img src="" alt="imagen" id="img" class="img" style="width: 450px; height: 250px; margin-top: 10px; display: none;">
								</div>
							</c:if>
						</div>
						<div class="tools-container_form">
							<div class="upload-container">
								<label for="upload" id="upload-image" class="upload-label">
									<i class="fas fa-file-image"></i> Subir imagen
								</label> 
<!-- 								TODO: AQUI COMO PODEMOS HACER PARA ACTUALIZAR LA IMAGEN? O QUE SIMPLEMENTE DESAPAREZCA? EN ESTE MOMENTO DESAPARECE-->
								<input type="file" name="upload" id="upload" class="upload ignore" value = "${preguntaElegida.getImagenPregunta()}" />
								<button type="button" class="btn_delete-image" id="btn_delete-image"><i class="fas fa-trash-alt"></i> Borrar imagen</button>
							</div>
							<div class="ask_tools">
								<button type="submit" id="button_ask" class="button_ask"
									form="form-container_question">Preguntar</button>
							</div>
						</div>
						
						<input name="preguntaNueva" type="hidden" value="false">
						<input name="IdPregunta" type="hidden" value="${preguntaElegida.getIdPregunta()}">
						<input name="eliminarImagen" type="hidden" id="delete-image" value="0">
						
					</c:if>

					
                    
                     
                </form>
            </div>
        </div>
    </main>
    <footer>
<!--         <div class="footer-container">Hecho por Derek Figon y Luis Daniel</div>   -->
			<jsp:include page="footer.jsp"></jsp:include>
    </footer>
</body>
</html>