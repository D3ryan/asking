<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    
<%

String usernameEncontrado = "false";
if(request.getAttribute("usernameEncontrado") != null)
	usernameEncontrado = request.getAttribute("usernameEncontrado").toString();
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/navbar.css">
<link rel="stylesheet" type="text/css" href="css/signup.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="stylesheet" type="text/css" href="css/validation.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
<script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
	
<script src="js/jquery.js"></script> 
<script src ="js/jquery.validate.min.js"></script>
<script src ="js/mainjquery.js"></script>
<title>Asking</title>
</head>
<body>
	<header>
		<div id="navbarPage"></div>
	</header>
	<main>
		<div class="left-side_main">
			<div class="message_left-side">
				<a href="#" id="asking_left-side">Asking?</a>
				<div id="subtitle_left-side">¿Sé el rey de las preguntas!</div>
			</div>
		</div>
		<div class="right-side_main">
			<div class="main-container_right-side">
			
				<form class="form_main-container" id="form_main-container" method = "post" action = "SingUpUsuarios" enctype="multipart/form-data">
			
					<div class="user_main-container" id="user_main-container">
						<img id="img_user" src="img/user-img.png" alt="imagen-usuario">
						<div class="overlay_user">
							<label for="imagen" id="text_overlay">Subir imagen</label>
            				<input type="file" name="imagen" id="imagen" class="imagen" required/>
						</div>
					</div>
					<div class="input-container_form">
						<div class="left-column_form">
							<div class="name-container">
								<label for="name">Nombres:</label> 
								<input type="text" name="name" required> 
							</div>
							<div class="lastnameP-container">
								<label for="lastname">Apellido Paterno:</label> 
								<input type="text" name="lastnameP" required>
							</div>
							<div class="lastnameM-container">
								<label for="lastname">Apellido Materno:</label> 
								<input type="text" name="lastnameM">  
							</div>
							<div class="birthday-container">
								<label for="birthday">Fecha de nacimiento:</label> 
								<input type="date" name="birthday" required>
							</div>
						</div>
						<div class="right-column_form">
							<div class="email-container">
								<label for="email">Email</label> 
								<input type="email" name="email" required> 
							</div>
							<div class="user-container">
								<label for="username">Nombre de usuario</label>
								<input id="username" type="text" name="username" required> 
							</div>
							<div class="pass-container">
								<label for="password">Contraseña:</label> 
								<input id="password" type="password" name="password" required>
							</div>
							<div class="passconfirm-container">
								<label for="passconfirm">Confirmar Contraseña:</label> 
								<input type="password" name="passconfirm" required>
							</div>
						</div>
					</div>
					<input id="usernameEncontrado" type="hidden" name="usernameEncontrado" value="<%=usernameEncontrado%>">
				</form>
				<button id="button-signup_main-container" form="form_main-container">Ingresar</button>

				
			</div>
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>