<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.AskingProject.models.*"%>
<%@page import="java.util.*"%>

<%
long IdUsuarioActivo = (long)request.getAttribute("IdUsuarioActivo");
pageContext.setAttribute("IdUsuarioActivo", IdUsuarioActivo);

List<Preguntas_Model> lista10Preguntas = (List<Preguntas_Model>) request.getAttribute("lista10Preguntas");
pageContext.setAttribute("lista10Preguntas", lista10Preguntas);

List<Categorias_Model> listaCategorias = (List<Categorias_Model>) request.getAttribute("listaCategorias");
pageContext.setAttribute("listaCategorias", listaCategorias);

String Busqueda = (String) request.getAttribute("Busqueda");
pageContext.setAttribute("Busqueda", Busqueda);

int numeroPagina = 1;
if (request.getAttribute("numeroPagina") != null){
	numeroPagina = (int)request.getAttribute("numeroPagina");
	pageContext.setAttribute("numeroPagina", numeroPagina);
}

byte EstadoUsuario = 0;
if (request.getAttribute("EstadoUsuario") != null){
	EstadoUsuario = (byte)request.getAttribute("EstadoUsuario");
	pageContext.setAttribute("EstadoUsuario", EstadoUsuario);
}

Preguntas_Model parametrosPregunta = (Preguntas_Model) request.getAttribute("parametrosPregunta");
pageContext.setAttribute("parametrosPregunta", parametrosPregunta);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/navbar.css">
<link rel="stylesheet" type="text/css" href="css/search.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link
	href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,200;0,300;0,400;0,500;1,200;1,300;1,400;1,500&display=swap"
	rel="stylesheet">
<script src="https://kit.fontawesome.com/ea1f7a49e6.js"
	crossorigin="anonymous"></script>
	
<script src="js/jquery.js"></script> 
<script src ="js/mainjquery.js"></script>

<title>Asking</title>
</head>
<body>
	<header>
		<div id="navbarPage"></div>
	</header>
	<main>
		<div class="left-side">
			<div class="search-container_left">
				<form class="form_search-container" action="SearchPreguntas">
					<input name="Busqueda" type="hidden" value="Preguntas">
					<input name="numeroPagina" type="hidden" value="1">
				
					<div class="search-bar_search-container">
						<label class="label_search-bar" for="text">¿Qué pregunta deseas buscar?</label> 
						<input class="input_search-bar" type="text" name = "question" value = "${parametrosPregunta.getTituloPregunta()}">
					</div>
										
					<div class="categories_search-container">
						<label class="label_categories" for="categories">Categoría:</label>
						<select class="dropdown_categories" name="categories" id="categories">
							<option value="-1" 
								<c:if test="${parametrosPregunta.getCategoria_Pregunta() == -1}">
									selected="selected"
								</c:if>
							>Todas</option>
							<c:forEach var="iCategoria" items="${listaCategorias}">
								<option value="${iCategoria.getIdCategoria()}"
									<c:if test="${iCategoria.getIdCategoria() eq parametrosPregunta.getCategoria_Pregunta()}">
										selected="selected"
									</c:if>
								>${iCategoria.getTituloCategoria()}</option>
							</c:forEach>
						
						</select>
					</div>
					<div class="date-range_search-container">
						<div class="from-container_date-range">
							<label class="fromlabel_from" for="date">Desde:</label> 
<!-- 							TODO: HAY UN PROBLEMA POR HABER CAMBIADO LOS IDS DE LAS FECHAS( ANTES ERAN EL MISMO)? -->
							<input class="frominput_from" type="date" name="dateFrom" id="dateFrom" value = "${parametrosPregunta.getPreguntaFechaDesdeString()}">
						</div>
						<div class="to-container_date-range">
							<label class="tolabel_to" for="date">Hasta:</label> 
							<input class="toinput_to" type="date" name="dateTo" id="dateTo" value = "${parametrosPregunta.getPreguntaFechaHastaString()}">
						</div>
					</div>
					<div class="useful_search-container">
						<label for="useful">Min. <i class="fas fa-thumbs-up"></i></label>
						<input class="input_useful" type="number" name="useful" value = "${parametrosPregunta.getCantidadVotosUtilesString()}">
					</div>
					<div class="fav_search-container">
						<label for="fav">Min. <i class="fas fa-star"></i></label> 
						<input class="input_fav" type="number" name="fav"  value = "${parametrosPregunta.getCantidadVotosFavoritosString()}">
					</div>
					
					<button class="button_form">Buscar</button>
				</form>
			</div>
		</div>
		<div class="right-side">
			<div class="qa-container">
			
				<c:if test="${IdUsuarioActivo != -1 && EstadoUsuario != 0}">		
					<div class="askbar_qa-container">
						<label class="label_askbar">¿Deseas preguntar algo?</label> 
						<a href="SubirPreguntas"><button class="button_askbar">¡Pregunta YA!</button></a>
					</div>
				</c:if>
				
				<c:forEach var="iPregunta" items="${lista10Preguntas}">

					<c:set var="iPregunta" value="${iPregunta}" scope="request"/>
					<jsp:include page="questionView.jsp"></jsp:include>

				</c:forEach>


				<c:if test="${not empty Busqueda}">
					<div class="button-container_qa-container">
						<c:if test="${numeroPagina - 1 == 0}">
							<a href="SearchPreguntas?
									Busqueda=Preguntas&
									numeroPagina=1&
									question=${parametrosPregunta.getTituloPregunta()}&
									categories=${parametrosPregunta.getCategoria_Pregunta()}&
									dateFrom=${parametrosPregunta.getPreguntaFechaDesdeString()}&
									dateTo=${parametrosPregunta.getPreguntaFechaHastaString()}&
									useful=${parametrosPregunta.getCantidadVotosUtiles()}&
									fav=${parametrosPregunta.getCantidadVotosFavoritos()}">
								<button class="btn_button-container">
									<i class="fas fa-angle-left"></i>
								</button>

							</a>
						</c:if>

						<c:if test="${numeroPagina - 1 != 0}">
							<a href="SearchPreguntas?
									Busqueda=Preguntas&
									numeroPagina=${numeroPagina - 1}&
									question=${parametrosPregunta.getTituloPregunta()}&
									categories=${parametrosPregunta.getCategoria_Pregunta()}&
									dateFrom=${parametrosPregunta.getPreguntaFechaDesdeString()}&
									dateTo=${parametrosPregunta.getPreguntaFechaHastaString()}&
									useful=${parametrosPregunta.getCantidadVotosUtiles()}&
									fav=${parametrosPregunta.getCantidadVotosFavoritos()}">
								<button class="btn_button-container">
									<i class="fas fa-angle-left"></i>
								</button>

							</a>
						</c:if>

						<div class="counter_button-container">
							${numeroPagina}
						</div>

						<a href="SearchPreguntas?
								Busqueda=Preguntas&
								numeroPagina=${numeroPagina + 1}&
								question=${parametrosPregunta.getTituloPregunta()}&
								categories=${parametrosPregunta.getCategoria_Pregunta()}&
								dateFrom=${parametrosPregunta.getPreguntaFechaDesdeString()}&
								dateTo=${parametrosPregunta.getPreguntaFechaHastaString()}&
								useful=${parametrosPregunta.getCantidadVotosUtiles()}&
								fav=${parametrosPregunta.getCantidadVotosFavoritos()}">
							<button class="btn_button-container">
								<i class="fas fa-angle-right"></i>
							</button>
						</a>
					</div>
				</c:if>
			</div>
		</div>
	</main>
	<footer>
<!-- 		<div class="footer-container">Hecho por Derek Figon y Luis Daniel</div> -->
			<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>