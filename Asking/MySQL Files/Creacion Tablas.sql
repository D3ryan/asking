Create database asking_sql;
USE asking_sql;

CREATE TABLE `usuarios` (
  `IdUsuario` bigint NOT NULL AUTO_INCREMENT,
  `NombreUsuario` varchar(30) NOT NULL,
  `ApellidoPaternoUsuario` varchar(30) NOT NULL,
  `ApellidoMaternoUsuario` varchar(30) NOT NULL,
  `FechaNacimientoUsuario` date NOT NULL,
  `CorreoUsuario` varchar(60) NOT NULL,
  `ImagenPerfilUsuario` mediumblob NOT NULL,
  `UsernameUsuario` varchar(30) NOT NULL,
  `PasswordUsuario` varchar(30) NOT NULL,
  `FechaCreacionUsuario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EstadoUsuario` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IdUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

  

  CREATE TABLE `preguntas` (
  `IdPregunta` bigint NOT NULL AUTO_INCREMENT,
  `TituloPregunta` varchar(100) NOT NULL,
  `DescripcionPregunta` varchar(800) DEFAULT NULL,
  `ImagenPregunta` mediumblob NULL,
  `FechaCreacionPregunta` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EstadoPregunta` tinyint(1) NOT NULL DEFAULT '1',
  `EditadoPregunta` tinyint(1) NOT NULL DEFAULT '0',
  `Usuario_Pregunta` bigint NOT NULL,
  `Respuesta_Correcta` bigint DEFAULT NULL,
  `Categoria_Pregunta` int DEFAULT NULL,
  PRIMARY KEY (`IdPregunta`),
  UNIQUE KEY `IdPregunta_UNIQUE` (`IdPregunta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

  
  

  CREATE TABLE `respuestas` (
  `IdRespuesta` bigint NOT NULL AUTO_INCREMENT,
  `TextoRespuesta` varchar(800) NOT NULL,
  `ImagenRespuesta` mediumblob NULL,
  `FechaCreacionRespuesta` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EstadoRespuesta` tinyint(1) NOT NULL DEFAULT '1',
  `EditadoRespuesta` tinyint(1) NOT NULL DEFAULT '0',
  `Usuario_Responde` bigint NOT NULL,
  `Pregunta_Respondida` bigint NOT NULL,
  PRIMARY KEY (`IdRespuesta`),
  UNIQUE KEY `IdRespuesta_UNIQUE` (`IdRespuesta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

  
  
  
CREATE TABLE `categorias` (
  `IdCategoria` int NOT NULL AUTO_INCREMENT,
  `TituloCategoria` varchar(40) NOT NULL,
  PRIMARY KEY (`IdCategoria`),
  UNIQUE KEY `IdCategoria_UNIQUE` (`IdCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



CREATE TABLE `usuarios_calf_fav_pregunta` (
  `IdCalificacion` bigint NOT NULL AUTO_INCREMENT,
  `UsuarioCalifica` bigint NOT NULL,
  `PreguntaCalificada` bigint NOT NULL,
  PRIMARY KEY (`IdCalificacion`),
  UNIQUE KEY `IdCalificacion_UNIQUE` (`IdCalificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

  
  CREATE TABLE `usuarios_calf_pregunta` (
  `IdCalificacion` int NOT NULL AUTO_INCREMENT,
  `UsuarioCalifica` bigint NOT NULL,
  `PreguntaCalificada` bigint NOT NULL,
  `UtilidadPreguntaCalificada` tinyint(1) NOT NULL,
  PRIMARY KEY (`IdCalificacion`),
  UNIQUE KEY `IdCalificacion_UNIQUE` (`IdCalificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

  
  CREATE TABLE `usuarios_calf_respuesta` (
  `IdCalificacion` int NOT NULL AUTO_INCREMENT,
  `UsuarioCalifica` bigint NOT NULL,
  `RespuestaCalificada` bigint NOT NULL,
  `UtilidadRespuestaCalificada` tinyint(1) NOT NULL,
  PRIMARY KEY (`IdCalificacion`),
  UNIQUE KEY `IdCalificacion_UNIQUE` (`IdCalificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

  
  
  
  
  
  
  
  
ALTER TABLE `preguntas` 
ADD INDEX `FK_PREG_USER_idx` (`Usuario_Pregunta` ASC) VISIBLE,
ADD INDEX `FK_PREG_RESP_idx` (`Respuesta_Correcta` ASC) VISIBLE,
ADD INDEX `FK_PREG_CATE_idx` (`Categoria_Pregunta` ASC) VISIBLE;
;
ALTER TABLE `preguntas` 
ADD CONSTRAINT `FK_PREG_USER`
  FOREIGN KEY (`Usuario_Pregunta`)
  REFERENCES `usuarios` (`IdUsuario`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_PREG_RESP`
  FOREIGN KEY (`Respuesta_Correcta`)
  REFERENCES `respuestas` (`IdRespuesta`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_PREG_CATE`
  FOREIGN KEY (`Categoria_Pregunta`)
  REFERENCES `categorias` (`IdCategoria`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;










ALTER TABLE `respuestas` 
ADD INDEX `FK_RESP_USER_idx` (`Usuario_Responde` ASC) VISIBLE,
ADD INDEX `FK_RESP_PREG_idx` (`Pregunta_Respondida` ASC) VISIBLE;
;
ALTER TABLE `respuestas` 
ADD CONSTRAINT `FK_RESP_USER`
  FOREIGN KEY (`Usuario_Responde`)
  REFERENCES `usuarios` (`IdUsuario`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_RESP_PREG`
  FOREIGN KEY (`Pregunta_Respondida`)
  REFERENCES `preguntas` (`IdPregunta`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;






ALTER TABLE `usuarios_calf_fav_pregunta` 
ADD INDEX `FK_CALF_FAV_USER_idx` (`UsuarioCalifica` ASC) VISIBLE,
ADD INDEX `FK_CALF_FAV_PREG_idx` (`PreguntaCalificada` ASC) VISIBLE;
;
ALTER TABLE `usuarios_calf_fav_pregunta` 
ADD CONSTRAINT `FK_CALF_FAV_USER`
  FOREIGN KEY (`UsuarioCalifica`)
  REFERENCES `usuarios` (`IdUsuario`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_CALF_FAV_PREG`
  FOREIGN KEY (`PreguntaCalificada`)
  REFERENCES `preguntas` (`IdPregunta`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;









ALTER TABLE `usuarios_calf_pregunta` 
ADD INDEX `FK_CALF_PR_USER_idx` (`UsuarioCalifica` ASC) VISIBLE,
ADD INDEX `FK_CALF_PR_PREG_idx` (`PreguntaCalificada` ASC) VISIBLE;
;
ALTER TABLE `usuarios_calf_pregunta` 
ADD CONSTRAINT `FK_CALF_PR_USER`
  FOREIGN KEY (`UsuarioCalifica`)
  REFERENCES `usuarios` (`IdUsuario`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_CALF_PR_PREG`
  FOREIGN KEY (`PreguntaCalificada`)
  REFERENCES `preguntas` (`IdPregunta`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;






ALTER TABLE `usuarios_calf_respuesta` 
ADD INDEX `FK_CALF_RE_USER_idx` (`UsuarioCalifica` ASC) VISIBLE,
ADD INDEX `FK_CALF_RE_RESP_idx` (`RespuestaCalificada` ASC) VISIBLE;
;
ALTER TABLE `usuarios_calf_respuesta` 
ADD CONSTRAINT `FK_CALF_RE_USER`
  FOREIGN KEY (`UsuarioCalifica`)
  REFERENCES `usuarios` (`IdUsuario`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_CALF_RE_RESP`
  FOREIGN KEY (`RespuestaCalificada`)
  REFERENCES `respuestas` (`IdRespuesta`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


INSERT INTO categorias(TituloCategoria) VALUES('Finanzas');
INSERT INTO categorias(TituloCategoria) VALUES('Cocina');
INSERT INTO categorias(TituloCategoria) VALUES('Educación');
INSERT INTO categorias(TituloCategoria) VALUES('Deportes');
INSERT INTO categorias(TituloCategoria) VALUES('Videojuegos');

set global max_allowed_packet = 1000 * 1000 * 25; # set size to 10M  

show VARIABLES like '%max_allowed_packet%';
