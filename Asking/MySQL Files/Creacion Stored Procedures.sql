USE `asking_sql`;

DROP procedure IF EXISTS `sp_Usuarios`;
DELIMITER $$
CREATE DEFINER=`admin`@`%`PROCEDURE `sp_Usuarios` (
	IN pOpc							char(3),
	IN pIdUsuario 					bigint,
	IN pNombreUsuario 				varchar(30),
	IN pApellidoPaternoUsuario 		varchar(30),
	IN pApellidoMaternoUsuario 		varchar(30),
	IN pFechaNacimientoUsuario 		date,
	IN pCorreoUsuario 				varchar(60),
	IN pImagenPerfilUsuario 		mediumblob,
	IN pUsernameUsuario 			varchar(30),
	IN pPasswordUsuario 			varchar(30),
	IN pFechaCreacionUsuario 		date,
	IN pEstadoUsuario 				tinyint(1) 
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO usuarios(	NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario, FechaNacimientoUsuario,
								CorreoUsuario, ImagenPerfilUsuario, UsernameUsuario, PasswordUsuario)
			VALUES(	pNombreUsuario, pApellidoPaternoUsuario, pApellidoMaternoUsuario, pFechaNacimientoUsuario, pCorreoUsuario, pImagenPerfilUsuario, pUsernameUsuario, pPasswordUsuario);
	END IF;
	IF pOpc = 'U'
	THEN
		UPDATE usuarios
			SET NombreUsuario = pNombreUsuario,
				ApellidoPaternoUsuario = pApellidoPaternoUsuario,				
                ApellidoMaternoUsuario = pApellidoMaternoUsuario,
				FechaNacimientoUsuario = pFechaNacimientoUsuario,
                CorreoUsuario = pCorreoUsuario,
				ImagenPerfilUsuario = IF(pImagenPerfilUsuario IS NULL, ImagenPerfilUsuario, pImagenPerfilUsuario),
                UsernameUsuario = pUsernameUsuario,
				PasswordUsuario = pPasswordUsuario
			WHERE IdUsuario = pIdUsuario;
            
	END IF;

#TODO: PENDIENTE CREAR UN INSTEAD OF DELETE DONDE HAGAMOS UNA BAJA LOGICA y cambiar  EstadoPregunta = 0
	IF pOpc = 'D'
	THEN
		DELETE
			FROM usuarios
		WHERE IdUsuario = pIdUsuario;
	END IF;
    
    IF pOpc = 'SUS'
	THEN
		UPDATE usuarios
			SET 
				EstadoUsuario = 0
			WHERE IdUsuario = pIdUsuario;
	END IF;

	IF pOpc = 'S'
	THEN
		SELECT IdUsuario, NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario, 
				FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, 
                UsernameUsuario, PasswordUsuario, FechaCreacionUsuario, EstadoUsuario
			FROM usuarios
		WHERE IdUsuario = pIdUsuario;
	END IF;

	IF pOpc = 'L'
	THEN
		SELECT IdUsuario, NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario, 
				FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, 
                UsernameUsuario, PasswordUsuario, FechaCreacionUsuario, EstadoUsuario
			FROM usuarios
		WHERE BINARY  UsernameUsuario = BINARY pUsernameUsuario
        AND BINARY PasswordUsuario = BINARY pPasswordUsuario;
	END IF;
    
    IF pOpc = 'UsP'
	THEN
		SELECT U.IdUsuario, U.NombreUsuario, U.ApellidoPaternoUsuario, U.ApellidoMaternoUsuario, 
				U.FechaNacimientoUsuario, U.CorreoUsuario, U.ImagenPerfilUsuario, 
                U.UsernameUsuario, U.PasswordUsuario, U.FechaCreacionUsuario, U.EstadoUsuario,
				COUNT(distinct P.IdPregunta) AS "CantPreguntasUsuario", COUNT(distinct R.IdRespuesta) AS "CantRespuestasUsuario",
                COUNT(distinct UCFP.IdCalificacion) AS "CantPreguntasFavoritasUsuario", 
                COUNT(distinct UCPU.IdCalificacion) AS "CantPreguntasUtilesUsuario", COUNT(distinct UCPNU.IdCalificacion) AS "CantPreguntasNoUtilesUsuario"
                #(SELECT COUNT(*) FROM preguntas AS P WHERE P.Usuario_Pregunta = U.IdUsuario) AS "PREGUNTAS_1", (SELECT COUNT(*) FROM preguntas  AS PR WHERE PR.Usuario_Pregunta = U.IdUsuario)  AS "PREGUNTAS_2"
			FROM usuarios AS U
		LEFT JOIN preguntas AS P
        ON P.Usuario_Pregunta = U.IdUsuario AND P.EstadoPregunta = 1
        LEFT JOIN respuestas AS R
        ON R.Usuario_Responde = U.IdUsuario  AND R.EstadoRespuesta = 1
        
        INNER JOIN preguntas AS PC
        ON PC.EstadoPregunta = 1
        
        LEFT JOIN usuarios_calf_fav_pregunta AS UCFP
        ON PC.IdPregunta = UCFP.PreguntaCalificada AND UCFP.UsuarioCalifica = U.IdUsuario
        
		LEFT JOIN usuarios_calf_pregunta AS UCPU
        ON PC.IdPregunta = UCPU.PreguntaCalificada AND UCPU.UsuarioCalifica = U.IdUsuario AND UCPU.UtilidadPreguntaCalificada = 1
        
		LEFT JOIN usuarios_calf_pregunta AS UCPNU
        ON PC.IdPregunta = UCPNU.PreguntaCalificada AND UCPNU.UsuarioCalifica = U.IdUsuario AND UCPNU.UtilidadPreguntaCalificada = 0
        
        WHERE IdUsuario = pIdUsuario
        GROUP BY U.IdUsuario;
	END IF;


	IF pOpc = 'X'
	THEN
		SELECT IdUsuario, NombreUsuario, ApellidoPaternoUsuario, ApellidoMaternoUsuario, 
				FechaNacimientoUsuario, CorreoUsuario, ImagenPerfilUsuario, 
                UsernameUsuario, PasswordUsuario, FechaCreacionUsuario, EstadoUsuario
			FROM usuarios;
		
	END IF;
    
    IF pOpc = 'V'
    THEN
		SELECT 1 'UsernameEncontrado'
        FROM usuarios
        WHERE UsernameUsuario = pUsernameUsuario;
    END IF;
END$$

DELIMITER ;


DROP procedure IF EXISTS `sp_Preguntas`;
DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `sp_Preguntas`(
	IN pOpc						char(3),
	IN pIdPregunta 				bigint,
	IN pTituloPregunta	 		varchar(100),
	IN pDescripcionPregunta 	varchar(800),
	IN pImagenPregunta 			mediumblob,
	IN pFechaCreacionPregunta 	datetime(6),
	IN pEstadoPregunta 			tinyint(1),
	IN pEditadoPregunta 		tinyint(1),
	IN pUsuario_Pregunta 		bigint,
	IN pRespuesta_Correcta 		bigint,
	IN pCategoria_Pregunta 		int,
    IN pNumeroPreguntaPagina	int,
    IN pPreguntaFechaDesde		datetime(6),
    IN pPreguntaFechaHasta		datetime(6),
    IN pCantidadVotosUtiles		long,
    IN pCantidadVotosFavoritos	long
)
BEGIN
IF pOpc = 'I'
	THEN 
		INSERT INTO preguntas(TituloPregunta, DescripcionPregunta, ImagenPregunta, Usuario_Pregunta, Categoria_Pregunta)
					VALUES(	pTituloPregunta, pDescripcionPregunta, pImagenPregunta, pUsuario_Pregunta, pCategoria_Pregunta);
		
        
	END IF;

	IF pOpc = 'U'
	THEN
		UPDATE preguntas
			SET TituloPregunta = pTituloPregunta,
				DescripcionPregunta = pDescripcionPregunta,				
                ImagenPregunta = IF(pImagenPregunta IS NULL, ImagenPregunta, pImagenPregunta),
				EditadoPregunta = 1,
                Categoria_Pregunta = pCategoria_Pregunta				
			WHERE IdPregunta = pIdPregunta;
	END IF;
    
    IF pOpc = 'UE'
	THEN
		UPDATE preguntas
			SET TituloPregunta = pTituloPregunta,
				DescripcionPregunta = pDescripcionPregunta,				
                ImagenPregunta = NULL,
				EditadoPregunta = 1,
                Categoria_Pregunta = pCategoria_Pregunta				
			WHERE IdPregunta = pIdPregunta;
            
	END IF;

	IF pOpc = 'IRC'
	THEN
		UPDATE preguntas
			SET Respuesta_Correcta = pRespuesta_Correcta				
			WHERE IdPregunta = pIdPregunta;
            
	END IF;
    
    IF pOpc = 'DRC'
	THEN
		UPDATE preguntas
			SET Respuesta_Correcta = NULL				
			WHERE IdPregunta = pIdPregunta;
            
	END IF;

	#TODO: PENDIENTECon un instead of Delete marcar la baja logica y cambiar  EstadoPregunta = 0
	IF pOpc = 'D'
	THEN
		DELETE
			FROM preguntas
		WHERE IdPregunta = pIdPregunta;
	END IF;

	IF pOpc = 'S'
	THEN
		SELECT IdPregunta, TituloPregunta, DescripcionPregunta, ImagenPregunta, FechaCreacionPregunta,EstadoPregunta, EditadoPregunta, Usuario_Pregunta, Respuesta_Correcta, Categoria_Pregunta
			FROM preguntas
		WHERE IdPregunta = pIdPregunta;
	END IF;

	IF pOpc = 'N'
	THEN
		#TODO: ES NECESARIO RECOPILAR TODAS LAS COLUMNAS SI SOLO NECESITO ID PREGUNTA? 
		SELECT 	P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario, C.TituloCategoria
			FROM preguntas as P
		INNER JOIN categorias AS C
        ON C.IdCategoria = P.Categoria_Pregunta
		INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
		WHERE U.IdUsuario = pUsuario_Pregunta
        ORDER  BY FechaCreacionPregunta DESC
        LIMIT 1;
	END IF;

        
        
        
        

	IF pOpc = 'B'
	THEN
		#TODO: FALTA TESTEAR QUE LOS VOTOS UTILES, NO UTILES Y FAVS SE VEAN BIEN
        #TAMBIÉN HAY QUE ESPECIFICAR QUE NO TRAIGA LA INFO SI ESTA ELIMINADA LA PREGUNTA? O ESO NO ES NECESARIO?
		SELECT 	P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario, U.EstadoUsuario, C.TituloCategoria,
                COUNT(distinct UCPU.IdCalificacion) AS "CantVotosUtiles",
                COUNT(distinct UCPNU.IdCalificacion) AS "CantVotosNoUtiles",
				COUNT(distinct UCFP.IdCalificacion) AS "CantVotosFavoritas"
                
			FROM preguntas as P
		INNER JOIN categorias AS C
        ON C.IdCategoria = P.Categoria_Pregunta
		INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
        
        LEFT JOIN usuarios_calf_pregunta AS UCPU
        ON UCPU.PreguntaCalificada = P.IdPregunta AND UCPU.UtilidadPreguntaCalificada = 1
        LEFT JOIN usuarios_calf_pregunta AS UCPNU
        ON UCPNU.PreguntaCalificada = P.IdPregunta AND UCPNU.UtilidadPreguntaCalificada = 0
        LEFT JOIN usuarios_calf_fav_pregunta AS UCFP
        ON UCFP.PreguntaCalificada = P.IdPregunta
        
		WHERE P.IdPregunta = pIdPregunta
        GROUP BY P.IdPregunta
        ;
	END IF;

	IF pOpc = 'BA'
	THEN
		SELECT 	P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario, C.TituloCategoria,
				COUNT(distinct UCPU.IdCalificacion) AS "CantVotosUtiles",
				COUNT(distinct UCFP.IdCalificacion) AS "CantVotosFavoritas"
			FROM preguntas as P
		INNER JOIN categorias AS C
        ON C.IdCategoria = P.Categoria_Pregunta
		INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
        
        
        LEFT JOIN usuarios_calf_pregunta AS UCPU
        ON UCPU.PreguntaCalificada = P.IdPregunta AND UCPU.UtilidadPreguntaCalificada = 1
        LEFT JOIN usuarios_calf_fav_pregunta AS UCFP
        ON UCFP.PreguntaCalificada = P.IdPregunta
        
        WHERE IF(pTituloPregunta IS NULL, 1, P.TituloPregunta LIKE CONCAT('%', pTituloPregunta, '%')	)
        AND   IF(IFNULL(pCategoria_Pregunta , -1) = -1, 1, P.Categoria_Pregunta = pCategoria_Pregunta )
        AND   IF(pPreguntaFechaDesde is null, 1, P.FechaCreacionPregunta >= pPreguntaFechaDesde) 
        AND   IF(pPreguntaFechaHasta is null, 1, P.FechaCreacionPregunta <=  DATE_ADD(pPreguntaFechaHasta,INTERVAL 1 DAY))
        AND   P.EstadoPregunta = 1
        
        GROUP BY P.IdPregunta
		HAVING IF(pCantidadVotosUtiles is null, 1, CantVotosUtiles >= pCantidadVotosUtiles)
        AND IF(pCantidadVotosFavoritos is null, 1, CantVotosFavoritas >= pCantidadVotosFavoritos)
        ORDER BY P.FechaCreacionPregunta
        DESC LIMIT pNumeroPreguntaPagina, 10;
	END IF;


	IF pOpc = 'X'
	THEN
		SELECT IdPregunta, TituloPregunta, DescripcionPregunta, ImagenPregunta, FechaCreacionPregunta,EstadoPregunta, EditadoPregunta, Usuario_Pregunta, Respuesta_Correcta, Categoria_Pregunta
			FROM preguntas;
	
	END IF;
    
    IF pOpc = 'P'
    THEN
    #TODO: ES NECESARIO TRAERME TODAS LAS COLUMNAS? SI SOLO OCUPO NOMBRE, USERNAME Y FOTO PERFIL?
		SELECT  P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario
			FROM preguntas as P
		INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
        WHERE   P.EstadoPregunta = 1
        ORDER BY FechaCreacionPregunta 
        DESC LIMIT pNumeroPreguntaPagina, 10;
    END IF;
    
    IF pOpc = 'UP'
    THEN
		SELECT P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario
			FROM preguntas AS P
        INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
        WHERE U.IdUsuario = pUsuario_Pregunta
        AND   P.EstadoPregunta = 1
        ORDER BY P.FechaCreacionPregunta DESC LIMIT pNumeroPreguntaPagina, 10;
    END IF;
    
    IF pOpc = 'UPF'
    THEN
		SELECT  P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario 
			FROM preguntas AS P
		INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
        
		INNER JOIN usuarios_calf_fav_pregunta AS UPF
		ON UPF.PreguntaCalificada = P.IdPregunta
		INNER JOIN usuarios AS UE
        ON UE.IdUsuario = UPF.UsuarioCalifica
        
        WHERE UE.IdUsuario = pUsuario_Pregunta
        AND   P.EstadoPregunta = 1
        ORDER BY P.FechaCreacionPregunta DESC LIMIT pNumeroPreguntaPagina, 10;
    END IF;
    
     IF pOpc = 'UPU'
    THEN
		SELECT  P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario 
			FROM preguntas AS P
		INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
        
		INNER JOIN usuarios_calf_pregunta AS UPU
		ON UPU.PreguntaCalificada = P.IdPregunta
		INNER JOIN usuarios AS UE
        ON UE.IdUsuario = UPU.UsuarioCalifica AND UPU.UtilidadPreguntaCalificada = 1
        
		WHERE UE.IdUsuario = pUsuario_Pregunta
        AND   P.EstadoPregunta = 1
        ORDER BY P.FechaCreacionPregunta DESC LIMIT pNumeroPreguntaPagina, 10;
    END IF;
    
   IF pOpc = 'UPN'
    THEN
		SELECT 	P.IdPregunta, P.TituloPregunta, P.DescripcionPregunta, P.ImagenPregunta, 
				P.FechaCreacionPregunta,P.EstadoPregunta, P.EditadoPregunta, 
                P.Usuario_Pregunta, P.Respuesta_Correcta, P.Categoria_Pregunta,
                U.UsernameUsuario, U.ImagenPerfilUsuario 
			FROM preguntas AS P
            
		INNER JOIN usuarios AS U
        ON U.IdUsuario = P.Usuario_Pregunta
        
		INNER JOIN usuarios_calf_pregunta AS UPU
		ON UPU.PreguntaCalificada = P.IdPregunta
		INNER JOIN usuarios AS UE
        ON UE.IdUsuario = UPU.UsuarioCalifica AND UPU.UtilidadPreguntaCalificada = 0
        
		WHERE UE.IdUsuario = pUsuario_Pregunta
        AND   P.EstadoPregunta = 1
        ORDER BY P.FechaCreacionPregunta DESC LIMIT pNumeroPreguntaPagina, 10;
    END IF;
    
    IF pOpc = 'E'
	THEN
		UPDATE preguntas
			SET 
				EstadoPregunta = 0
			WHERE IdPregunta = pIdPregunta;  
	END IF;
END$$
DELIMITER ;


DROP procedure IF EXISTS `sp_Respuestas`;
DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `sp_Respuestas`(
	IN pOpc						char(3),
	IN pIdRespuesta bigint,
	IN pTextoRespuesta varchar(800),
	IN pImagenRespuesta mediumblob,
	IN pFechaCreacionRespuesta datetime(6),
	IN pEstadoRespuesta tinyint(1),
	IN pEditadoRespuesta tinyint(1),
	IN pUsuario_Responde bigint,
	IN pPregunta_Respondida bigint,
    IN pNumeroRespuestaPagina int
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO respuestas(TextoRespuesta, ImagenRespuesta, Usuario_Responde, Pregunta_Respondida)
			VALUES(	pTextoRespuesta, pImagenRespuesta, pUsuario_Responde ,pPregunta_Respondida);
		
	END IF;

	IF pOpc = 'U'
	THEN
		UPDATE respuestas
			SET TextoRespuesta = pTextoRespuesta,
				ImagenRespuesta = IF(pImagenRespuesta IS NULL, ImagenRespuesta, pImagenRespuesta),
                EditadoRespuesta = 1
			WHERE IdRespuesta = pIdRespuesta;
	END IF;
    
    IF pOpc = 'UE'
	THEN
		UPDATE respuestas
			SET TextoRespuesta = pTextoRespuesta,
				ImagenRespuesta = NULL,
                EditadoRespuesta = 1
			WHERE IdRespuesta = pIdRespuesta;
	END IF;
    
	#TODO: PENDIENTECon un instead of Delete marcar la baja logica y cambiar EstadoRespuesta = 0
	IF pOpc = 'D'
	THEN
		DELETE
			FROM respuestas
		WHERE IdRespuesta = pIdRespuesta;
	END IF;

	IF pOpc = 'S'
	THEN
		SELECT IdRespuesta, TextoRespuesta, ImagenRespuesta, FechaCreacionRespuesta, EstadoRespuesta, EditadoRespuesta, Usuario_Responde, Pregunta_Respondida
			FROM respuestas
		WHERE IdRespuesta = pIdRespuesta;
	END IF;

	IF pOpc = 'B'
    THEN
		SELECT 	R.IdRespuesta, R.TextoRespuesta, R.ImagenRespuesta, R.FechaCreacionRespuesta, 
				R.EstadoRespuesta, R.EditadoRespuesta, R.Usuario_Responde, R.Pregunta_Respondida,
                U.UsernameUsuario, U.ImagenPerfilUsuario,
                COUNT(distinct UCRU.IdCalificacion) AS "CantVotosUtiles",
                COUNT(distinct UCRNU.IdCalificacion) AS "CantVotosNoUtiles"
                
			FROM respuestas AS R
		INNER JOIN usuarios AS U
        ON U.IdUsuario = R.Usuario_Responde
        INNER JOIN preguntas AS P
        ON P.IdPregunta = R.Pregunta_Respondida
        
		
        LEFT JOIN usuarios_calf_respuesta AS UCRU
        ON UCRU.RespuestaCalificada = R.IdRespuesta AND UCRU.UtilidadRespuestaCalificada = 1
        LEFT JOIN usuarios_calf_respuesta AS UCRNU
        ON UCRNU.RespuestaCalificada = R.IdRespuesta AND UCRNU.UtilidadRespuestaCalificada = 0
        
        WHERE IdRespuesta = pIdRespuesta;
        
    END IF;

	IF pOpc = 'R'
    THEN
		SELECT 	R.IdRespuesta, R.TextoRespuesta, R.ImagenRespuesta, R.FechaCreacionRespuesta, 
				R.EstadoRespuesta, R.EditadoRespuesta, R.Usuario_Responde, R.Pregunta_Respondida,
                U.UsernameUsuario, U.ImagenPerfilUsuario
                
			FROM respuestas AS R
		INNER JOIN usuarios AS U
        ON U.IdUsuario = R.Usuario_Responde
        INNER JOIN preguntas AS P
        ON P.IdPregunta = R.Pregunta_Respondida
        WHERE P.IdPregunta = pPregunta_Respondida
        ORDER BY R.FechaCreacionRespuesta DESC LIMIT pNumeroRespuestaPagina, 10;
    END IF;
    
    IF pOpc = 'V' 
    THEN
		SELECT 	R.IdRespuesta, R.TextoRespuesta, R.ImagenRespuesta, R.FechaCreacionRespuesta, 
				R.EstadoRespuesta, R.EditadoRespuesta, R.Usuario_Responde, R.Pregunta_Respondida,
                U.UsernameUsuario, U.ImagenPerfilUsuario,
                COUNT(distinct UCRU.IdCalificacion) AS "CantVotosUtiles",
                COUNT(distinct UCRNU.IdCalificacion) AS "CantVotosNoUtiles"
                
			FROM respuestas AS R
		INNER JOIN usuarios AS U
        ON U.IdUsuario = R.Usuario_Responde
        INNER JOIN preguntas AS P
        ON P.IdPregunta = R.Pregunta_Respondida
        
        LEFT JOIN usuarios_calf_respuesta AS UCRU
        ON UCRU.RespuestaCalificada = R.IdRespuesta AND UCRU.UtilidadRespuestaCalificada = 1
        LEFT JOIN usuarios_calf_respuesta AS UCRNU
        ON UCRNU.RespuestaCalificada = R.IdRespuesta AND UCRNU.UtilidadRespuestaCalificada = 0
        
        WHERE P.IdPregunta = pPregunta_Respondida
        GROUP BY R.IdRespuesta
        ORDER BY R.FechaCreacionRespuesta DESC LIMIT pNumeroRespuestaPagina, 10;
    END IF;
    
	IF pOpc = 'X'
	THEN
		SELECT IdRespuesta, TextoRespuesta, ImagenRespuesta, FechaCreacionRespuesta, EstadoRespuesta, EditadoRespuesta, Usuario_Responde, Pregunta_Respondida
			FROM respuestas;
		
	END IF;
    
     IF pOpc = 'UR'
    THEN
		SELECT  R.IdRespuesta, R.TextoRespuesta, R.ImagenRespuesta, 
				R.FechaCreacionRespuesta, R.EstadoRespuesta, R.EditadoRespuesta, 
                R.Usuario_Responde, R.Pregunta_Respondida,
                U.UsernameUsuario, U.ImagenPerfilUsuario 
			FROM respuestas AS R
        INNER JOIN usuarios AS U
        ON U.IdUsuario = R.Usuario_Responde
        WHERE U.IdUsuario = pUsuario_Responde
        AND   R.EstadoRespuesta = 1
        ORDER BY R.FechaCreacionRespuesta DESC LIMIT pNumeroRespuestaPagina, 10;
    END IF;
    
    IF pOpc = 'E'
	THEN
		UPDATE respuestas
			SET 
				EstadoRespuesta = 0
			WHERE IdRespuesta = pIdRespuesta;
            
		UPDATE preguntas as P
        INNER JOIN respuestas AS R
        ON R.IdRespuesta = P.Respuesta_Correcta
			SET 
				P.Respuesta_Correcta = NULL				
		WHERE R.IdRespuesta = pIdRespuesta;
            
	END IF;
END$$
DELIMITER ;




DROP procedure IF EXISTS `sp_Categorias`;
DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `sp_Categorias` (
	IN pOpc					char(3),
	IN pIdCategoria 		int,
	IN pTituloCategoria 	varchar(40)
)
BEGIN
IF pOpc = 'I'
	THEN 
		INSERT INTO categorias(TituloCategoria)
			VALUES(pTituloCategoria);
		
	END IF;

	IF pOpc = 'U'
	THEN
		UPDATE categorias
			SET TituloCategoria = pTituloCategoria				
			WHERE IdCategoria = pIdCategoria;
            
	END IF;

	IF pOpc = 'D'
	THEN
		DELETE
			FROM categorias
		WHERE IdCategoria = pIdCategoria;
	END IF;

	IF pOpc = 'S'
	THEN
		SELECT IdCategoria, TituloCategoria
			FROM categorias
		WHERE IdCategoria = pIdCategoria;
	END IF;

	IF pOpc = 'X'
	THEN
		SELECT IdCategoria, TituloCategoria
			FROM categorias;
	END IF;
END$$

DELIMITER ;


DROP procedure IF EXISTS `sp_Usuarios_calf_fav_pregunta`;
DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `sp_Usuarios_calf_fav_pregunta` (
	IN pOpc						char(3),
	IN pIdCalificacion 			bigint,
	IN pUsuarioCalifica 		bigint,
	IN pPreguntaCalificada 		bigint
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO usuarios_calf_fav_pregunta(UsuarioCalifica, PreguntaCalificada)
			VALUES(pUsuarioCalifica, pPreguntaCalificada);
		
	END IF;

	IF pOpc = 'U'
	THEN
		UPDATE usuarios_calf_fav_pregunta
			SET UsuarioCalifica = pUsuarioCalifica,
				PreguntaCalificada = pPreguntaCalificada
			WHERE IdCalificacion = pIdCalificacion;
            
	END IF;

	IF pOpc = 'D'
	THEN
		DELETE
			FROM usuarios_calf_fav_pregunta
		WHERE UsuarioCalifica = pUsuarioCalifica
		AND PreguntaCalificada = pPreguntaCalificada;
	END IF;

	IF pOpc = 'S'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, PreguntaCalificada
			FROM usuarios_calf_fav_pregunta
		WHERE IdCalificacion = pIdCalificacion;
	END IF;
    
    IF pOpc = 'V'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, PreguntaCalificada
			FROM usuarios_calf_fav_pregunta
		WHERE UsuarioCalifica = pUsuarioCalifica
        AND PreguntaCalificada = pPreguntaCalificada;
	END IF;

	IF pOpc = 'X'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, PreguntaCalificada
			FROM usuarios_calf_fav_pregunta;
	END IF;
END$$

DELIMITER ;


DROP procedure IF EXISTS `sp_Usuarios_calf_pregunta`;
DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `sp_Usuarios_calf_pregunta` (
	IN pOpc							char(3),
	IN pIdCalificacion 				int,
	IN pUsuarioCalifica 			bigint,
	IN pPreguntaCalificada 			bigint,
	IN pUtilidadPreguntaCalificada 	tinyint(1)
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO usuarios_calf_pregunta(UsuarioCalifica, PreguntaCalificada, UtilidadPreguntaCalificada)
			VALUES(pUsuarioCalifica, pPreguntaCalificada, pUtilidadPreguntaCalificada);
		
	END IF;

	IF pOpc = 'U'
	THEN
		UPDATE usuarios_calf_pregunta
			SET 
				UtilidadPreguntaCalificada = pUtilidadPreguntaCalificada
			WHERE UsuarioCalifica = pUsuarioCalifica
			AND PreguntaCalificada = pPreguntaCalificada;
            
	END IF;

	IF pOpc = 'D'
	THEN
		DELETE
			FROM usuarios_calf_pregunta
		WHERE UsuarioCalifica = pUsuarioCalifica
		AND PreguntaCalificada = pPreguntaCalificada;
	END IF;

	IF pOpc = 'S'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, PreguntaCalificada, UtilidadPreguntaCalificada
			FROM usuarios_calf_pregunta
		WHERE IdCalificacion = pIdCalificacion;
	END IF;
    
	IF pOpc = 'V'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, PreguntaCalificada, UtilidadPreguntaCalificada
			FROM usuarios_calf_pregunta
		WHERE UsuarioCalifica = pUsuarioCalifica
        AND PreguntaCalificada = pPreguntaCalificada;
	END IF;

	IF pOpc = 'X'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, PreguntaCalificada, UtilidadPreguntaCalificada
			FROM usuarios_calf_pregunta;
	END IF;
END$$

DELIMITER ;






DROP procedure IF EXISTS `sp_Usuarios_calf_respuesta`;
DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `sp_Usuarios_calf_respuesta` (
	IN pOpc							char(3),
	IN pIdCalificacion 				int,
	IN pUsuarioCalifica 			bigint,
	IN pRespuestaCalificada 		bigint,
	IN pUtilidadRespuestaCalificada tinyint(1)
)
BEGIN
	IF pOpc = 'I'
	THEN 
		INSERT INTO usuarios_calf_respuesta(UsuarioCalifica, RespuestaCalificada, UtilidadRespuestaCalificada)
			VALUES(pUsuarioCalifica, pRespuestaCalificada, pUtilidadRespuestaCalificada);
		
	END IF;

	IF pOpc = 'U'
	THEN
		UPDATE usuarios_calf_respuesta
			SET 
				UtilidadRespuestaCalificada = pUtilidadRespuestaCalificada
			WHERE UsuarioCalifica = pUsuarioCalifica
			AND RespuestaCalificada = pRespuestaCalificada;
            
	END IF;

	IF pOpc = 'D'
	THEN
		DELETE
			FROM usuarios_calf_respuesta
		WHERE UsuarioCalifica = pUsuarioCalifica
		AND RespuestaCalificada = pRespuestaCalificada;
	END IF;

	IF pOpc = 'S'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, RespuestaCalificada, UtilidadRespuestaCalificada
			FROM usuarios_calf_respuesta
		WHERE IdCalificacion = pIdCalificacion;
	END IF;
    
    
    IF pOpc = 'V'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, RespuestaCalificada, UtilidadRespuestaCalificada
			FROM usuarios_calf_respuesta
		WHERE UsuarioCalifica = pUsuarioCalifica
        AND RespuestaCalificada = pRespuestaCalificada;
	END IF;

	IF pOpc = 'X'
	THEN
		SELECT IdCalificacion, UsuarioCalifica, RespuestaCalificada, UtilidadRespuestaCalificada
			FROM usuarios_calf_respuesta;
	END IF;
END$$

DELIMITER ;





